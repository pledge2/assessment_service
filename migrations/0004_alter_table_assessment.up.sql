BEGIN;

ALTER TABLE assessment ADD COLUMN ext_cbid int;
ALTER TABLE assessment ADD COLUMN branch_id int;

COMMIT;
BEGIN;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS status(
    "id" uuid primary key default (uuid_generate_v4()),
    "status_code" integer unique not null, 
    "status_name" varchar not null,
    "photo_name" varchar not null,
    "created_at" timestamptz not null default NOW(),
    "updated_at" timestamptz not null default NOW()
);
COMMIT;
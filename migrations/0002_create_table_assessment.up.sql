BEGIN;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS assessment(
    "id" uuid primary key default (uuid_generate_v4()),
    "cbid" varchar,
    "a_id" uuid[],
    "p_id" uuid,
    "g_id" uuid,
    "iib_info_id" uuid REFERENCES iib_info(id) ON DELETE CASCADE,
    "status" int,
    "card_id" varchar,
    "created_at" timestamptz not null default NOW(),
    "updated_at" timestamptz not null default NOW()
);
COMMIT;
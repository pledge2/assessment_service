BEGIN;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS iib_info(
    "id" uuid primary key default (uuid_generate_v4()),
    "owner_type" int,
    "owner_pinfl" varchar,
    "owner_fullname" varchar,
    "owner_date_birth" varchar,
    "vehicle_type" integer,
    "vehicle_id" int,
    "vehicle_plate_number" varchar,
    "vehicle_model" varchar,
    "vehicle_color" varchar,
    "vehicle_made_year" integer,
    "vehicle_kuzov" varchar,
    "vehicle_full_weight" integer,
    "vehicle_empty_weight" integer,
    "vehicle_motor" varchar,
    "vehicle_fuel_type" integer,
    "vehicle_seats" integer,
    "vehicle_stands" integer,
    "division" varchar,
    "iib_registration_date" varchar,
    "iib_permit_info" varchar,
    "garov_permit_info" varchar,
    "inspection" varchar,
    "texpassport_seria" varchar,
    "texpassport_number" varchar,
    "body_type_name" varchar,
    "shassi" varchar,
    "power" integer,
    "date_schet_spravka" varchar,
    "tuning_permit" varchar,
    "tuning_given_date" varchar,
    "tuning_issue_date" varchar,
    "prev_pnfl" varchar,
    "prev_owner" varchar,
    "prev_owner_type" varchar,
    "prev_plate_number" varchar,
    "prev_texpassport_seria" varchar,
    "prev_texpassport_number" varchar,
    "statee" varchar,
    "count" integer,
    "created_at" timestamptz not null default NOW()
);
COMMIT;


BEGIN;

ALTER TABLE assessment DROP COLUMN ext_cbid;
ALTER TABLE assessment DROP COLUMN branch_id;

COMMIT;
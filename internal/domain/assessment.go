package domain

import "time"

type AssessmentStore struct {
	ID        string
	CBID      int
	Direct    int
	EXTCBID   int
	BranchID  int
	AIDs      []string
	AID       string
	PID       string
	GID       string
	IIBInfoID string
	Status    int
	Photos    AllPhotosName
	Transport Transport
	CardID    string
}

type AssessmentResponse struct {
	ID        string
	CBID      int
	AIDs      []string
	AID       string
	PID       string
	GID       string
	Status    int
	Photos    AllPhotosUrl
	Transport Transport
}

type GovUz struct {
	VehicleKuzov       string
	OwnerPinfl         string
	OwnerFullname      string
	VehicleModel       string
	VehicleColor       string
	VehicleMadeYear    int
	VehicleMotor       string
	VehicleFullWeight  int
	VehicleEmptyWeight int
	VehiclePlateNumber string
	VehiclePower       int
	PassportNumber     string
	PassportSeria      string
	VinNumber          string
	Seats              int
	Shassi             string
}

type Files struct {
	Names []string
}

type Assessment struct {
	ID            string
	CBID          int
	AIDs          []string
	AID           string
	PID           string
	GID           string
	Transport     Transport
	TransportView TransportView
	GovUz         GovUz
	IIBPermitInfo string
	IIBInfoYear   int
	IIBInfoFuel   int
	IIBInfoID     string
	Status        int
	MarketPrice   int
	CreditPrice   int
	Url           string
	AllPhotosUrl  AllPhotosUrl
	CreatedAt     time.Time
	CardID        string
}

type Transport struct {
	ID            string
	Type          string
	TypeKey       string
	Model         string
	Name          string
	NameKey       string
	Position      string
	Year          int
	Speedometer   int
	Validity      int
	Fuel          string
	Price         int
	SellPrice     int
	ExtraParam    string
	AllPhotosUrl  AllPhotosUrl
	AllPhotosName AllPhotosName
}

type TransportView struct {
	TransportModel    string
	TransportName     string
	TransportPosition string
	TransportYear     int
}

type AllPhotosName struct {
	PhotoName []PhotoName
}
type PhotoName struct {
	LAT       string
	LON       string
	PhotoName string
	Title     string
	Order     int
}

type AllPhotosUrl struct {
	PhotoUrl []PhotoUrl
}

type PhotoUrl struct {
	Title    string
	Order    int
	PhotoUrl string
	LAT      string
	LON      string
}

type AssessmentResult struct {
	MarketPrice   int
	CreditPrice   int
	TransportView TransportView
	Analogs       []string
}

type AssessmentPagination struct {
	Assessments []Assessment
	TotalPages  int
	StatusName  string
}

type AssessmentStatus struct {
	ID          string
	StatusCode  int
	StatusName  string
	StatusCount int
	Url         string
	Comment     string
}

type GovUzParams struct {
	AssessmentID string
	// VinNumber      string
	PassportNumber string
	PassportSeria  string
	PlateNumber    string
}

type Result struct {
	Result bool
}

type Product struct {
	ID        string
	Product   string
	PhotoName string
	PhotoUrl  string
	Active    bool
	CreatedAt time.Time
	UpdatedAt time.Time
}

type ReportData struct {
	CBID          int
	AnalogID      []string
	TransportID   string
	CardID        string
	OwnerFullName string
	OwnerPinfl    string
	VIN           string
	PlateNum      string
	MadeYear      int
	UpdatedAt     time.Time
	Analog        []Analog
	EmpData
	Transport
}

type GovInfoWithCBID struct {
	AssessmentID string
	TransportID  string
	CBID         string
	Transport
	GovUzStore
}

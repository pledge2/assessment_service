package domain

type EmpData struct {
	EmpName     string
	EmpCBID     int
	EmpRole     int
	EmpPosition string
	EmpBranchID string
}

package domain

import "time"

type TransportType struct {
	ID          string
	TypeKey     string
	Index       int
	Type        string
	Active      bool
	PhotoName   string
	FastURL     string
	OfficialURL string
	PhotoURL    string
	CreateAt    time.Time
	UpdateAt    time.Time
}

type TransportModel struct {
	ID       string
	TypeID   string
	Model    string
	Active   bool
	CreateAt time.Time
	UpdateAt time.Time
}

type TransportName struct {
	ID             string
	ModelID        string
	Name           string
	NameKey        string
	TransportGroup string
	Active         bool
	CreateAt       time.Time
	UpdateAt       time.Time
}

type TransportPosition struct {
	ID       string
	NameID   string
	Position string
	Active   bool
	CreateAt time.Time
	UpdateAt time.Time
}

type Discounts struct {
	ID             string
	NameID         string
	DiscountParams []DiscountParams
}

type DiscountParams struct {
	Code      int
	Percent   int
	CreatedAt string
}

type ReferenceNames struct {
	Type     string
	Model    string
	Name     string
	Position string
	Fuel     string
}

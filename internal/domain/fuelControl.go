package domain

type FuelControlParam struct {
	FuelID    string
	Fuel      string
	Price     int
	CreatedAt string
}

type FuelControls struct {
	ID           string
	NameID       string
	FuelControls []FuelControlParam
}

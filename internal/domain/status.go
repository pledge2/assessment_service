package domain

import "time"

type Status struct {
	ID         string
	StatusCode int
	StatusName string
	Count      int
	CreateAt   time.Time
	UpdateAt   time.Time
}

package domain

type GovUzStore struct {
	Pinfl                string
	OwnerFullname        string
	OwnerType            int
	OwnerDateBirth       string
	VehicleID            int
	PlateNumber          string
	Model                string
	VehicleColor         string
	RegistrationDate     string
	IibPermitInfo        string
	Division             string
	GarovPermitInfo      string
	Year                 int
	VehicleType          int
	Kuzov                string
	FullWeight           int
	EmptyWeight          int
	Motor                string
	FuelType             int
	Seats                int
	Stands               int
	Comments             string
	Inspection           string
	TexpassportSeria     string
	TexpassportNumber    string
	BodyTypeName         string
	Shassi               string
	Power                int
	DateSchetSpravka     string
	TuningPermit         string
	TuningGivenDate      string
	TuningIssueDate      string
	PrevPnfl             string
	PrevOwner            string
	PrevOwnerType        string
	PrevPlateNumber      string
	PrevTexpasportSery   string
	PrevTexpasportNumber string
	State                string
}

// type GovUzStore struct {
// 	VinNumber string `json:"vin_number"`
// 	// PassportNumber     string `json:"passport_number"`
// 	// PassportSeria      string `json:"passport_seria"`
// 	OwnerFullname       string `json:"owner_fullname"`
// 	OwnerPinfl          string `json:"owner_pinfl"`
// 	VehicleModel        string `json:"vehicle_model"`
// 	VehicleColor        string `json:"vehicle_color"`
// 	VehicleYear         string `json:"vehicle_year"`
// 	VehicleMotor        string `json:"vehicle_motor"`
// 	VehicleFullWeight   string `json:"vehicle_full_weight"`
// 	VehicleEmptyWeight  string `json:"vehicle_empty_weight"`
// 	VehiclePlateNumber  string `json:"vehicle_plate_number"`  //iib_info table
// 	VehicleEngineNumber string `json:"vehicle_engine_number"` //iib_info table
// 	IIBPermitInfo       string `json:"iib_permit_info"`
// }

type GovUzResponse struct {
	Result          int       `json:"result"`
	Comment         string    `json:"comment"`
	Pinfl           string    `json:"pinfl"`
	OwnerFullname   string    `json:"owner"`
	OwnerType       int       `json:"owner_type"`
	OrganizationInn string    `json:"organization_inn"`
	Vehicle         []Vehicle `json:"vehicle"`
}

type Vehicle struct {
	VehicleID            int    `json:"vehicle_id"`
	PlateNumber          string `json:"plate_number"`
	Model                string `json:"model"`
	VehicleColor         string `json:"vehicle_color"`
	RegistrationDate     string `json:"registration_date"`
	Division             string `json:"division"`
	Year                 int    `json:"year"`
	VehicleType          int    `json:"vehicle_type"`
	Kuzov                string `json:"kuzov"`
	FullWeight           int    `json:"full_weight"`
	EmptyWeight          int    `json:"empty_weight"`
	Motor                string `json:"motor"`
	FuelType             int    `json:"fuel_type"`
	Seats                int    `json:"seats"`
	Stands               int    `json:"stands"`
	Comments             string `json:"comments"`
	Inspection           string `json:"inspection"`
	TexpassportSeria     string `json:"texpassport_seria"`
	TexpassportNumber    string `json:"texpassport_number"`
	BodyTypeName         string `json:"body_type_name"`
	Shassi               string `json:"shassi"`
	Power                int    `json:"power"`
	DateSchetSpravka     string `json:"date_schet_spravka"`
	TuningPermit         string `json:"tuning_permit"`
	TuningGivenDate      string `json:"tuning_given_date"`
	TuningIssueDate      string `json:"tuning_issue_date"`
	PrevPnfl             string `json:"prev_pnfl"`
	PrevOwner            string `json:"prev_owner"`
	PrevOwnerType        string `json:"prev_owner_type"`
	PrevPlateNumber      string `json:"prev_plate_number"`
	PrevTexpasportSery   string `json:"prev_texpasport_seria"`
	PrevTexpasportNumber string `json:"prev_texpasport_number"`
	State                string `json:"state"`
}

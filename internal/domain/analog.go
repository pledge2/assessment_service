package domain

type Analog struct {
	ID            string
	Model         string
	Name          string
	Position      string
	Speedometer   int
	Year          int
	Fuel          string
	SellPrice     int
	Price         int
	Validity      int
	Condition     string
	Comment       string
	SourceFileUrl string
	SourceFile    string
}

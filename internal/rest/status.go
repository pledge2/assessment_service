package rest

import (
	"pledge/assessment_service/internal/domain"
	"pledge/assessment_service/internal/errs"
	"pledge/assessment_service/pkg/utils"

	"github.com/gin-gonic/gin"
)

type statusStore struct {
	StatusCode int    `json:"status_code"`
	StatusName string `json:"status_name"`
}

// createStatus godoc
// @Router /status/store [POST]
// @Summary Create a new Status
// @Description API Create a Status
// @Tags Status
// @Produce json
// @Param request body statusStore true "Status details"
// @Success 201 {object} Response{data=idResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) storeStatus() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			response idResponse
			data     statusStore
		)
		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, response, errs.ErrValidation)

			return

		}
		request := domain.Status{
			StatusCode: data.StatusCode,
			StatusName: data.StatusName,
		}
		id, err := s.statusU.Store(c, request)
		if err != nil {
			Return(c, response, err)

			return
		}
		response = idResponse{
			ID: id,
		}

		Return(c, response, nil)
	}
}

type statusResponse struct {
	ID         string `json:"id"`
	StatusCode int    `json:"status_code"`
	StatusName string `json:"status_name"`
	Count      int    `json:"count"`
	CreatedAt  string `json:"created_at"`
}

// findStatusByID godoc
// @Router /status/find/one [GET]
// @Summary Find Status
// @Description API Find Status
// @Tags Status
// @Produce json
// @Param id query string true "Status -> id"
// @Success 201 {object} Response{data=statusResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) findOne() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			response statusResponse
		)
		id := c.Query("id")

		if id == "" {
			Return(c, response, errs.ErrStatusIDEmpty)

			return
		}

		if !utils.IsValidUUID(id) {
			Return(c, response, errs.ErrStatusIDFormat)

			return

		}

		result, err := s.statusU.FindOne(c, id)
		if err != nil {
			Return(c, response, err)

			return
		}

		response = statusResponse{
			ID:         result.ID,
			StatusCode: result.StatusCode,
			StatusName: result.StatusName,
			Count:      result.Count,
			CreatedAt:  utils.TimeToString(result.CreateAt),
		}
		Return(c, response, err)
	}
}

// findAllStatus godoc
// @Router /status/find/all [GET]
// @Summary Find all Statuses
// @Description API to Find all Statuses
// @Tags Status
// @Produce json
// @Success 201 {object} Response{data=[]statusResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) findStatuses() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			response []statusResponse
		)
		result, err := s.statusU.FindAll(c)
		if err != nil {
			Return(c, response, err)

			return
		}

		for _, val := range result {
			value := statusResponse{
				ID:         val.ID,
				StatusCode: val.StatusCode,
				StatusName: val.StatusName,
				Count:      val.Count,
				CreatedAt:  utils.TimeToString(val.CreateAt),
			}
			response = append(response, value)
		}

		Return(c, response, nil)
	}
}

// updateStatus godoc
// @Router /status/update [PUT]
// @Summary Update Status
// @Description API to Update Status
// @Tags Status
// @Accept json
// @Produce json
// @Param id query string true "id"
// @Param request body statusStore true "Status details"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) updateStatus() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			req      domain.Status
			response Result
		)
		id := c.Query("id")
		if id == "" {
			Return(c, response, errs.ErrStatusIDEmpty)

			return
		}

		if !utils.IsValidUUID(id) {
			Return(c, response, errs.ErrStatusIDFormat)

			return
		}

		if err := c.ShouldBindJSON(&req); err != nil {
			Return(c, response, errs.ErrValidation)

			return
		}

		if err := s.statusU.Update(c, id, req); err != nil {
			Return(c, response, err)

			return
		}

		response = Result{
			Result: true,
		}
		Return(c, response, nil)
	}
}

// deleteStatus godoc
// @Router /status/delete [DELETE]
// @Summary Delete a Status
// @Description API Delete a Status
// @Tags Status
// @Produce json
// @Param id query string true "id"
// @Success 201 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) deleteStatus() gin.HandlerFunc {
	return func(c *gin.Context) {
		var response Result

		id := c.Query("id")
		if id == "" {
			Return(c, response, errs.ErrStatusIDEmpty)

			return
		}

		if !utils.IsValidUUID(id) {
			Return(c, response, errs.ErrStatusIDFormat)

			return
		}

		if err := s.statusU.Delete(c, id); err != nil {
			Return(c, response, err)

			return
		}

		response = Result{
			Result: true,
		}
		Return(c, response, nil)
	}
}

package rest

import (
	"errors"
	"net/http"
	"pledge/assessment_service/internal/errs"
	"pledge/assessment_service/pkg/status"

	"github.com/gin-gonic/gin"
)

type Response struct {
	Status    string      `json:"status"`
	ErrorCode int         `json:"error_code"`
	ErrorNote string      `json:"error_note"`
	Data      interface{} `json:"data"`
}

type Result struct {
	Result bool `json:"result"`
}

type idResponse struct {
	ID string `json:"id"`
}

//nolint:exhaustruct
func Return(c *gin.Context, data interface{}, err error) {
	switch {
	case err == nil:
		c.JSON(http.StatusOK, Response{
			Status:    status.Success,
			ErrorCode: status.NoError,
			ErrorNote: "",
			Data:      data,
		})
	case errors.Is(err, errs.ErrUnauthorized):
		c.JSON(http.StatusUnauthorized, Response{
			Status:    status.Failure,
			ErrorCode: errCode(err),
			ErrorNote: err.Error(),
			Data:      data,
		})

	case errors.Is(err, errs.ErrValidation):
		c.JSON(http.StatusUnprocessableEntity, Response{
			Status:    status.Failure,
			ErrorCode: errCode(err),
			ErrorNote: err.Error(),
			Data:      data,
		})

	default:
		c.JSON(http.StatusInternalServerError, Response{
			Status:    status.Failure,
			ErrorCode: errCode(err),
			ErrorNote: err.Error(),
			Data:      data,
		})
	}
}

func errCode(err error) int {
	switch err.Error() {
	case errs.ErrInternal.Error():
		{
			return status.ErrorCodeInternal
		}
	case errs.ErrValidation.Error():
		{
			return status.ErrorCodeValidation
		}

	case errs.ErrUnauthorized.Error():
		{
			return status.ErrorCodeUnauthorization
		}

	case errs.ErrTokenNotFound.Error():
		{
			return status.ErrorCodeTokenNotFound
		}
	case errs.ErrAccessTokenExpired.Error():
		{
			return status.ErrorCodeAccessTokenExpired
		}

	case errs.ErrRefreshTokenExpired.Error():
		{
			return status.ErrorCodeRefreshTokenExpired
		}

	case errs.ErrAnalogNotFound.Error():
		{
			return status.ErrorCodeAnalogNotFound
		}

	case errs.ErrAssessmentIDEmpty.Error():
		{
			return status.ErrorCodeAssessmentIDEmpty
		}
	case errs.ErrAssessmentIDFormat.Error():
		{
			return status.ErrorCodeAssessmentIDFormat
		}
	case errs.ErrStatusIDEmpty.Error():
		{
			return status.ErrorCodeStatusIDEmpty
		}
	case errs.ErrStatusIDFormat.Error():
		{
			return status.ErrorCodeStatusIDFormat
		}

	case errs.ErrStatusCodeEmpty.Error():
		{
			return status.ErrorCodeStatusCodeEmpty
		}
	case errs.ErrStatusCodeFormat.Error():
		{
			return status.ErrorCodeStatusCodeFormat
		}
	case errs.ErrLimitEmpty.Error():
		{
			return status.ErrorCodeLimitEmpty
		}
	case errs.ErrLimitFormat.Error():
		{
			return status.ErrorCodeLimitFormat
		}

	case errs.ErrPageEmpty.Error():
		{
			return status.ErrorCodePageEmpty
		}

	case errs.ErrPageFormat.Error():
		{
			return status.ErrorCodePageFormat
		}

	case errs.ErrCbidEmpty.Error():
		{
			return status.ErrorCodeCbidEmpty
		}

	case errs.ErrCbidFormat.Error():
		{
			return status.ErrorCodeCbidFormat
		}

	case errs.ErrCbidNotFound.Error():
		{
			return status.ErrorCodeCbidNotFound
		}
	case errs.ErrCbidOrEmailNotFound.Error():
		{
			return status.ErrorCodeCbidOrEmailNotFound
		}
	case errs.ErrSmsFormat.Error():
		{
			return status.ErrorCodeSmsFormat
		}

	case errs.ErrDeviceFormat.Error():
		{
			return status.ErrorCodeDeviceFormat
		}
	case errs.ErrPasswordNotSame.Error():
		{
			return status.ErrorCodePasswordNotSame
		}
	case errs.ErrPasswordFormat.Error():
		{
			return status.ErrorCodePasswordFormat
		}
	case errs.ErrPasswordInvalid.Error():
		{
			return status.ErrorCodePasswordInvalid
		}
	case errs.ErrBankEmail.Error():
		{
			return status.ErrorCodeBankEmail
		}
	case errs.ErrEmailFormat.Error():
		{
			return status.ErrorCodeEmailFormat
		}
	case errs.ErrDeviceBlocked.Error():
		{
			return status.ErrorCodeDeviceBlocked
		}
	case errs.ErrFakeUser.Error():
		{
			return status.ErrorCodeFakeUser
		}
	case errs.ErrDataNotFound.Error():
		{
			return status.ErrorCodeDataNotFound
		}
	case errs.ErrSmsExpired.Error():
		{
			return status.ErrorCodeSmsExpired
		}

	case errs.ErrRoleIDEmpty.Error():
		{
			return status.ErrorCodeRoleIDEmpty
		}
	case errs.ErrRoleIDFormat.Error():
		{
			return status.ErrorCodeRoleIDFormat
		}
	case errs.ErrRoleCodeEmpty.Error():
		{
			return status.ErrorCodeRoleCodeEmpty
		}
	case errs.ErrRoleCodeFormat.Error():
		{
			return status.ErrorCodeRoleCodeFormat
		}
	case errs.ErrPostCodeEmpty.Error():
		{
			return status.ErrorCodePostCodeEmpty
		}

	case errs.ErrTypeIDEmpty.Error():
		{
			return status.ErrorCodeTypeIDEmpty
		}
	case errs.ErrTypeIDFormat.Error():
		{
			return status.ErrorCodeTypeIDFormat
		}

	case errs.ErrModelIDEmpty.Error():
		{
			return status.ErrorCodeModelIDEmpty
		}
	case errs.ErrModelIDFormat.Error():
		{
			return status.ErrorCodeModelIDFormat
		}
	case errs.ErrNameIDEmpty.Error():
		{
			return status.ErrorCodeNameIDEmpty
		}
	case errs.ErrNameIDFormat.Error():
		{
			return status.ErrorCodeNameIDFormat
		}
	case errs.ErrPositionIDEmpty.Error():
		{
			return status.ErrorCodePositionIDEmpty
		}
	case errs.ErrPositionIDFormat.Error():
		{
			return status.ErrorCodePositionIDFormat
		}
	case errs.ErrValidityIDEmpty.Error():
		{
			return status.ErrorCodeValidityIDEmpty
		}
	case errs.ErrValidityIDFormat.Error():
		{
			return status.ErrorCodeValidityIDFormat
		}
	case errs.ErrFuelIDEmpty.Error():
		{
			return status.ErrorCodeFuelIDEmpty
		}
	case errs.ErrFuelIDFormat.Error():
		{
			return status.ErrorCodeFuelIDFormat
		}

	case errs.ErrProductIDEmpty.Error():
		{
			return status.ErrorCodeProductIDEmpty
		}
	case errs.ErrProductIDFormat.Error():
		{
			return status.ErrorCodeProductIDFormat
		}
	case errs.ErrTransportIDEmpty.Error():
		{
			return status.ErrorCodeTransportIDEmpty
		}
	case errs.ErrTransportIDFormat.Error():
		{
			return status.ErrorCodeTransportIDFormat
		}
	case errs.ErrAnalogIDEmpty.Error():
		{
			return status.ErrorCodeAnalogIDEmpty
		}
	case errs.ErrAnalogIDFormat.Error():
		{
			return status.ErrorCodeAnalogIDFormat
		}

	case errs.ErrBucketEmpty.Error():
		{
			return status.ErrorCodeBucketEmpty
		}
	case errs.ErrFolderEmpty.Error():
		{
			return status.ErrorCodeFolderEmpty
		}
	case errs.ErrLatEmpty.Error():
		{
			return status.ErrorCodeLATEmpty
		}
	case errs.ErrLonEmpty.Error():
		{
			return status.ErrorCodeLONEmpty
		}
	case errs.ErrSuffixEmpty.Error():
		{
			return status.ErrorCodeSuffixEmpty
		}

	case errs.ErrFileNameEmpty.Error():
		{
			return status.ErrorCodeFileNameEmpty
		}
	case errs.ErrTooLargeSize.Error():
		{
			return status.ErrorCodeTooLargeSize
		}
	case errs.ErrRoleChanged.Error():
		{
			return status.ErrorCodeRoleChanged
		}

	case errs.ErrCbidDuplicate.Error():
		{
			return status.ErrorCodeCbidDuplicate
		}
	case errs.ErrPostCodeDuplicate.Error():
		{
			return status.ErrorCodePostCodeDuplicate
		}
	case errs.ErrBranchIDEmpty.Error():
		{
			return status.ErrorCodeBranchIDEmpty
		}
	case errs.ErrBranchIDFormat.Error():
		{
			return status.ErrorCodeBranchIDFormat
		}
	case errs.ErrDirectEmpty.Error():
		{
			return status.ErrorCodeDirectEmpty
		}
	case errs.ErrDirectFormat.Error():
		{
			return status.ErrorCodeDirectFormat
		}
	case errs.ErrEmpIDEmpty.Error():
		{
			return status.ErrorCodeEmpIDEmpty
		}
	case errs.ErrEmpIDFormat.Error():
		{
			return status.ErrorCodeEmpIDFormat
		}
	case errs.ErrDiscountIDEmpty.Error():
		{
			return status.ErrorCodeDiscountIDEmpty
		}
	case errs.ErrDiscountIDFormat.Error():
		{
			return status.ErrorCodeDiscountIDFormat
		}
	case errs.ErrDiscountNotFound.Error():
		{
			return status.ErrorCodeDiscountNotFound
		}
	case errs.ErrTransportMadeYear.Error():
		{
			return status.ErrorCodeTransportMadeYear
		}
	case errs.ErrFakedValidity.Error():
		{
			return status.ErrorCodeFakedValidity
		}

	default:
		{
			return status.ErrorCodeDefault
		}
	}
}

package rest

import (
	"context"
	"net/http"
	"pledge/assessment_service/internal/domain"
	"pledge/assessment_service/pkg/logger"

	"github.com/gin-gonic/gin"
)

type assessmentUsecase interface {
	StorePhoto(ctx context.Context, assessmentID, folder string, data domain.PhotoName) (domain.Result, error)
	Store(ctx context.Context, data domain.AssessmentStore) (string, error)
	GetPhotos(ctx context.Context, assessmentID string) (domain.AllPhotosUrl, error)
	GetInfo(ctx context.Context, id string) (domain.Assessment, error)
	Find(ctx context.Context, id string) (domain.Assessment, error)
	CountAllStatus(ctx context.Context, cbid, branchID, roleCode, direct int) ([]domain.AssessmentStatus, error)
	FindAll(ctx context.Context) ([]domain.Assessment, error)
	FindByStatus(ctx context.Context, status, limit, page, cbid, branchID, roleCode, direct int) (domain.AssessmentPagination, error)
	FastAssessment(ctx context.Context, data domain.AssessmentStore) (domain.AssessmentResult, error)
	OfficialAssessment(ctx context.Context, data domain.AssessmentStore) (domain.AssessmentResult, error)
	GetReportData(ctx context.Context, id string) (domain.ReportData, error)
	UpdateStatus(ctx context.Context, req domain.AssessmentStatus) error
	AnalogsList(ctx context.Context, assessmentID string) ([]domain.Analog, error)
	GetAssessmentData(ctx context.Context, cbid string, texPassSeria, texPassNum string) (domain.GovInfoWithCBID, error)
	DeleteAssessment(ctx context.Context, assessmentID string) (domain.Result, error)
}

type govInfoUsecase interface {
	SearchByCarNum(ctx context.Context, val domain.GovUzParams) (domain.GovUz, error)
	UpdateGovInfoData(ctx context.Context, assessmentID string, info domain.GovUzStore) error
}

type statusUsecase interface {
	Store(ctx context.Context, data domain.Status) (string, error)
	FindOne(ctx context.Context, id string) (domain.Status, error)
	FindAll(ctx context.Context) ([]domain.Status, error)
	Update(ctx context.Context, id string, data domain.Status) error
	Delete(ctx context.Context, id string) error
}
type Server struct {
	router      *gin.Engine
	log         logger.Logger
	assessmentU assessmentUsecase
	govInfoU    govInfoUsecase
	statusU     statusUsecase
}

func New(r *gin.Engine, log logger.Logger, assessmentU assessmentUsecase, govInfoU govInfoUsecase, statusU statusUsecase) *Server {
	srv := &Server{
		router:      r,
		log:         log,
		assessmentU: assessmentU,
		govInfoU:    govInfoU,
		statusU:     statusU,
	}
	srv.Routing()

	return srv
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.router.ServeHTTP(w, r)
}

package rest

import (
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func (s *Server) Routing() {

	assessment := s.router.Group("assessment")
	assessment.GET("/find/one", s.findAssessment())
	assessment.GET("/find/all", s.findAssessments())
	assessment.GET("/find/status", s.findByStatus())
	assessment.GET("/count/status", s.countStatus())
	assessment.DELETE("/delete", s.deleteAssessment())

	assessment.POST("/store/main", s.storeMainInfo())
	assessment.GET("/get/photos", s.getPhotos())
	assessment.PUT("/store/photo", s.storePhoto())

	assessment.GET("/get/report", s.getReportData())
	assessment.GET("/analogs/list", s.analogsList())
	assessment.POST("/change/status", s.changeAssessmentStatus())

	assessment.POST("/govuz/carinfo", s.searchByCarNum())
	assessment.POST("/official", s.officialAssessment())
	assessment.POST("/fast", s.fastAssessment())

	assessment.GET("/data", s.getAssessmentData())

	s.router.PUT("/update/gov/data", s.updateGovData())

	status := s.router.Group("status")
	status.POST("/store", s.storeStatus())
	status.GET("/find/one", s.findOne())
	status.GET("/find/all", s.findStatuses())
	status.PUT("/update", s.updateStatus())
	status.DELETE("/delete", s.deleteStatus())

	s.router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
}

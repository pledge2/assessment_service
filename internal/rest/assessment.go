package rest

import (
	"fmt"
	"pledge/assessment_service/internal/domain"
	"pledge/assessment_service/internal/errs"
	"pledge/assessment_service/pkg/utils"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

type allPhotoUrl struct {
	PhotoUrl []photoUrl `json:"photo"`
}

type photoUrl struct {
	Title    string `json:"title"`
	Order    int    `json:"order"`
	PhotoUrl string `json:"photo_url"`
	LAT      string `json:"lat"`
	LON      string `json:"lon"`
}

type assessmentResponse struct {
	ID                 string      `json:"id"`   // baxolash id
	CBID               int         `json:"cbid"` // user id
	AIDs               []string    `json:"-"`    // analoglar id
	AID                string      `json:"-"`    // analoglar id
	PID                string      `json:"p_id"` // product id
	GID                string      `json:"-"`    //Analog table bilan transport_deatils table ni reference_key
	IIBInfoID          string      `json:"iib_info_id"`
	Status             int         `json:"status"` // baxolash statusi, xolati
	VehicleKuzov       string      `json:"vehicle_kuzov"`
	OwnerPinfl         string      `json:"owner_pinfl"`
	OwnerFullname      string      `json:"owner_fullname"`
	VehicleModel       string      `json:"vehicle_model"`
	VehicleColor       string      `json:"vehicle_color"`
	VehicleMadeYear    int         `json:"vehicle_year"`
	VehicleMotor       string      `json:"vehicle_motor"`
	VehicleFullWeight  int         `json:"vehicle_full_weight"`
	VehicleEmptyWeight int         `json:"vehicle_empty_weight"`
	VehiclePlateNumber string      `json:"vehicle_plate_number"`
	VehiclePower       int         `json:"vehicle_power"`
	PassportNumber     string      `json:"passport_number"`
	PassportSeria      string      `json:"passport_seria"`
	VinNumber          string      `json:"vin_number"`
	VehicleShassi      string      `json:"vehicle_shassi"`
	Type               string      `json:"type"`
	TypeKey            string      `json:"type_key"`
	Model              string      `json:"model"`
	Name               string      `json:"name"`
	NameKey            string      `json:"name_key"`
	Position           string      `json:"position"`
	Year               int         `json:"year"`
	Speedometer        int         `json:"speedometer"`
	Validity           int         `json:"validity"`
	Fuel               string      `json:"fuel"`
	Price              int         `json:"price"`
	SellPrice          int         `json:"sell_price"`
	MarketPrice        int         `json:"market_price"`
	CreditPrice        int         `json:"credit_price"`
	TransportModel     string      `json:"transport_model"`
	TransportName      string      `json:"transport_name"`
	TransportPosition  string      `json:"transport_position"`
	TransportYear      int         `json:"transport_year"`
	Photos             allPhotoUrl `json:"photos"`
	Url                string      `json:"url"`
	ExtraParam         string      `json:"extra_param"`
	IIBPermitInfo      string      `json:"iib_permit_info"`
}

// findAssessment godoc
// @Router /assessment/find/one [GET]
// @Summary Find an Assessment BY assessmentID
// @Description API Find Assessment
// @Tags Assessment Service
// @Produce json
// @Param id query string true "Assessment details"
// @Success 201 {object} Response{data=assessmentResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) findAssessment() gin.HandlerFunc {
	return func(c *gin.Context) {
		var response assessmentResponse
		id := c.Query("id")
		if id == "" {
			Return(c, response, errs.ErrAssessmentIDEmpty)

			return
		}

		if !utils.IsValidUUID(id) {
			Return(c, response, errs.ErrAssessmentIDFormat)

			return
		}

		data, err := s.assessmentU.Find(c, id)
		if err != nil {
			Return(c, response, err)

			return
		}

		response = assessmentResponse{
			ID:                 data.ID,
			CBID:               data.CBID,
			AID:                data.AID,
			PID:                data.PID,
			GID:                data.GID,
			IIBInfoID:          data.IIBInfoID,
			Status:             data.Status,
			VehicleKuzov:       data.GovUz.VehicleKuzov,
			OwnerPinfl:         data.GovUz.OwnerPinfl,
			OwnerFullname:      data.GovUz.OwnerFullname,
			VehicleModel:       data.GovUz.VehicleModel,
			VehicleColor:       data.GovUz.VehicleColor,
			VehicleMadeYear:    data.GovUz.VehicleMadeYear,
			VehicleMotor:       data.GovUz.VehicleMotor,
			VehicleFullWeight:  data.GovUz.VehicleFullWeight,
			VehicleEmptyWeight: data.GovUz.VehicleEmptyWeight,
			VehiclePlateNumber: data.GovUz.VehiclePlateNumber,
			VehiclePower:       data.GovUz.VehiclePower,
			PassportNumber:     data.GovUz.PassportNumber,
			PassportSeria:      data.GovUz.PassportSeria,
			VinNumber:          data.GovUz.VinNumber,
			VehicleShassi:      data.GovUz.Shassi,
			Type:               data.Transport.Type,
			TypeKey:            data.Transport.TypeKey,
			Model:              data.Transport.Model,
			Name:               data.Transport.Name,
			NameKey:            data.Transport.NameKey,
			Position:           data.Transport.Position,
			Year:               data.Transport.Year,
			Speedometer:        data.Transport.Speedometer,
			Validity:           data.Transport.Validity,
			Fuel:               data.Transport.Fuel,
			SellPrice:          data.Transport.SellPrice,
			MarketPrice:        data.MarketPrice,
			CreditPrice:        data.CreditPrice,
			TransportModel:     data.TransportView.TransportModel,
			TransportName:      data.TransportView.TransportName,
			TransportPosition:  data.TransportView.TransportPosition,
			TransportYear:      data.TransportView.TransportYear,
			IIBPermitInfo:      data.IIBPermitInfo,
			ExtraParam:         data.Transport.ExtraParam,
			Url:                data.Url,
		}

		for _, photo := range data.AllPhotosUrl.PhotoUrl {
			value := photoUrl{
				Title:    photo.Title,
				Order:    photo.Order,
				PhotoUrl: photo.PhotoUrl,
				LAT:      photo.LAT,
				LON:      photo.LON,
			}
			response.Photos.PhotoUrl = append(response.Photos.PhotoUrl, value)
		}
		Return(c, response, nil)
	}
}

type assessmentStore struct {
	CBID        int    `json:"cbid" example:"56833"`                                // user id
	EXTCBID     int    `json:"ext_cbid" example:"56833"`                            // user id
	Direct      int    `json:"direct" example:"1"`                                  // user id
	BranchID    int    `json:"branch_id" example:"09012"`                           // user id
	PID         string `json:"p_id" example:"a2a51ecf-d7c6-423b-b80b-0d7887f907f5"` // product id
	Type        string `json:"type" example:"e2dc1bbc-b464-4df5-811f-3329636a4ef0"`
	Model       string `json:"model" example:"890753db-47e1-4723-bb23-bcd9ea14c47e"`
	Name        string `json:"name" example:"296e966d-c0f9-42e0-932b-46d8c164cdb0"`
	Position    string `json:"position" example:"42d6106a-10cc-4417-a03a-0b5e2dc6a4aa"`
	Year        int    `json:"year" example:"2023"`
	Speedometer int    `json:"speedometer" example:"10000"`
	Validity    int    `json:"validity" example:"1"`
	Fuel        string `json:"fuel" example:"bfcdea15-3203-4a1d-9abe-36957241b75a"`
	ExtraParam  string `json:"extra_param"`
}

// storeAssessment godoc
// @Router /assessment/store/main [POST]
// @Summary Create an new Assessment
// @Description API Create an Assessment
// @Tags Assessment Service
// @Produce json
// @Param request body assessmentStore true "Assessment details"
// @Success 201 {object} Response{data=idResponse}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) storeMainInfo() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data     assessmentStore
			response idResponse
		)
		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, response, errs.ErrValidation)

			return
		}

		request := domain.AssessmentStore{
			CBID:     data.CBID,
			EXTCBID:  data.EXTCBID,
			Direct:   data.Direct,
			BranchID: data.BranchID,
			PID:      data.PID,
			Transport: domain.Transport{
				Type:        data.Type,
				Model:       data.Model,
				Name:        data.Name,
				Position:    data.Position,
				Year:        data.Year,
				Speedometer: data.Speedometer,
				Validity:    data.Validity,
				Fuel:        data.Fuel,
				ExtraParam:  data.ExtraParam,
			},
		}

		assessmentID, err := s.assessmentU.Store(c, request)
		if err != nil {
			Return(c, response, err)

			return
		}
		response = idResponse{
			ID: assessmentID,
		}
		Return(c, response, nil)
	}
}

type photoName struct {
	LAT       string `json:"lat"`
	LON       string `json:"lon"`
	PhotoName string `json:"photo_name" binding:"required"`
	Title     string `json:"title"`
	Order     int    `json:"order"`
}

// storePhoto godoc
// @Router /assessment/store/photo [PUT]
// @Summary Create a new Assessment
// @Description API Create a new Assessment
// @Tags Assessment Service
// @Produce json
// @Param assessmentID query string true "Assessment ID"
// @Param folder query string true "folder name"
// @Param req body photoName true "Photos name"
// @Success 201 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) storePhoto() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			response Result
			data     photoName
		)

		assessmentID := c.Query("assessmentID")
		if assessmentID == "" {
			Return(c, response, errs.ErrAssessmentIDEmpty)

			return
		}

		if !utils.IsValidUUID(assessmentID) {
			Return(c, response, errs.ErrAssessmentIDFormat)

			return
		}

		folder := c.Query("folder")
		if folder == "" {
			Return(c, response, errs.ErrFolderEmpty)
			return
		}

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, response, errs.ErrValidation)

			return
		}
		request := domain.PhotoName{
			LAT:       data.LAT,
			LON:       data.LON,
			PhotoName: data.PhotoName,
			Title:     data.Title,
			Order:     data.Order,
		}

		info, err := s.assessmentU.StorePhoto(c, assessmentID, folder, request)
		if err != nil {
			Return(c, response, err)

			return
		}

		response.Result = info.Result
		Return(c, response, nil)
	}
}

// getPhotos godoc
// @Router /assessment/get/photos [GET]
// @Summary Create a new Assessment
// @Description API Create a Assessment
// @Tags Assessment Service
// @Produce json
// @Param assessmentID query string true "Assessment ID"
// @Success 201 {object} Response{data=allPhotoUrl}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) getPhotos() gin.HandlerFunc {
	return func(c *gin.Context) {
		var response allPhotoUrl
		assessmentID := c.Query("assessmentID")
		if assessmentID == "" {
			Return(c, response, errs.ErrAssessmentIDEmpty)
			return
		}

		if !utils.IsValidUUID(assessmentID) {
			Return(c, response, errs.ErrAssessmentIDFormat)

			return
		}

		data, err := s.assessmentU.GetPhotos(c, assessmentID)
		if err != nil {
			Return(c, response, err)

			return
		}

		for _, photo := range data.PhotoUrl {
			value := photoUrl{
				Title:    photo.Title,
				Order:    photo.Order,
				PhotoUrl: photo.PhotoUrl,
				LAT:      photo.LAT,
				LON:      photo.LON,
			}
			response.PhotoUrl = append(response.PhotoUrl, value)
		}

		Return(c, response, nil)
	}
}

type assessmentReport struct {
	MarketPrice       int    `json:"market_price"`
	CreditPrice       int    `json:"credit_price"`
	TransportModel    string `json:"transport_model"`
	TransportName     string `json:"transport_name"`
	TransportPosition string `json:"transport_position"`
	TransportYear     int    `json:"transport_year"`
}

type officialAssessment struct {
	ID string `json:"id" binding:"required"`
}

// officialAssessment godoc
// @Router /assessment/official [POST]
// @Summary Create an new Assessment
// @Description API Create an Assessment
// @Tags Assessment Service
// @Produce json
// @Param request body officialAssessment true "Official Assessment details"
// @Success 201 {object} Response{data=assessmentReport}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) officialAssessment() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data     officialAssessment
			response assessmentReport
		)
		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, response, errs.ErrValidation)

			return
		}

		if !utils.IsValidUUID(data.ID) {
			Return(c, response, errs.ErrAssessmentIDFormat)

			return
		}

		request := domain.AssessmentStore{
			ID: data.ID,
		}

		info, err := s.assessmentU.OfficialAssessment(c, request)
		if err != nil {
			Return(c, response, err)
			return
		}
		response = assessmentReport{
			MarketPrice:       info.MarketPrice,
			CreditPrice:       info.CreditPrice,
			TransportModel:    info.TransportView.TransportModel,
			TransportName:     info.TransportView.TransportName,
			TransportPosition: info.TransportView.TransportPosition,
			TransportYear:     info.TransportView.TransportYear,
		}
		Return(c, response, nil)
	}
}

type assessmentFast struct {
	Type        string `json:"type" binding:"required"`
	Model       string `json:"model" binding:"required"`
	Name        string `json:"name" binding:"required"`
	Year        int    `json:"year" binding:"required"`
	Validity    int    `json:"validity"`
	Position    string `json:"position" binding:"required"`
	Fuel        string `json:"fuel"`
	Speedometer int    `json:"speedometer"`
	NameKey     string `json:"name_key"`
	ExtraParam  string `json:"extra_param"`
}

// fastAssessment godoc
// @Router /assessment/fast [POST]
// @Summary Create an new Assessment
// @Description API Create an Assessment
// @Tags Assessment Service
// @Produce json
// @Param request body assessmentFast true "Fast Assessment details"
// @Success 201 {object} Response{data=assessmentReport}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) fastAssessment() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data     assessmentFast
			response assessmentReport
		)

		err := c.ShouldBindJSON(&data)
		if err != nil {
			Return(c, response, errs.ErrValidation)

			return
		}

		request := domain.AssessmentStore{
			Transport: domain.Transport{
				Type:        data.Type,
				Model:       data.Model,
				Name:        data.Name,
				NameKey:     data.NameKey,
				Year:        data.Year,
				Speedometer: data.Speedometer,
				Validity:    data.Validity,
				Position:    data.Position,
				Fuel:        data.Fuel,
				ExtraParam:  data.ExtraParam,
			},
		}

		info, err := s.assessmentU.FastAssessment(c, request)
		if err != nil {
			Return(c, response, err)

			return
		}

		response = assessmentReport{
			MarketPrice:       info.MarketPrice,
			CreditPrice:       info.CreditPrice,
			TransportModel:    info.TransportView.TransportModel,
			TransportName:     info.TransportView.TransportName,
			TransportPosition: info.TransportView.TransportPosition,
			TransportYear:     info.TransportView.TransportYear,
		}
		Return(c, response, nil)
	}
}

type assessmentList struct {
	ID        string    `json:"id"`   // baxolash id
	GID       string    `json:"g_id"` //Analog table bilan transport_deatils table ni reference_key
	CreatedAt time.Time `json:"created_at"`
}

// findAssessments godoc
// @Router /assessment/find/all [GET]
// @Summary Find All Assessment
// @Description API FindAll Assessment
// @Tags Assessment Service
// @Produce json
// @Success 201 {object} Response{data=[]assessmentList}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) findAssessments() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result []assessmentList

		data, err := s.assessmentU.FindAll(c)
		if err != nil {
			Return(c, result, err)

			return
		}

		for _, val := range data {
			value := assessmentList{
				ID:        val.ID,
				GID:       val.GID,
				CreatedAt: val.CreatedAt,
			}

			result = append(result, value)
		}

		Return(c, result, nil)
	}
}

type assessmentPagination struct {
	Assessments []assessment `json:"assessments"`
	TotalPages  int          `json:"total_pages"`
	StatusName  string       `json:"status_name"`
}

type assessment struct {
	ID                 string      `json:"id"`
	CBID               int         `json:"cbid"`
	AID                string      `json:"a_id"`
	PID                string      `json:"p_id"`
	GID                string      `json:"g_id"`
	IIBInfoID          string      `json:"iib_info_id"`
	Status             int         `json:"status"`
	OwnerFullName      string      `json:"owner_fullname"`
	VehiclePlateNumber string      `json:"vehicle_plate_number"`
	VehicleKuzov       string      `json:"vehicle_kuzov"`
	PhotoUrl           allPhotoUrl `json:"photos"`
	CreateAt           string      `json:"created_at"`
	TransportName      string      `json:"transport_name"`
	CardID             string      `json:"card_id"`
}

// finjsonyStatus godoc
// @Router /assessment/find/status [GET]
// @Summary Find an Assessment By Status
// @Description API Find Assessment By Status
// @Tags Assessment Service
// @Produce json
// @Param direct query string true "direct"
// @Param roleCode query string true "RoleCode"
// @Param branchID query string true "BranchID"
// @Param cbid query string true "Cbid"
// @Param status query string true "Status"
// @Param limit query string true "Limit"
// @Param page query string true "Page"
// @Success 201 {object} Response{data=assessmentPagination}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) findByStatus() gin.HandlerFunc {
	return func(c *gin.Context) {
		var result assessmentPagination

		roleCode := c.Query("roleCode")
		if roleCode == "" {
			Return(c, result, errs.ErrRoleCodeEmpty)

			return
		}

		intRoleCode, err := strconv.Atoi(roleCode)
		if err != nil {
			Return(c, result, errs.ErrRoleCodeFormat)

			return
		}

		if intRoleCode < 0 {
			Return(c, result, errs.ErrRoleCodeFormat)

			return
		}

		branchID := c.Query("branchID")
		if branchID == "" {
			Return(c, result, errs.ErrStatusCodeEmpty)

			return
		}

		intBranchID, err := strconv.Atoi(branchID)
		if err != nil {
			Return(c, result, errs.ErrStatusCodeFormat)

			return
		}

		if intBranchID < 0 {
			Return(c, result, errs.ErrStatusCodeFormat)

			return
		}

		statusParam := c.Query("status")
		if statusParam == "" {
			Return(c, result, errs.ErrStatusCodeEmpty)

			return
		}

		intStatus, err := strconv.Atoi(statusParam)
		if err != nil {
			Return(c, result, errs.ErrStatusCodeFormat)

			return
		}

		if intStatus < 0 {
			Return(c, result, errs.ErrStatusCodeFormat)

			return
		}

		limit := c.Query("limit")
		if limit == "" {
			Return(c, result, errs.ErrLimitEmpty)

			return
		}

		intLimit, err := strconv.Atoi(limit)
		if err != nil {
			Return(c, result, errs.ErrLimitFormat)

			return
		}

		if intLimit < 0 {
			Return(c, result, errs.ErrLimitFormat)

			return
		}

		page := c.Query("page")
		if page == "" {
			Return(c, result, errs.ErrPageEmpty)

			return
		}

		intPage, err := strconv.Atoi(page)
		if err != nil {
			Return(c, result, errs.ErrPageFormat)

			return
		}

		if intPage < 0 {
			Return(c, result, errs.ErrPageFormat)

			return
		}

		cbid := c.Query("cbid")
		if cbid == "" {
			Return(c, result, errs.ErrCbidEmpty)

			return
		}

		intCbid, err := strconv.Atoi(cbid)
		if err != nil {
			Return(c, result, errs.ErrCbidFormat)

			return
		}

		direct := c.Query("direct")
		if direct == "" {
			Return(c, result, errs.ErrDirectEmpty)

			return
		}

		intDirect, err := strconv.Atoi(direct)
		if err != nil {
			Return(c, result, errs.ErrDirectFormat)

			return
		}

		data, err := s.assessmentU.FindByStatus(c, intStatus, intLimit, intPage, intCbid, intBranchID, intRoleCode, intDirect)
		if err != nil {
			Return(c, nil, err)

			return
		}

		result = assessmentPagination{
			TotalPages: data.TotalPages,
			StatusName: data.StatusName,
		}

		for _, val := range data.Assessments {
			info := assessment{
				ID:                 val.ID,
				CBID:               val.CBID,
				AID:                val.AID,
				PID:                val.PID,
				GID:                val.GID,
				IIBInfoID:          val.IIBInfoID,
				Status:             val.Status,
				CardID:             val.CardID,
				OwnerFullName:      val.GovUz.OwnerFullname,
				VehiclePlateNumber: val.GovUz.VehiclePlateNumber,
				VehicleKuzov:       val.GovUz.VehicleKuzov,
				CreateAt:           utils.TimeToString(val.CreatedAt),
				TransportName:      val.TransportView.TransportName,
			}

			for _, photo := range val.AllPhotosUrl.PhotoUrl {
				if photo.Title == "Oldi qismi" {
					value := photoUrl{
						Title:    photo.Title,
						Order:    photo.Order,
						PhotoUrl: photo.PhotoUrl,
						LAT:      photo.LAT,
						LON:      photo.LON,
					}
					info.PhotoUrl.PhotoUrl = append(info.PhotoUrl.PhotoUrl, value)
				}
			}
			result.Assessments = append(result.Assessments, info)
		}
		Return(c, result, nil)
	}
}

type assessmentStatus struct {
	StatusCode int    `json:"status_code"`
	StatusName string `json:"status_name"`
	Count      int    `json:"count"`
}

// countAllStatus godoc
// @Router /assessment/count/status [GET]
// @Summary Count All Status
// @Description API Count All Status
// @Tags Assessment Service
// @Produce json
// @Param direct query string true "direct"
// @Param roleCode query string true "roleCode"
// @Param branchID query string true "branchID"
// @Param cbid query string true "Cbid"
// @Success 201 {object} Response{data=[]assessmentStatus}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) countStatus() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			result []assessmentStatus
		)

		roleCode := c.Query("roleCode")
		if roleCode == "" {
			Return(c, result, errs.ErrRoleCodeEmpty)

			return
		}

		intRoleCode, err := strconv.Atoi(roleCode)
		if err != nil {
			Return(c, result, errs.ErrRoleCodeFormat)

			return
		}

		if intRoleCode < 0 {
			Return(c, result, errs.ErrRoleCodeFormat)

			return
		}

		branchID := c.Query("branchID")
		if branchID == "" {
			Return(c, result, errs.ErrStatusCodeEmpty)

			return
		}

		intBranchID, err := strconv.Atoi(branchID)
		if err != nil {
			Return(c, result, errs.ErrStatusCodeFormat)

			return
		}

		if intBranchID < 0 {
			Return(c, result, errs.ErrStatusCodeFormat)

			return
		}

		cbid := c.Query("cbid")
		if cbid == "" {
			Return(c, result, errs.ErrCbidEmpty)

			return

		}

		intCbid, err := strconv.Atoi(cbid)
		if err != nil {
			Return(c, result, errs.ErrCbidFormat)

			return
		}

		direct := c.Query("direct")
		if direct == "" {
			Return(c, result, errs.ErrDirectEmpty)

			return
		}

		intDirect, err := strconv.Atoi(direct)
		if err != nil {
			Return(c, result, errs.ErrDirectFormat)

			return
		}

		data, err := s.assessmentU.CountAllStatus(c, intCbid, intBranchID, intRoleCode, intDirect)
		if err != nil {
			Return(c, result, err)

			return
		}

		for _, val := range data {
			value := assessmentStatus{
				StatusCode: val.StatusCode,
				StatusName: val.StatusName,
				Count:      val.StatusCount,
			}
			result = append(result, value)

		}

		Return(c, result, nil)
	}
}

type changeStatus struct {
	AssessmentID string `json:"assessment_id" binding:"required"`
	StatusCode   int    `json:"status_code"`
	Comment      string `json:"comment"`
}

// changeAssessmentStatus godoc
// @Router /assessment/change/status [POST]
// @Summary Change an Assessment By Status
// @Description API Change Assessment By Status
// @Tags Assessment Service
// @Produce json
// @Param request body changeStatus true "assessmentStatus"
// @Success 200 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) changeAssessmentStatus() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			response Result
			request  domain.AssessmentStatus
			data     changeStatus
		)
		if err := c.ShouldBindJSON(&data); err != nil {
			Return(c, response, errs.ErrValidation)
			return
		}
		request = domain.AssessmentStatus{
			ID:         data.AssessmentID,
			StatusCode: data.StatusCode,
			Comment:    data.Comment,
		}

		if err := s.assessmentU.UpdateStatus(c, request); err != nil {
			Return(c, response, err)

			return
		}
		response.Result = true

		Return(c, response, nil)
	}
}

type reportData struct {
	CardID                string    `json:"card_id"`
	EmpBranchID           string    `json:"emp_branch_id"`
	EmpName               string    `json:"emp_name"`
	EmpCBID               int       `json:"emp_cbid"`
	OwnerFullName         string    `json:"owner_full_name"`
	OwnerPinfl            string    `json:"owner_pinfl"`
	TransportName         string    `json:"transport_name"`
	TransportVIN          string    `json:"transport_vin"`
	TransportPlateNum     string    `json:"transport_plate_num"`
	TransportYear         int       `json:"transport_year"`
	TransportMileage      int       `json:"transport_mileage"`
	TransportCondition    string    `json:"transport_condition"`
	TransportFuel         string    `json:"transport_fuel"`
	TransportMarketPrice  int       `json:"transport_market_price"`
	TransportCreditPrice  int       `json:"transport_credit_price"`
	AssessmentUpdatedTime time.Time `json:"assessment_updated_time"`
	AnalogCount           string    `json:"analog_count"`
	AnalogData            []Analog  `json:"analog_data"`
}

type Analog struct {
	ID          string `json:"a_id"`
	Name        string `json:"name"`
	Year        int    `json:"year"`
	Speedometer int    `json:"speedometer"`
	Price       int    `json:"price"`
	AnalogUrl   string `json:"analog_url"`
}

// getReportData godoc
// @Router /assessment/get/report [GET]
// @Summary Get Report Data
// @Description API To Get Report Data
// @Tags Assessment Service
// @Produce json
// @Param assessmentID query string true "assessmentID"
// @Success 201 {object} Response{data=reportData}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) getReportData() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			result reportData
		)
		assessmentID := c.Query("assessmentID")
		if assessmentID == "" {
			Return(c, result, errs.ErrAssessmentIDEmpty)

			return
		}

		if !utils.IsValidUUID(assessmentID) {
			Return(c, result, errs.ErrAssessmentIDFormat)

			return
		}

		data, err := s.assessmentU.GetReportData(c, assessmentID)
		if err != nil {
			Return(c, result, err)
			return
		}
		result = reportData{
			CardID:                data.CardID,
			EmpBranchID:           data.EmpBranchID,
			EmpName:               data.EmpName,
			EmpCBID:               data.EmpCBID,
			OwnerFullName:         data.OwnerFullName,
			OwnerPinfl:            data.OwnerPinfl,
			TransportName:         data.Name,
			TransportVIN:          data.VIN,
			TransportPlateNum:     data.PlateNum,
			TransportYear:         data.MadeYear,
			TransportMileage:      data.Transport.Speedometer,
			TransportCondition:    codeToValidity(data.Transport.Validity),
			TransportFuel:         data.Transport.Fuel,
			TransportMarketPrice:  data.Transport.Price,
			TransportCreditPrice:  data.Transport.SellPrice,
			AssessmentUpdatedTime: data.UpdatedAt,
		}
		for index, val := range data.Analog {
			aData := Analog{
				ID:          val.ID,
				Name:        val.Name,
				Year:        val.Year,
				Speedometer: val.Speedometer,
				Price:       val.Price,
				AnalogUrl:   val.SourceFile,
			}
			if index < 5 {

				result.AnalogData = append(result.AnalogData, aData)
			}

		}

		result.AnalogCount = getCount(len(data.Analog))

		Return(c, result, nil)
	}
}

type analogList struct {
	ID            string `json:"id"`
	Model         string `json:"model"`
	Name          string `json:"name"`
	Position      string `json:"position"`
	Speedometer   int    `json:"speedometer"`
	Year          int    `json:"year"`
	Fuel          string `json:"fuel"`
	SellPrice     int    `json:"sell_price"`
	Price         int    `json:"price"`
	Validity      string `json:"validity"`
	SourceFileUrl string `json:"source_file_url"`
	SourceFile    string `json:"source_file"`
}

// analogsList godoc
// @Router /assessment/analogs/list [GET]
// @Summary Get all Analogs list with assessmentID
// @Description API To Get all Analogs list with assessmentID
// @Tags Assessment Service
// @Produce json
// @Param assessmentID query string true "assessmentID"
// @Success 201 {object} Response{data=[]analogList}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) analogsList() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			result []analogList
		)
		assessmentID := c.Query("assessmentID")
		if assessmentID == "" {
			Return(c, result, errs.ErrAssessmentIDEmpty)

			return
		}

		if !utils.IsValidUUID(assessmentID) {
			Return(c, result, errs.ErrAssessmentIDFormat)

			return
		}

		data, err := s.assessmentU.AnalogsList(c, assessmentID)
		if err != nil {
			Return(c, result, err)
			return
		}

		for _, val := range data {
			aData := analogList{
				ID:            val.ID,
				Model:         val.Model,
				Name:          val.Name,
				Position:      val.Position,
				Year:          val.Year,
				Speedometer:   val.Speedometer,
				SellPrice:     val.SellPrice,
				Price:         val.Price,
				Fuel:          val.Fuel,
				Validity:      val.Condition,
				SourceFile:    val.SourceFile,
				SourceFileUrl: val.SourceFileUrl,
			}
			result = append(result, aData)
		}
		Return(c, result, nil)
	}
}

type appTransportInfo struct {
	TransportModel       string `json:"transport_model"`
	TransportName        string `json:"transport_name"`
	TransportPosition    string `json:"transport_position"`
	TransportYear        int    `json:"transport_year"`
	TransportSpeedometer int    `json:"transport_speedometer"`
	TransportValidity    string `json:"transport_condition"`
	TransportFuel        string `json:"transport_fuel"`
	TransportPrice       int    `json:"market_price"`
	TransportSellPrice   int    `json:"credit_price"`
}

type govUzData struct {
	OwnerPinfl               string `json:"owner_pinfl"`
	OwnerFullname            string `json:"owner_fullname"`
	OwnerType                int    `json:"owner_type"`
	OwnerDateBirth           string `json:"owner_date_birth"`
	VehicleID                int    `json:"vehicle_id"`
	VehiclePlateNumber       string `json:"vehicle_plate_number"`
	VehicleModel             string `json:"vehicle_model"`
	VehicleColor             string `json:"vehicle_color"`
	VehicleRegistrationDate  string `json:"vehicle_registration_date"`
	IibPermitInfo            string `json:"iib_permit_info"`
	Division                 string `json:"division"`
	GarovPermitInfo          string `json:"garov_permit_info"`
	VehicleMadeYear          int    `json:"vehicle_made_year"`
	VehicleType              int    `json:"vehicle_type"`
	VehicleKuzov             string `json:"vehicle_kuzov"`
	VehicleFullWeight        int    `json:"vehicle_full_weight"`
	VehicleEmptyWeight       int    `json:"vehicle_empty_weight"`
	VehicleMotor             string `json:"vehicle_motor"`
	VehicleFuelType          int    `json:"vehicle_fuel_type"`
	VehicleSeats             int    `json:"vehicle_seats"`
	VehicleStands            int    `json:"vehicle_stands"`
	Inspection               string `json:"inspection"`
	VehicleTexpassportSeria  string `json:"vehicle_texpassport_seria"`
	VehicleTexpassportNumber string `json:"vehicle_texpassport_number"`
	BodyTypeName             string `json:"body_type_name"`
	VehicleShassi            string `json:"vehicle_shassi"`
	VehiclePower             int    `json:"vehicle_power"`
	DateSchetSpravka         string `json:"date_schet_spravka"`
	TuningPermit             string `json:"tuning_permit"`
	TuningGivenDate          string `json:"tuning_given_date"`
	TuningIssueDate          string `json:"tuning_issue_date"`
	PrevPnfl                 string `json:"prev_pnfl"`
	PrevOwner                string `json:"prev_owner"`
	PrevOwnerType            string `json:"prev_owner_type"`
	PrevPlateNumber          string `json:"prev_plate_number"`
	PrevTexpasportSery       string `json:"prev_texpasport_seria"`
	PrevTexpasportNumber     string `json:"prev_texpasport_number"`
	State                    string `json:"state"`
}

type assessmentDataWithGovInfo struct {
	AssessmentID string           `json:"assessment_id"`
	CBID         int              `json:"cbid"`
	Transport    appTransportInfo `json:"transport_info"`
	GovUzData    govUzData        `json:"gov_uz_data"`
}

// getReportData godoc
// @Router /assessment/data [GET]
// @Summary Get Assessment Data
// @Description API To Get Assessment Data
// @Tags Outsource Integration
// @Produce json
// @Param cbid query string true "cbid"
// @Param tex_passport_seria query string true "tex_passport_seria"
// @Param tex_passport_number query string true "tex_passport_number"
// @Success 201 {object} Response{data=assessmentDataWithGovInfo}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) getAssessmentData() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			result assessmentDataWithGovInfo
		)
		cbid := c.Query("cbid")
		if cbid == "" {
			Return(c, result, errs.ErrAssessmentIDEmpty)

			return
		}
		texPassportSeria := c.Query("tex_passport_seria")
		if texPassportSeria == "" {
			Return(c, result, errs.ErrAssessmentIDEmpty)

			return
		}
		texPassportNumber := c.Query("tex_passport_number")
		if texPassportNumber == "" {
			Return(c, result, errs.ErrAssessmentIDEmpty)

			return
		}
		resp, err := s.assessmentU.GetAssessmentData(c, cbid, texPassportSeria, texPassportNumber)
		if err != nil {
			Return(c, result, err)
			return
		}

		intCBID, _ := strconv.Atoi(resp.CBID)

		result = assessmentDataWithGovInfo{
			AssessmentID: resp.AssessmentID,
			CBID:         intCBID,
			Transport: appTransportInfo{
				TransportModel:       resp.Transport.Model,
				TransportName:        resp.Transport.Name,
				TransportPosition:    resp.Transport.Position,
				TransportYear:        resp.Transport.Year,
				TransportSpeedometer: resp.Transport.Speedometer,
				TransportValidity:    codeToValidity(resp.Transport.Validity),
				TransportFuel:        resp.Transport.Fuel,
				TransportPrice:       resp.Transport.Price,
				TransportSellPrice:   resp.Transport.SellPrice,
			},
			GovUzData: govUzData{
				OwnerPinfl:               resp.GovUzStore.Pinfl,
				OwnerFullname:            resp.GovUzStore.OwnerFullname,
				OwnerType:                resp.GovUzStore.OwnerType,
				OwnerDateBirth:           resp.GovUzStore.OwnerDateBirth,
				VehicleID:                resp.GovUzStore.VehicleID,
				VehiclePlateNumber:       resp.GovUzStore.PlateNumber,
				VehicleModel:             resp.GovUzStore.Model,
				VehicleColor:             resp.GovUzStore.VehicleColor,
				VehicleRegistrationDate:  resp.GovUzStore.RegistrationDate,
				IibPermitInfo:            resp.GovUzStore.IibPermitInfo,
				Division:                 resp.GovUzStore.Division,
				GarovPermitInfo:          resp.GovUzStore.GarovPermitInfo,
				VehicleMadeYear:          resp.GovUzStore.Year,
				VehicleType:              resp.GovUzStore.VehicleType,
				VehicleKuzov:             resp.GovUzStore.Kuzov,
				VehicleFullWeight:        resp.GovUzStore.FullWeight,
				VehicleEmptyWeight:       resp.GovUzStore.EmptyWeight,
				VehicleMotor:             resp.GovUzStore.Motor,
				VehicleFuelType:          resp.GovUzStore.FuelType,
				VehicleSeats:             resp.GovUzStore.Seats,
				VehicleStands:            resp.GovUzStore.Stands,
				Inspection:               resp.GovUzStore.Inspection,
				VehicleTexpassportSeria:  resp.GovUzStore.TexpassportSeria,
				VehicleTexpassportNumber: resp.GovUzStore.TexpassportNumber,
				BodyTypeName:             resp.GovUzStore.BodyTypeName,
				VehicleShassi:            resp.GovUzStore.Shassi,
				VehiclePower:             resp.GovUzStore.Power,
				DateSchetSpravka:         resp.GovUzStore.DateSchetSpravka,
				TuningPermit:             resp.GovUzStore.TuningPermit,
				TuningGivenDate:          resp.GovUzStore.TuningGivenDate,
				TuningIssueDate:          resp.GovUzStore.TuningIssueDate,
				PrevPnfl:                 resp.GovUzStore.PrevPnfl,
				PrevOwner:                resp.GovUzStore.PrevOwner,
				PrevOwnerType:            resp.GovUzStore.PrevOwnerType,
				PrevPlateNumber:          resp.GovUzStore.PrevPlateNumber,
				PrevTexpasportSery:       resp.GovUzStore.PrevTexpasportSery,
				PrevTexpasportNumber:     resp.GovUzStore.PrevTexpasportNumber,
				State:                    resp.GovUzStore.State,
			},
		}

		Return(c, result, nil)
	}
}

// deleteAssessment godoc
// @Router /assessment/delete [DELETE]
// @Summary Delete Assessment
// @Description API Delete Assessment
// @Tags Assessment Service
// @Produce json
// @Param id query string true "id"
// @Success 201 {object} Response{data=Result}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) deleteAssessment() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			result Result
		)

		ID := c.Query("id")
		if ID == "" {
			Return(c, result, errs.ErrAssessmentIDEmpty)
			return
		}

		if !utils.IsValidUUID(ID) {
			Return(c, result, errs.ErrAssessmentIDFormat)
			return

		}

		resp, err := s.assessmentU.DeleteAssessment(c, ID)
		if err != nil {
			Return(c, result, err)
			return
		}

		result.Result = resp.Result

		Return(c, result, nil)
	}
}

func getCount(index int) string {
	var (
		maxAnalog    int
		reportAnalog int
	)
	if index > 5 {
		maxAnalog = index
		reportAnalog = 5
	} else {
		maxAnalog = index
		reportAnalog = index
	}
	defaultMessage := fmt.Sprintln("Mazkur mulkni baholash uchun “ONLAYN BAHO” dasturidagi ", maxAnalog, " ta analoglardan foydalanildi. Mazkur xulosada ma’lumot uchun ", reportAnalog, " tasi ko’rsatilgan bo’lib foydalanilgan analoglar bilan  “ONLAYN BAHO” dasturi orqali tanishishingiz mumkin.")

	return defaultMessage
}
func codeToValidity(val int) string {
	switch val {
	case 0:
		return "Yangi"
	case 1:
		return "Ishchi holatida"
	case 2:
		return "Ta'mirdan chiqqan"
	case 3:
		return "Ta'mirtalab"
	default:
		return "No'malum Qiymat"
	}
}

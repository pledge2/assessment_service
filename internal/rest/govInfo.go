package rest

import (
	"pledge/assessment_service/internal/domain"
	"pledge/assessment_service/internal/errs"
	"pledge/assessment_service/pkg/utils"

	"github.com/gin-gonic/gin"
)

type govUz struct {
	VehicleKuzov       string `json:"vehicle_kuzov"`
	OwnerPinfl         string `json:"owner_pinfl"`
	OwnerFullname      string `json:"owner_fullname"`
	VehicleModel       string `json:"vehicle_model"`
	VehicleColor       string `json:"vehicle_color"`
	VehicleMadeYear    int    `json:"vehicle_made_year"`
	VehicleMotor       string `json:"vehicle_motor"`
	VehicleFullWeight  int    `json:"vehicle_full_weight"`
	VehicleEmptyWeight int    `json:"vehicle_empty_weight"`
	VehiclePlateNumber string `json:"vehicle_plate_number"`
	PassportNumber     string `json:"passport_number"`
	PassportSeria      string `json:"passport_seria"`
	VinNumber          string `json:"vin_number"`
	VehiclePower       int    `json:"vehicle_power"`
	Seats              int    `json:"seats"`
	VehicleShassi      string `json:"vehicle_shassi"`
}

type techNumberParams struct {
	AssessmentID   string `json:"assessment_id" binding:"required"`
	PassportNumber string `json:"passport_number" binding:"required"`
	PassportSeria  string `json:"passport_seria" binding:"required"`
	PlateNumber    string `json:"plate_number" binding:"required"`
}

// searchByCarNum godoc
// @Router /assessment/govuz/carinfo [POST]
// @Summary Integration with GOV
// @Description Integration with Technical Passport and Car Number
// @Tags Integration with GovUz
// @Produce json
// @Param request body techNumberParams true "Official Assessment details"
// @Success 201 {object} Response{data=govUz}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) searchByCarNum() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data     techNumberParams
			request  domain.GovUzParams
			response govUz
		)

		if err := c.ShouldBindJSON(&data); err != nil {
			Return(c, response, errs.ErrValidation)
			return
		}

		request = domain.GovUzParams{
			AssessmentID:   data.AssessmentID,
			PassportNumber: data.PassportNumber,
			PassportSeria:  data.PassportSeria,
			PlateNumber:    data.PlateNumber,
		}

		info, err := s.govInfoU.SearchByCarNum(c, request)
		if err != nil {
			Return(c, response, err)
			return
		}

		response = govUz{
			OwnerFullname:      info.OwnerFullname,
			OwnerPinfl:         info.OwnerPinfl,
			VehicleMadeYear:    info.VehicleMadeYear,
			VehiclePlateNumber: info.VehiclePlateNumber,
			VehicleModel:       info.VehicleModel,
			VehicleColor:       info.VehicleColor,
			VehicleKuzov:       info.VehicleKuzov,
			VehicleFullWeight:  info.VehicleFullWeight,
			VehicleEmptyWeight: info.VehicleEmptyWeight,
			VehicleMotor:       info.VehicleMotor,
			PassportNumber:     info.PassportNumber,
			PassportSeria:      info.PassportSeria,
			VinNumber:          info.VinNumber,
			VehiclePower:       info.VehiclePower,
			Seats:              info.Seats,
			VehicleShassi:      info.Shassi,
		}
		Return(c, response, nil)
	}
}

// updateGovData godoc
// @Router /update/gov/data [PUT]
// @Summary Updated GovUz Data
// @Description Updated GovUz Data with AssessmentID and GovData
// @Tags Integration with GovUz
// @Produce json
// @Param assessment_id query string true "assessment_id"
// @Param request body govUz true "Gov Data"
// @Success 201 {object} Response{data=govUz}
// @Failure 400 {object} Response{data}
// @Failure 500 {object} Response{data}
func (s *Server) updateGovData() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			data   govUz
			result Result
		)

		assessmentID := c.Query("assessment_id")
		if assessmentID == "" {
			Return(c, result, errs.ErrAssessmentIDEmpty)
			return
		}

		if !utils.IsValidUUID(assessmentID) {
			Return(c, result, errs.ErrAssessmentIDFormat)
			return
		}

		if err := c.ShouldBindJSON(&data); err != nil {
			Return(c, result, errs.ErrValidation)
			return
		}

		request := domain.GovUzStore{
			OwnerFullname:     data.OwnerFullname,
			Pinfl:             data.OwnerPinfl,
			Year:              data.VehicleMadeYear,
			PlateNumber:       data.VehiclePlateNumber,
			Model:             data.VehicleModel,
			VehicleColor:      data.VehicleColor,
			Kuzov:             data.VehicleKuzov,
			FullWeight:        data.VehicleFullWeight,
			EmptyWeight:       data.VehicleEmptyWeight,
			Motor:             data.VehicleMotor,
			TexpassportNumber: data.PassportNumber,
			TexpassportSeria:  data.PassportSeria,
			Power:             data.VehiclePower,
			Shassi:            data.VehicleShassi,
		}
		if err := s.govInfoU.UpdateGovInfoData(c, assessmentID, request); err != nil {
			Return(c, result, err)
			return
		}

		result.Result = true

		Return(c, result, nil)
	}
}

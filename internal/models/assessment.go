package models

import (
	"database/sql"
	"time"
)

type IIBInfo struct {
	ID                  string    `json:"-"` // baxolash id
	OwnerType           int       `json:"owner_type"`
	OwnerInps           string    `json:"owner_inps"`
	OwnerFullname       string    `json:"owner_fullname"`
	OwnerDateBirth      string    `json:"owner_date_birth"`
	VehicleID           int       `json:"vehicle_id"`
	VehiclePlateNumber  string    `json:"vehicle_plate_number"`
	VehicleModel        string    `json:"vehicle_model"`
	VehicleColor        string    `json:"vehicle_color"`
	IIBRegistrationDate time.Time `json:"iib_registration_date"`
	Division            string    `json:"division"`
	VehicleYear         string    `json:"vehicle_year"`
	VehicleType         string    `json:"vehicle_type"`
	VehicleKuzov        string    `json:"vehicle_kuzov"`
	VehicleFullWeight   string    `json:"vehicle_full_weight"`
	VehicleEmptyWeight  string    `json:"vehicle_empty_weight"`
	VehicleMotor        string    `json:"vehicle_motor"`
	VehicleEngineNumber string    `json:"vehicle_engine_number"`
	VehicleFuelType     string    `json:"vehicle_fuel_type"`
	VehicleSeats        string    `json:"vehicle_seats"`
	VehicleStands       string    `json:"vehicle_stands"`
	IIBPermitInfo       string    `json:"iib_permit_info"`
	Inspection          string    `json:"inspection"`
}

type MainInfo struct {
	ID            string    `json:"-"`                                                   // baxolash id
	CBID          int       `json:"cbid" example:"56833"`                                // user id
	AID           []string  `json:"-"`                                                   // analoglar id
	PID           string    `json:"p_id" example:"c02ff8d9-1596-4c1e-a4b1-a8a8e7220987"` // product id
	GID           string    `json:"-"`                                                   //Analog table bilan transport_deatils table ni reference_key
	IIBInfoID     string    `json:"-"`
	Status        int       `json:"-"` // baxolash statusi, xolati
	Type          string    `json:"type" example:"03feadec-8370-4355-800c-e88ac1ee2684"`
	Model         string    `json:"model" example:"559c5b35-3767-43e0-b175-4938f88191c1"`
	Name          string    `json:"name" example:"c6699f5b-dee2-43b3-a780-d6cc325d89c1"`
	Position      string    `json:"position" example:"ce21d58b-e1a3-49f8-b4fb-720c0fc802a3"`
	Year          int       `json:"year" example:"2023"`
	Speedometer   int       `json:"speedometer" example:"10000"`
	SpeedometerAt time.Time `json:"-"`
	Validity      int       `json:"validity" example:"1"`
	Fuel          string    `json:"fuel" example:"9fdc6329-6a84-43b4-ac2a-0847cc48ff84"`
	Price         int       `json:"-"`
	PriceAt       time.Time `json:"-"`
	Source        AllSource `json:"-"`
}

type OfficialAssessment struct {
	ID string `json:"id"`
}

type AssessmentInfo struct {
	ID              string         `db:"id"`
	CBID            int            `db:"cbid"`
	GID             sql.NullString `db:"g_id"`
	IIBInfoID       sql.NullString `db:"iib_info_id"`
	Status          int            `db:"status"`
	VehicleMadeYear string         `db:"vehicle_made_year"`
	VehicleFuelType string         `db:"vehicle_fuel_year_type"`
	Type            string         `json:"type"`
	Model           string         `json:"model"`
	Name            string         `json:"name"`
	Position        string         `json:"position"`
	Year            int            `json:"year"`
	Speedometer     int            `json:"speedometer"`
	Validity        int            `json:"validity"`
	Fuel            string         `json:"fuel"`
}

type Assessment struct {
	ID                  string         `json:"id"`   // baxolash id
	CBID                string         `json:"cbid"` // user id
	AID                 []string       `json:"a_id"` // analoglar id
	PID                 string         `json:"p_id"` // product id
	GID                 string         `json:"-"`    //Analog table bilan transport_deatils table ni reference_key
	IIBInfoID           string         `json:"iib_info_id"`
	Status              int            `json:"status"` // baxolash statusi, xolati
	VehicleKuzov        string         `json:"vehicle_kuzov"`
	OwnerPinfl          string         `json:"owner_pinfl"`
	OwnerFullname       string         `json:"owner_fullname"`
	VehicleModel        string         `json:"vehicle_model"`
	VehicleColor        string         `json:"vehicle_color"`
	VehicleMadeYear     string         `json:"vehicle_year"`
	VehicleMotor        string         `json:"vehicle_motor"`
	VehicleFullWeight   string         `json:"vehicle_full_weight"`
	VehicleEmptyWeight  string         `json:"vehicle_empty_weight"`
	VehiclePlateNumber  string         `json:"vehicle_plate_number"`
	VehicleEngineNumber string         `json:"vehicle_engine_number"`
	PassportNumber      string         `json:"passport_number"`
	PassportSeria       string         `json:"passport_seria"`
	VinNumber           string         `json:"vin_number"`
	Type                string         `json:"type"`
	Model               string         `json:"model"`
	Name                string         `json:"name"`
	Position            string         `json:"position"`
	Year                int            `json:"year"`
	Speedometer         int            `json:"speedometer"`
	Validity            int            `json:"validity"`
	Fuel                string         `json:"fuel"`
	Price               int            `json:"price"`
	MarketPrice         int            `json:"market_price"`
	CreditPrice         int            `json:"credit_price"`
	TransportModel      string         `json:"transport_model"`
	TransportName       string         `json:"transport_name"`
	TransportPosition   string         `json:"transport_position"`
	TransportYear       int            `json:"transport_year"`
	Source              AllPhotoSource `json:"source"`
	Url                 string         `json:"url"`
	IIBPermitInfo       string         `json:"iib_permit_info"`
}

type Transport struct {
	ID            string         `json:"-"`
	Type          string         `json:"type"`
	Model         string         `json:"model"`
	Name          string         `json:"name"`
	Position      string         `json:"position"`
	Year          int            `json:"year"`
	Speedometer   int            `json:"speedometer"`
	SpeedometerAt time.Time      `json:"speedometer_at"`
	Validity      int            `json:"validity"`
	Fuel          string         `json:"fuel"`
	Price         int            `json:"price"`
	PriceAt       time.Time      `json:"price_at"`
	Source        AllPhotoSource `json:"source"`
}
type Source struct {
	LAT       string `json:"lat"`
	LON       string `json:"lon"`
	PhotoName string `json:"photo_name"`
	Title     string `json:"title"`
	Order     int    `json:"order"`
}

type AllSource struct {
	Source []Source `json:"source"`
}

type AllPhotoSource struct {
	PhotoSource []PhotoSource `json:"photo_source"`
}

type PhotoSource struct {
	Title    string `json:"title"`
	Order    int    `json:"order"`
	PhotoUrl string `json:"photo_url"`
	LAT      string `json:"lat"`
	LON      string `json:"lon"`
}

type AssessmentRequest struct {
	Type        string `json:"type"`
	Model       string `json:"model"`
	Name        string `json:"name"`
	Year        int    `json:"year"`
	Speedometer int    `json:"speedometer"`
	Validity    int    `json:"validity"`
	Position    string `json:"position"`
	Fuel        string `json:"fuel"`
}
type AssessmentResponse struct {
	MarketPrice       int    `json:"market_price"`
	CreditPrice       int    `json:"credit_price"`
	TransportModel    string `json:"transport_model"`
	TransportName     string `json:"transport_name"`
	TransportPosition string `json:"transport_position"`
	TransportYear     int    `json:"transport_year"`
}

type AssessmentResp struct {
	ID            string         `json:"id"`     // baxolash id
	CBID          string         `json:"cbid"`   // user id
	AID           []string       `json:"a_id"`   // analoglar id
	PID           string         `json:"p_id"`   // product id
	GID           string         `json:"g_id"`   //Analog table bilan transport_deatils table ni reference_key
	Status        int            `json:"status"` // baxolash statusi, xolati
	Type          string         `json:"type"`
	Model         string         `json:"model"`
	Name          string         `json:"name"`
	Position      string         `json:"position"`
	Year          int            `json:"year"`
	Speedometer   int            `json:"speedometer"`
	SpeedometerAt time.Time      `json:"speedometer_at"`
	Validity      int            `json:"validity"`
	Fuel          string         `json:"fuel"`
	Price         int            `json:"price"`
	PriceAt       time.Time      `json:"price_at"`
	Source        AllPhotoSource `json:"source"`
}

type AssessmentByStatus struct {
	ID                 string         `json:"id"`                   // baxolash id
	CBID               string         `json:"cbid"`                 // user id
	GID                string         `json:"g_id"`                 //Analog table bilan transport_deatils table ni reference_key
	Status             int            `json:"-"`                    // baxolash statusi, xolati
	OwnerFullName      string         `json:"owner_fullname"`       //iib_info table
	VehiclePlateNumber string         `json:"vehicle_plate_number"` //iib_info table
	VehicleKuzov       string         `json:"vehicle_kuzov"`        //iib_info table
	Name               string         `json:"-"`
	TrasnportName      string         `json:"transport_name"`
	Source             AllPhotoSource `json:"source"`
	CreatedAt          time.Time      `json:"created_at"`
}

type AssessmentResult struct {
	Assessments []*AssessmentByStatus
	TotalPages  int    `json:"total_pages"`
	StatusName  string `json:"status_name"`
}

type StatusCount struct {
	ID         string `json:"-"`
	Status     int    `json:"status_code"`
	StatusName string `json:"status_name"`
	Count      string `json:"count"`
}

type StatusCode struct {
	Code  int `json:"code"`
	Count int `json:"count"`
}

type StatusRes struct {
	// ID         string `json:"id"`
	StatusCode int    `json:"status_code"`
	StatusName string `json:"status_name"`
	Count      int    `json:"count"`
	// Url        string `json:"-"`
	// CreateAt   time.Time `json:"-"`
	// UpdateAt   time.Time `json:"-"`
}

type Result struct {
	Result bool `json:"result"`
}

type GovTechnicalPass struct {
	AssessmentID   string `json:"assessment_id"`
	PassportNumber string `json:"passport_number"`
	PassportSeria  string `json:"passport_seria"`
}

type GovVinNumber struct {
	AssessmentID string `json:"assessment_id"`
	VinNumber    string `json:"vin_number"`
}
type GovDBResponse struct {
	VinNumber string `json:"vin_number"`
	// PassportNumber     string `json:"passport_number"`
	// PassportSeria      string `json:"passport_seria"`
	OwnerFullname       string `json:"owner_fullname"`
	OwnerPinfl          string `json:"owner_pinfl"`
	VehicleModel        string `json:"vehicle_model"`
	VehicleColor        string `json:"vehicle_color"`
	VehicleYear         string `json:"vehicle_year"`
	VehicleMotor        string `json:"vehicle_motor"`
	VehicleFullWeight   string `json:"vehicle_full_weight"`
	VehicleEmptyWeight  string `json:"vehicle_empty_weight"`
	VehiclePlateNumber  string `json:"vehicle_plate_number"`  //iib_info table
	VehicleEngineNumber string `json:"vehicle_engine_number"` //iib_info table
	IIBPermitInfo       string `json:"iib_permit_info"`
}

type GovResponse struct {
	Result            int                 `json:"result"`
	Comment           string              `json:"comment"`
	OwnerType         int                 `json:"owner_type"`
	Pinpp             string              `json:"pinpp"`
	OwnerInn          string              `json:"owner_inn"`
	OwnerFullname     string              `json:"owner"`
	Surname           string              `json:"surname"`
	Name              string              `json:"name"`
	Patronym          string              `json:"patronym"`
	DateBirth         string              `json:"date_birth"`
	VehicleInfoResult []VehicleInfoResult `json:"vehicle_info_result"`
}

type VehicleInfoResult struct {
	VehicleID        int    `json:"vehicle_id"`
	PlateNumber      string `json:"plate_number"`
	Model            string `json:"model"`
	VehicleColor     string `json:"vehicle_color"`
	RegistrationDate string `json:"registration_date"`
	Division         string `json:"division"`
	Year             string `json:"year"`
	VehicleType      string `json:"vehicle_type"`
	Kuzov            string `json:"kuzov"`
	FullWeight       string `json:"full_weight"`
	EmptyWeight      string `json:"empty_weight"`
	Motor            string `json:"motor"`
	FuelType         string `json:"fuel_type"`
	Seats            string `json:"seats"`
	Stands           string `json:"stands"`
	Comments         string `json:"comments"`
	Inspection       string `json:"inspection"`
}

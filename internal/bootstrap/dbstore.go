package bootstrap

import (
	"database/sql"

	assessment_store "pledge/assessment_service/internal/drivers/dbstore/assessment"
	gov_info_store "pledge/assessment_service/internal/drivers/dbstore/gov_info"
	status_store "pledge/assessment_service/internal/drivers/dbstore/status"
	"pledge/assessment_service/pkg/logger"
)

type Repos struct {
	assessment assessment_store.AssessmentRepo
	gov_info   gov_info_store.GovInfoRepo
	status     status_store.StatusRepo
}

func initRepos(db *sql.DB, log logger.Logger) Repos {

	assessmentRepo := assessment_store.New(db, log)
	govInfoRepo := gov_info_store.New(db, log)
	statusRepo := status_store.New(db, log)

	return Repos{
		assessment: *assessmentRepo,
		gov_info:   *govInfoRepo,
		status:     *statusRepo,
	}
}

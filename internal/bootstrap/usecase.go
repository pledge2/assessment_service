package bootstrap

import (
	assessment_usecase "pledge/assessment_service/internal/usecase/assessment"
	gov_info_usecase "pledge/assessment_service/internal/usecase/gov_info"
	status_usecase "pledge/assessment_service/internal/usecase/status"
	"pledge/assessment_service/pkg/logger"
)

type usecases struct {
	statusU     status_usecase.Usecase
	assessmentU assessment_usecase.Usecase
	govInfoU    gov_info_usecase.Usecase
}

func initUsecases(repo Repos, client Clients, log logger.Logger) usecases {
	statusUsecase := status_usecase.New(&repo.status, log)
	assessmentUsecase := assessment_usecase.New(&repo.assessment, &repo.gov_info, &client.transport, &client.analog, &client.reference, &repo.status, &client.identification, log)
	govInfoUsecase := gov_info_usecase.New(&client.gov, &repo.gov_info, &repo.assessment, log)
	return usecases{
		statusU:     *statusUsecase,
		assessmentU: *assessmentUsecase,
		govInfoU:    *govInfoUsecase,
	}
}

package bootstrap

import (
	"pledge/assessment_service/internal/config"
	"pledge/assessment_service/internal/drivers/service/analog_service"
	"pledge/assessment_service/internal/drivers/service/gov_info_service"
	"pledge/assessment_service/internal/drivers/service/identification_service"
	"pledge/assessment_service/internal/drivers/service/reference_service"
	"pledge/assessment_service/internal/drivers/service/transport_service"
	"pledge/assessment_service/pkg/logger"
)

type Clients struct {
	transport      transport_service.Client
	analog         analog_service.Client
	reference      reference_service.Client
	gov            gov_info_service.Client
	identification identification_service.Client
}

func initClients(cfg config.Config, log logger.Logger) Clients {
	analogClient := analog_service.New(cfg, log)
	transportClient := transport_service.New(cfg, log)
	referenceClient := reference_service.New(cfg, log)
	govClient := gov_info_service.New(cfg, log)
	identificationClient := identification_service.New(cfg, log)
	return Clients{
		transport:      *transportClient,
		analog:         *analogClient,
		reference:      *referenceClient,
		gov:            *govClient,
		identification: *identificationClient,
	}
}

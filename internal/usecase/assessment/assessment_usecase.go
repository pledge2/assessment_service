package assessment_usecase

import (
	"context"
	"fmt"
	"math"
	"pledge/assessment_service/internal/domain"
	"pledge/assessment_service/internal/errs"
	"pledge/assessment_service/pkg/logger"
	"pledge/assessment_service/pkg/utils"
	"strings"
	"time"
)

const (
	expertiza = 3
	discount  = 6
	completed = 7
)
const (
	cardUrl = "/officialAssessment"
)

const (
	new        = 0
	used       = 0.025
	repaired   = 0.05
	needRepair = 0.10
)
const (
	speed0  = 0
	speed10 = 0.01
	speed15 = 0.02
	speed20 = 0.03
	speed25 = 0.04
	speed35 = 0.05
	speed45 = 0.06
	speed55 = 0.07
	speed65 = 0.08
	speed75 = 0.09
	speed80 = 0.10
)
const (
	year0 = 0
	year1 = 0.05
	year2 = 0.10
	year3 = 0.15
	year4 = 0.20
	year5 = 0.25
	year6 = 100
)

type assessmentRepo interface {
	Store(ctx context.Context, data domain.AssessmentStore) (string, error)
	Find(ctx context.Context, id string) (domain.Assessment, error)
	GetInfo(ctx context.Context, id string) (domain.Assessment, error)
	FindAll(ctx context.Context) ([]domain.Assessment, error)
	CountAllStatus(ctx context.Context, cbid, branchID, roleCode, direct int) ([]domain.AssessmentStatus, error)
	FindByStatus(ctx context.Context, status, limit, offset, cbid, branchID, roleCode, direct int) ([]domain.Assessment, error)
	GetTotalAssessmentCount(ctx context.Context, status, cbid, branchID, roleCode, direct int) (int, error)
	GetAssessmentByBranchID(ctx context.Context, status, branchID int) ([]int, error)
	UpdateStatus(ctx context.Context, id string, status int) error
	CountAllAssessments(ctx context.Context) (int, error)
	GetReportData(ctx context.Context, id string) (domain.ReportData, error)
	UpdateAnalogID(ctx context.Context, req domain.AssessmentStore) error
	GetGovInfoByAssessmentIDAndStatus(ctx context.Context, cbid string, texPassSeria, texPassNum string) (domain.GovInfoWithCBID, error)
	DeleteAssessment(ctx context.Context, id string) error
}

type statusRepo interface {
	FindByStatusCode(ctx context.Context, status int) (domain.Status, error)
	FindStatuses(ctx context.Context) ([]domain.Status, error)
}

type govInfoRepo interface {
	Store(ctx context.Context, data domain.GovUzStore) (string, error)
}

type iTransport interface {
	Store(ctx context.Context, a domain.Transport) (string, error)
	Find(ctx context.Context, id string) (domain.Transport, error)
	GetPhotos(ctx context.Context, id, year string) (domain.AllPhotosUrl, error)
	StorePhoto(ctx context.Context, id, folder string, data domain.PhotoName) (domain.Result, error)
	UpdatePrice(ctx context.Context, id string, price, sellprice int) (domain.Result, error)
	DeleteTransport(ctx context.Context, id, year string) (domain.Result, error)
}

type iAnalog interface {
	AnalogFindOne(ctx context.Context, id string) (domain.Analog, error)
	AnalogFindView(ctx context.Context, id string) (domain.Analog, error)
	FindAnalog(ctx context.Context, data domain.Transport) ([]domain.AssessmentStore, error)
	ProductFindOne(ctx context.Context, id string) (domain.Product, error)
}

type iReference interface {
	GetTypeByID(ctx context.Context, id string) (domain.TransportType, error)
	GetModelByID(ctx context.Context, data string) (domain.TransportModel, error)
	GetNameByID(ctx context.Context, data string) (domain.TransportName, error)
	GetPositionByID(ctx context.Context, data string) (domain.TransportPosition, error)
	GetDiscount(ctx context.Context, nameID string) (domain.Discounts, error)
	ReferenceFindName(ctx context.Context, val domain.ReferenceNames) (domain.ReferenceNames, error)
	FuelControls(ctx context.Context, nameID string) (domain.FuelControls, error)
}

type IdentificationI interface {
	GetEmpInfo(ctx context.Context, cbid int) (domain.EmpData, error)
}

type Usecase struct {
	assessment     assessmentRepo
	govInfo        govInfoRepo
	transport      iTransport
	analog         iAnalog
	reference      iReference
	status         statusRepo
	identification IdentificationI
	log            logger.Logger
}

func New(assessment assessmentRepo, govInfo govInfoRepo, transport iTransport, analog iAnalog, reference iReference, status statusRepo, identification IdentificationI, log logger.Logger) *Usecase {
	return &Usecase{
		assessment:     assessment,
		govInfo:        govInfo,
		transport:      transport,
		analog:         analog,
		reference:      reference,
		status:         status,
		identification: identification,
		log:            log,
	}
}

func (a *Usecase) Store(ctx context.Context, data domain.AssessmentStore) (string, error) {
	var logMsg = "uc.Assessment.Create "

	req := domain.Transport{
		Type:        data.Transport.Type,
		Model:       data.Transport.Model,
		Name:        data.Transport.Name,
		Position:    data.Transport.Position,
		Year:        data.Transport.Year,
		Speedometer: data.Transport.Speedometer,
		Validity:    data.Transport.Validity,
		Fuel:        data.Transport.Fuel,
		SellPrice:   data.Transport.SellPrice,
		ExtraParam:  data.Transport.ExtraParam,
	}

	transportID, err := a.transport.Store(ctx, req)
	if err != nil {
		a.log.Error(logMsg+"a.transport.Store failed", logger.Error(err))

		return "", err
	}

	ogvInfo := domain.GovUzStore{}
	govID, err := a.govInfo.Store(ctx, ogvInfo)
	if err != nil {
		a.log.Error(logMsg+"a.govInfo.Store failed", logger.Error(err))

		return "", err
	}

	productName, err := a.analog.ProductFindOne(ctx, data.PID)
	if err != nil {
		a.log.Error(logMsg+"a.analog.ProductFindOne failed", logger.Error(err))

		return "", err
	}

	cardID, err := a.generateCardID(ctx, productName.Product)
	if err != nil {
		a.log.Error(logMsg+"a.generateCardID failed", logger.Error(err))

		return "", err
	}

	assessment := domain.AssessmentStore{
		CBID:      data.CBID,
		EXTCBID:   data.CBID,
		BranchID:  data.BranchID,
		AIDs:      data.AIDs,
		PID:       data.PID,
		GID:       transportID,
		Status:    data.Status,
		IIBInfoID: govID,
		CardID:    cardID,
		Direct:    data.Direct,
	}

	assessmentID, err := a.assessment.Store(ctx, assessment)
	if err != nil {
		a.log.Error(logMsg+"a.assessment.Store failed", logger.Error(err))

		return "", err
	}
	return assessmentID, nil
}

func (a *Usecase) generateCardID(ctx context.Context, productName string) (string, error) {
	var prefix string

	switch productName {
	case "Avtotransport":
		prefix = "T"
	case "Ko’chmas mulk":
		prefix = "J"
	case "TMZ":
		prefix = "TMZ"
	case "Uskunalar":
		prefix = "U"
	default:
		prefix = "X"
	}

	countS, err := a.assessment.CountAllAssessments(ctx)
	if err != nil {
		return "", nil
	}
	// Increment the count
	countS++

	// Format the count as a 4-digit number
	countStr := fmt.Sprintf("%04d", countS)

	currentDate := time.Now()

	// Get current month and format it as two digits
	month := fmt.Sprintf("%02d", currentDate.Month())

	// Get last two digits of the current year
	year := fmt.Sprintf("%02d", currentDate.Year()%100)

	// Create the final random number using the specified format
	finalNumber := fmt.Sprintf("%s%s%s%s", prefix, countStr, month, year)

	return finalNumber, nil
}

func (a *Usecase) StorePhoto(ctx context.Context, assessmentID, folder string, data domain.PhotoName) (domain.Result, error) {
	var (
		logMsg = "uc.Assessment.StorePhoto "
		result domain.Result
	)
	assessment, err := a.assessment.Find(ctx, assessmentID)
	if err != nil {
		a.log.Error(logMsg+"a.assessment.Find failed", logger.Error(err))

		return result, err
	}

	info, err := a.transport.StorePhoto(ctx, assessment.GID, folder, data)
	if err != nil {
		a.log.Error(logMsg+"a.transport.StorePhoto failed", logger.Error(err))

		return result, err
	}

	result = domain.Result{
		Result: info.Result,
	}

	return result, nil
}

func (a *Usecase) GetPhotos(ctx context.Context, assessmentID string) (domain.AllPhotosUrl, error) {
	var (
		logMsg = "uc.Assessment.GetPhotos "
		result domain.AllPhotosUrl
	)

	assessment, err := a.assessment.Find(ctx, assessmentID)
	if err != nil {
		a.log.Error(logMsg+"a.assessment.Find failed", logger.Error(err))

		return result, err
	}

	resultPhoto, err := a.transport.GetPhotos(ctx, assessment.GID, assessment.CreatedAt.Format("2006"))
	if err != nil {
		a.log.Error(logMsg+"a.transport.GetPhotos failed", logger.Error(err))

		return result, err
	}

	for _, photo := range resultPhoto.PhotoUrl {
		value := domain.PhotoUrl{
			Title:    photo.Title,
			Order:    photo.Order,
			PhotoUrl: photo.PhotoUrl,
			LAT:      photo.LAT,
			LON:      photo.LON,
		}
		result.PhotoUrl = append(result.PhotoUrl, value)
	}

	return result, nil
}

func (a *Usecase) GetInfo(ctx context.Context, id string) (domain.Assessment, error) {
	var (
		logMsg = "uc.Assessment.GetInfo "
		result domain.Assessment
	)
	data, err := a.assessment.GetInfo(ctx, id)
	if err != nil {
		a.log.Error(logMsg+"a.assessment.GetInfo failed", logger.Error(err))

		return result, err
	}

	result = domain.Assessment{
		ID:          data.ID,
		GID:         data.GID,
		CBID:        data.CBID,
		IIBInfoID:   data.IIBInfoID,
		Status:      data.Status,
		IIBInfoYear: data.IIBInfoYear,
		IIBInfoFuel: data.IIBInfoFuel,
	}

	transport, err := a.transport.Find(ctx, result.GID)
	if err != nil {
		a.log.Error(logMsg+"a.transport.Find failed", logger.Error(err))

		return result, err
	}

	result.Transport.Type = transport.Type
	result.Transport.Model = transport.Model
	result.Transport.Name = transport.Name
	result.Transport.Position = transport.Position
	result.Transport.Year = transport.Year
	result.Transport.Speedometer = transport.Speedometer
	result.Transport.Validity = transport.Validity
	result.Transport.Fuel = transport.Fuel

	return result, nil
}

func (a *Usecase) Find(ctx context.Context, id string) (domain.Assessment, error) {
	var (
		logMsg = "uc.Assessment.Find "
		result domain.Assessment
	)
	data, err := a.assessment.Find(ctx, id)
	if err != nil {
		a.log.Error(logMsg+"a.assessment.Find failed", logger.Error(err))

		return result, err
	}

	info, err := a.transport.Find(ctx, data.GID)
	if err != nil {
		a.log.Error(logMsg+"a.transport.Find failed", logger.Error(err))

		return result, err
	}

	//referenceNames, err := a.reference.ReferenceFindName(ctx, domain.ReferenceNames{
	//	Type:     info.Type,
	//	Model:    info.Model,
	//	Name:     info.Name,
	//	Position: info.Position,
	//})
	//if err != nil {
	//	a.log.Error(logMsg+"a.reference.ReferenceFindName failed", logger.Error(err))
	//
	//	return result, err
	//}

	tData, err := a.reference.GetTypeByID(ctx, info.Type)
	if err != nil {
		a.log.Error(logMsg+"a.reference.GetModelByID failed", logger.Error(err))

		return result, err
	}

	model, err := a.reference.GetModelByID(ctx, info.Model)
	if err != nil {
		a.log.Error(logMsg+"a.reference.GetModelByID failed", logger.Error(err))

		return result, err
	}

	name, err := a.reference.GetNameByID(ctx, info.Name)
	if err != nil {
		a.log.Error(logMsg+"a.reference.GetNameByID failed", logger.Error(err))

		return result, err
	}

	position, err := a.reference.GetPositionByID(ctx, info.Position)
	if err != nil {
		a.log.Error(logMsg+"a.reference.GetPositionByID failed", logger.Error(err))

		return result, err
	}

	result = domain.Assessment{
		ID:   data.ID,
		CBID: data.CBID,
		AIDs: data.AIDs,
		PID:  data.PID,
		GID:  data.GID,
		GovUz: domain.GovUz{
			VehicleKuzov:       data.GovUz.VehicleKuzov,
			OwnerPinfl:         data.GovUz.OwnerPinfl,
			OwnerFullname:      data.GovUz.OwnerFullname,
			VehicleModel:       data.GovUz.VehicleModel,
			VehicleColor:       data.GovUz.VehicleColor,
			VehicleMadeYear:    data.GovUz.VehicleMadeYear,
			VehicleMotor:       data.GovUz.VehicleMotor,
			VehicleFullWeight:  data.GovUz.VehicleFullWeight,
			VehicleEmptyWeight: data.GovUz.VehicleEmptyWeight,
			VehiclePlateNumber: data.GovUz.VehiclePlateNumber,
			VehiclePower:       data.GovUz.VehiclePower,
			PassportNumber:     data.GovUz.PassportNumber,
			PassportSeria:      data.GovUz.PassportSeria,
			Shassi:             data.GovUz.Shassi,
		},
		IIBPermitInfo: data.IIBPermitInfo,
		Transport: domain.Transport{
			ID:          info.ID,
			TypeKey:     tData.TypeKey,
			Type:        info.Type,
			Model:       info.Model,
			Name:        info.Name,
			NameKey:     name.NameKey,
			Position:    info.Position,
			Year:        info.Year,
			Speedometer: info.Speedometer,
			Validity:    info.Validity,
			Fuel:        info.Fuel,
			Price:       info.Price,
			SellPrice:   info.SellPrice,
			ExtraParam:  info.ExtraParam,
		},
		Url:    cardUrl,
		Status: data.Status,
	}

	result.TransportView.TransportModel = model.Model
	result.TransportView.TransportName = name.Name
	result.TransportView.TransportPosition = position.Position
	result.TransportView.TransportYear = info.Year
	result.MarketPrice = info.Price
	result.CreditPrice = info.SellPrice
	result.AllPhotosUrl.PhotoUrl = append(result.AllPhotosUrl.PhotoUrl, info.AllPhotosUrl.PhotoUrl...)

	return result, nil
}

func (a *Usecase) CountAllStatus(ctx context.Context, cbid, branchID, roleCode, direct int) ([]domain.AssessmentStatus, error) {
	var (
		logMsg = "uc.Assessment.CountAllStatus "
		result []domain.AssessmentStatus
	)

	allStatus, err := a.status.FindStatuses(ctx)
	if err != nil {
		a.log.Error(logMsg+"a.status.FindStatuses failed", logger.Error(err))

		return nil, err

	}

	counts, err := a.assessment.CountAllStatus(ctx, cbid, branchID, roleCode, direct)
	if err != nil && err != errs.ErrDataNotFound {
		a.log.Error(logMsg+"a.assessment.CountAllStatus failed", logger.Error(err))

		return nil, err
	}

	for _, status := range allStatus {
		oneStatus := domain.AssessmentStatus{
			StatusCode: status.StatusCode,
			StatusName: status.StatusName,
		}

		for _, count := range counts {
			if oneStatus.StatusCode == count.StatusCode {
				oneStatus.StatusCount = count.StatusCount
			}
		}

		result = append(result, oneStatus)
	}

	return result, nil
}

func (a *Usecase) FindAll(ctx context.Context) ([]domain.Assessment, error) {
	var (
		logMsg = "uc.Assessment.FindAll "
	)

	data, err := a.assessment.FindAll(ctx)
	if err != nil {
		a.log.Error(logMsg+"a.assessment.FindAll failed", logger.Error(err))

		return nil, err
	}

	return data, nil
}

func (a *Usecase) FindByStatus(ctx context.Context, status, limit, page, cbid, branchID, roleCode, direct int) (domain.AssessmentPagination, error) {
	var (
		logMsg = "uc.Assessment.FindByStatus "
		result domain.AssessmentPagination
	)

	offset := (page - 1) * limit

	assessment, err := a.assessment.FindByStatus(ctx, status, limit, offset, cbid, branchID, roleCode, direct)
	if err != nil {
		a.log.Error(logMsg+"a.assessment.FindByStatus failed", logger.Error(err))

		return result, err
	}

	for _, value := range assessment {
		info, err := a.transport.Find(ctx, value.GID)
		if err != nil {
			a.log.Error(logMsg+"a.transport.Find failed", logger.Error(err))

			return result, err
		}

		allPhotosUrl, err := a.transport.GetPhotos(ctx, value.GID, value.CreatedAt.Format("2006"))
		if err != nil {
			a.log.Error(logMsg+"a.transport.GetPhotos failed", logger.Error(err))

			return result, err
		}

		transport, err := a.reference.GetNameByID(ctx, info.Name)
		if err != nil {
			a.log.Error(logMsg+"a.transport.GetNameByID failed", logger.Error(err))

			return result, err
		}

		value.TransportView.TransportName = fmt.Sprint(transport.Name, " avtomashinasi")
		value.AllPhotosUrl = allPhotosUrl

		result.Assessments = append(result.Assessments, value)
	}

	totalCount, err := a.assessment.GetTotalAssessmentCount(ctx, status, cbid, branchID, roleCode, direct)
	if err != nil {
		a.log.Error(logMsg+"a.assessment.GetTotalAssessmentCount failed", logger.Error(err))

		return result, nil
	}

	totalPages := int(math.Ceil(float64(totalCount) / float64(limit)))

	result = domain.AssessmentPagination{
		Assessments: result.Assessments,
		TotalPages:  totalPages,
	}

	res, err := a.status.FindByStatusCode(ctx, status)
	if err != nil {
		a.log.Error(logMsg+"a.status.FindByStatusCode failed", logger.Error(err))

		return result, err
	}

	result.StatusName = res.StatusName

	return result, nil
}

func (a *Usecase) FastAssessment(ctx context.Context, data domain.AssessmentStore) (domain.AssessmentResult, error) {
	var (
		logMsg = "uc.Assessment.FastAssessment "

		result domain.AssessmentResult
	)

	result, err := a.Assessment(ctx, data)
	if err != nil {
		a.log.Error(logMsg+"a.Assessment failed", logger.Error(err))

		return result, err
	}

	return result, nil

}

func (a *Usecase) OfficialAssessment(ctx context.Context, data domain.AssessmentStore) (domain.AssessmentResult, error) {
	var (
		logMsg = "uc.Assessment.OfficialAssessment "
		result domain.AssessmentResult
	)

	info, err := a.Find(ctx, data.ID)
	if err != nil {
		a.log.Error(logMsg+"a.GetInfo failed", logger.Error(err))

		return result, err
	}

	request := domain.AssessmentStore{
		Transport: domain.Transport{
			Type:        info.Transport.Type,
			Model:       info.Transport.Model,
			Name:        info.Transport.Name,
			Year:        info.Transport.Year,
			Speedometer: info.Transport.Speedometer,
			Validity:    info.Transport.Validity,
			Position:    info.Transport.Position,
			Fuel:        info.Transport.Fuel,
			ExtraParam:  info.Transport.ExtraParam,
		},
	}

	if info.Transport.Year != info.GovUz.VehicleMadeYear {
		a.log.Error(logMsg+"error with transport made year", logger.Error(err))

		return result, errs.ErrTransportMadeYear
	}

	result, err = a.Assessment(ctx, request)
	if err != nil {
		a.log.Error(logMsg+"a.Assessment failed", logger.Error(err))

		if err.Error() == errs.ErrAnalogNotFound.Error() {
			if err := a.updateStatus(ctx, data.ID, errs.ErrAnalogNotFound); err != nil {
				a.log.Error(logMsg+"a.updateStatus failed", logger.Error(err))

				return result, err
			}
		} else if err.Error() == errs.ErrDiscountNotFound.Error() {
			if err := a.updateStatus(ctx, data.ID, errs.ErrDiscountNotFound); err != nil {
				a.log.Error(logMsg+"a.updateStatus failed", logger.Error(err))

				return result, err
			}
		}

		return result, err
	}

	_, err = a.transport.UpdatePrice(ctx, info.GID, result.MarketPrice, result.CreditPrice)
	if err != nil {
		a.log.Error(logMsg+"a.transport.UpdatePrice failed", logger.Error(err))

		return result, err
	}

	//fmt.Println(result.Analogs, "analogs")
	body := domain.AssessmentStore{
		AIDs: result.Analogs,
		ID:   data.ID,
	}

	AID := a.assessment.UpdateAnalogID(ctx, body)
	if AID != nil {
		a.log.Error(logMsg+"a.assessment.UpdateAnalogID failed", logger.Error(err))

		return result, err
	}

	if err := a.updateStatus(ctx, data.ID, nil); err != nil {
		a.log.Error(logMsg+"a.updateStatus failed", logger.Error(err))

		return result, err
	}

	return result, nil

}

func (a *Usecase) updateStatus(ctx context.Context, id string, err error) error {
	var logMsg = "uc.Assessment.updateStatus "

	if err == nil {
		if err := a.assessment.UpdateStatus(ctx, id, completed); err != nil {
			a.log.Error(logMsg+"a.assessment.UpdateStatus failed", logger.Error(err))

			return err
		}
	} else if err.Error() == errs.ErrAnalogNotFound.Error() {
		if err := a.assessment.UpdateStatus(ctx, id, expertiza); err != nil {
			a.log.Error(logMsg+"a.assessment.UpdateStatus failed", logger.Error(err))

			return err
		}
	} else if err.Error() == errs.ErrDiscountNotFound.Error() {
		if err := a.assessment.UpdateStatus(ctx, id, discount); err != nil {
			a.log.Error(logMsg+"a.assessment.UpdateStatus failed", logger.Error(err))

			return err
		}
	}

	return nil
}

func checkValidty(data domain.AssessmentStore) bool {
	var result bool
	if data.Transport.Validity == 0 {
		if data.Transport.Year == time.Now().Year() {
			if data.Transport.NameKey != "" {
				if strings.Contains(data.Transport.NameKey, "MH") {
					if data.Transport.Speedometer <= 50 {
						result = true
					}
				} else if strings.Contains(data.Transport.NameKey, "KM") {
					if data.Transport.Speedometer <= 3000 {
						result = true
					}
				} else if strings.Contains(data.Transport.NameKey, "NE") {
					result = true
				}
			} else {
				if data.Transport.Speedometer <= 3000 {
					result = true
				}
			}
		}
	} else {
		result = true
	}

	return result
}

func (a *Usecase) Assessment(ctx context.Context, data domain.AssessmentStore) (domain.AssessmentResult, error) {
	var (
		logMsg                                  = "uc.Assessment.Assessment "
		result                                  domain.AssessmentResult
		validity, year, speedometer             []float64
		validityList, yearList, speedometerList []float64
		difference                              float64
		priceList                               []int
		analogIDS                               []string
		sum                                     int
		newPrice, currentPrice                  int
		isFuelOneExist, isFuelTwoExist          bool
	)

	var formalized, formalizedfinal []domain.AssessmentStore
	request := domain.Transport{
		Type:        data.Transport.Type,
		Model:       data.Transport.Model,
		Name:        data.Transport.Name,
		Position:    data.Transport.Position,
		Year:        data.Transport.Year,
		Speedometer: data.Transport.Speedometer,
		Validity:    data.Transport.Validity,
		Fuel:        data.Transport.Fuel,
		SellPrice:   data.Transport.SellPrice,
	}

	if !checkValidty(data) {
		a.log.Error(logMsg + "error Validity with params")

		return result, errs.ErrFakedValidity
	}

	info, err := a.analog.FindAnalog(ctx, request)
	if err != nil {
		a.log.Error(logMsg+"a.analog.FindAnalog failed", logger.Error(err))

		return result, err
	}

	formalized = append(formalized, info...)

	for _, val := range formalized {
		if val.Transport.Fuel == "" {
			if val.Transport.Year-data.Transport.Year <= 5 && val.Transport.Year-data.Transport.Year >= -5 {
				formalizedfinal = append(formalizedfinal, val)
			}
		} else {

			if val.Transport.Year-data.Transport.Year <= 5 && val.Transport.Year-data.Transport.Year >= -5 {
				if strings.EqualFold(val.Transport.Fuel, data.Transport.Fuel) {

					formalizedfinal = append(formalizedfinal, val)

				} else {
					fuels, err := a.reference.FuelControls(ctx, data.Transport.Name)
					if err != nil {
						if err.Error() == errs.ErrDataNotFound.Error() {
							a.log.Error(logMsg+"a.reference.FuelControls failed", logger.Error(err))

						} else {
							a.log.Error(logMsg+"a.reference.FuelControls failed", logger.Error(err))

							return result, err
						}
					}

					for _, fuel := range fuels.FuelControls {

						if fuel.FuelID == val.Transport.Fuel {
							currentPrice = fuel.Price
							isFuelOneExist = true
						}

						if fuel.FuelID == data.Transport.Fuel {
							newPrice = fuel.Price
							isFuelTwoExist = true

						}
					}
					if isFuelOneExist && isFuelTwoExist {
						val.Transport.SellPrice = val.Transport.SellPrice - currentPrice + newPrice
						formalizedfinal = append(formalizedfinal, val)
					}

				}
			}
		}
	}

	if !NumAnalogs(formalizedfinal) {
		a.log.Error(logMsg + "!NumAnalogs(formalizedfinal) failed")

		return result, errs.ErrAnalogNotFound
	}

	for _, r := range formalizedfinal {

		mergeValidity := r.Transport.Validity - data.Transport.Validity
		validity = append(validity, float64(mergeValidity))

		mergeYear := (r.Transport.Year - data.Transport.Year) * (-1)
		year = append(year, float64(mergeYear))

		if !utils.IsTrailer(data.Transport.NameKey) {
			mergeSpeedometer := r.Transport.Speedometer - data.Transport.Speedometer
			speedometer = append(speedometer, float64(mergeSpeedometer))
		}

		analogIDS = append(analogIDS, r.AID)

	}

	for _, y := range year {
		yy := math.Abs(y)
		switch yy {
		case 0:
			difference = year0
		case 1:
			difference = (y / yy) * year1
		case 2:
			difference = (y / yy) * year2
		case 3:
			difference = (y / yy) * year3
		case 4:
			difference = (y / yy) * year4
		case 5:
			difference = (y / yy) * year5
		default:
			difference = (y / yy) * year5
		}
		yearList = append(yearList, difference)
	}

	if !utils.IsTrailer(data.Transport.NameKey) {
		for _, s := range speedometer {
			defaultValue := 5000
			if utils.IsMotoHour(data.Transport.NameKey) {
				defaultValue = 500
			}
			ss := math.Abs(s)
			for i := 0.0; i <= (ss/float64(defaultValue))+1; i++ {

				if i*float64(defaultValue) <= ss && ss < (i+1)*float64(defaultValue) {
					switch i {
					case 0:
						difference = speed0
					case 1.0:
						difference = (s / ss) * speed0
					case 2.0:
						difference = (s / ss) * speed10
					case 3.0:
						difference = (s / ss) * speed15
					case 4.0:
						difference = (s / ss) * speed20
					case 5.0:
						difference = (s / ss) * speed25
					case 6.0:
						difference = (s / ss) * speed25
					case 7.0:
						difference = (s / ss) * speed35
					case 8.0:
						difference = (s / ss) * speed35
					case 9.0:
						difference = (s / ss) * speed45
					case 10.0:
						difference = (s / ss) * speed45
					case 11.0:
						difference = (s / ss) * speed55
					case 12.0:
						difference = (s / ss) * speed55
					case 13.0:
						difference = (s / ss) * speed65
					case 14.0:
						difference = (s / ss) * speed65
					case 15.0:
						difference = (s / ss) * speed75
					case 16.0:
						difference = (s / ss) * speed80
					case 17.0:
						difference = (s / ss) * speed80
					case 18.0:
						difference = (s / ss) * speed80
					case 19.0:
						difference = (s / ss) * speed80
					case 20.0:
						difference = (s / ss) * speed80
					case 21.0:
						difference = (s / ss) * speed80
					case 22.0:
						difference = (s / ss) * speed80
					case 23.0:
						difference = (s / ss) * speed80
					case 24.0:
						difference = (s / ss) * speed80
					case 25.0:
						difference = (s / ss) * speed80
					default:
						i = (ss / float64(defaultValue)) + 2
						difference = (s / ss) * speed80
					}
					speedometerList = append(speedometerList, difference)
				}
			}
		}
	}

	for _, v := range validity {
		val := math.Abs(v)

		switch val {
		case 0:
			difference = new
		case 1.0:
			difference = (v / val) * used
		case 2.0:
			difference = (v / val) * repaired
		case 3.0:
			difference = (v / val) * needRepair
		default:
			difference = (v / val) * needRepair
		}
		validityList = append(validityList, difference)
	}

	for id, r := range formalizedfinal {
		if utils.IsTrailer(data.Transport.NameKey) {
			rsum := int((1 + validityList[id] + yearList[id]) * float64(r.Transport.SellPrice))
			priceList = append(priceList, rsum)
		} else {
			rsum := int((1 + validityList[id] + yearList[id] + speedometerList[id]) * float64(r.Transport.SellPrice))
			priceList = append(priceList, rsum)
		}
	}

	for _, p := range priceList {
		sum += p
	}
	avarageSum := sum / len(priceList)

	model, err := a.reference.GetModelByID(ctx, data.Transport.Model)
	if err != nil {
		a.log.Error(logMsg+"a.reference.GetModelByID failed", logger.Error(err))

		return result, err
	}

	name, err := a.reference.GetNameByID(ctx, data.Transport.Name)
	if err != nil {
		a.log.Error(logMsg+"a.reference.GetNameByID failed", logger.Error(err))

		return result, err
	}

	position, err := a.reference.GetPositionByID(ctx, data.Transport.Position)
	if err != nil {
		a.log.Error(logMsg+"a.reference.GetPositionByID failed", logger.Error(err))

		return result, err
	}

	discount, err := a.reference.GetDiscount(ctx, data.Transport.Name)
	if err != nil {
		a.log.Error(logMsg+"a.reference.GetDiscount failed", logger.Error(errs.ErrDataNotFound))
		return result, err
	}

	var finalDiscount int
	now := time.Now().Year()
	a.log.Info("Current Year: ", logger.Any("%d", now))

	for _, param := range discount.DiscountParams {
		if now-data.Transport.Year == param.Code {
			finalDiscount = param.Percent
		}
	}

	if finalDiscount == 0 {
		a.log.Error(logMsg+"a.reference.GetDiscount failed", logger.Error(err))
		return result, errs.ErrDiscountNotFound
	}

	finalPrice := roundPrice(avarageSum)
	finalResult := domain.AssessmentResult{
		MarketPrice: finalPrice,
		CreditPrice: roundPrice(finalPrice * finalDiscount / 100),
		TransportView: domain.TransportView{
			TransportModel:    model.Model,
			TransportName:     name.Name,
			TransportPosition: position.Position,
			TransportYear:     data.Transport.Year,
		},
		Analogs: analogIDS,
	}
	return finalResult, nil
}

func NumAnalogs(data []domain.AssessmentStore) bool {
	return len(data) > 1
}

func roundPrice(price int) int {
	middleDigit := (price / 100000) % 10
	mainDigit := price / 1000000
	finalPrice := 0
	if middleDigit > 5 {
		finalPrice = (mainDigit + 1) * 1000000
	} else {
		finalPrice = (mainDigit) * 1000000
	}

	return finalPrice
}

func (a *Usecase) UpdateStatus(ctx context.Context, req domain.AssessmentStatus) error {
	var (
		logMsg = "uc.Assessment.UpdateStatus"
	)

	if err := a.assessment.UpdateStatus(ctx, req.ID, req.StatusCode); err != nil {
		a.log.Error(logMsg+"u.modelRepo.Update failed", logger.Error(err))

		return err
	}
	return nil
}

func (a *Usecase) GetReportData(ctx context.Context, id string) (domain.ReportData, error) {
	var (
		logMsg = "uc.Assessment.GetReportData "
		result domain.ReportData
	)

	assessmentData, err := a.assessment.GetReportData(ctx, id)
	if err != nil {
		a.log.Error(logMsg+"a.assessment.GetReportData failed", logger.Error(err))

		return result, err
	}

	tData, err := a.transport.Find(ctx, assessmentData.TransportID)
	if err != nil {
		a.log.Error(logMsg+"a.transport.Find failed", logger.Error(err))

		return result, err
	}

	empData, err := a.identification.GetEmpInfo(ctx, assessmentData.CBID)
	if err != nil {
		a.log.Error(logMsg+"a.identification.GetEmpInfo failed", logger.Error(err))

		return result, err
	}

	referenceData, err := a.reference.ReferenceFindName(ctx, domain.ReferenceNames{Name: tData.Name, Fuel: tData.Fuel})
	if err != nil {
		a.log.Error(logMsg+"a.reference.ReferenceFindNam failed", logger.Error(err))

		return result, err
	}

	result = domain.ReportData{
		CardID: assessmentData.CardID,
		EmpData: domain.EmpData{
			EmpBranchID: empData.EmpBranchID,
			EmpName:     empData.EmpName,
			EmpCBID:     empData.EmpCBID,
		},
		UpdatedAt:     assessmentData.UpdatedAt,
		OwnerFullName: assessmentData.OwnerFullName,
		OwnerPinfl:    assessmentData.OwnerPinfl,
		VIN:           assessmentData.VIN,
		PlateNum:      assessmentData.PlateNum,
		MadeYear:      assessmentData.MadeYear,
		Transport: domain.Transport{
			Name:        referenceData.Name,
			Speedometer: tData.Speedometer,
			Validity:    tData.Validity,
			Fuel:        referenceData.Fuel,
			Price:       tData.Price,
			SellPrice:   tData.SellPrice,
		},
	}

	for _, val := range assessmentData.AnalogID {
		aData, err := a.analog.AnalogFindView(ctx, val)
		if err != nil {
			a.log.Error(logMsg+"a.analog.AnalogFindView failed", logger.Error(err))

			return result, err
		}

		analogData := domain.Analog{
			ID:          aData.ID,
			Name:        aData.Name,
			Year:        aData.Year,
			Speedometer: aData.Speedometer,
			SellPrice:   aData.SellPrice,
			Price:       aData.Price,
			Comment:     aData.Comment,
			SourceFile:  aData.SourceFile,
		}
		result.Analog = append(result.Analog, analogData)
	}
	return result, nil
}

func (a *Usecase) AnalogsList(ctx context.Context, assessmentID string) ([]domain.Analog, error) {
	var (
		logMsg = "uc.Assessment.AnalogsList "
		result []domain.Analog
	)

	assessmentData, err := a.assessment.GetReportData(ctx, assessmentID)
	if err != nil {
		a.log.Error(logMsg+"a.assessment.GetReportData failed", logger.Error(err))

		return result, err
	}

	for _, val := range assessmentData.AnalogID {
		aData, err := a.analog.AnalogFindView(ctx, val)
		if err != nil {
			a.log.Error(logMsg+"a.analog.AnalogFindView failed", logger.Error(err))

			return result, err
		}

		analogData := domain.Analog{
			ID:            aData.ID,
			Model:         aData.Model,
			Name:          aData.Name,
			Position:      aData.Position,
			Year:          aData.Year,
			Speedometer:   aData.Speedometer,
			SellPrice:     aData.SellPrice,
			Price:         aData.Price,
			Fuel:          aData.Fuel,
			Condition:     aData.Condition,
			SourceFile:    aData.SourceFile,
			SourceFileUrl: aData.SourceFileUrl,
		}
		result = append(result, analogData)
	}

	return result, nil
}

func (a *Usecase) GetAssessmentData(ctx context.Context, cbid string, texPassSeria, texPassNum string) (domain.GovInfoWithCBID, error) {
	var (
		logMsg = "uc.Assessment.GetAssessmentData "
		result domain.GovInfoWithCBID
	)

	assessmentData, err := a.assessment.GetGovInfoByAssessmentIDAndStatus(ctx, cbid, texPassSeria, texPassNum)
	if err != nil {
		a.log.Error(logMsg+"a.assessment.GetGovInfoByAssessmentIDAndStatus failed", logger.Error(err))

		return result, err
	}

	transportData, err := a.transport.Find(ctx, assessmentData.TransportID)
	if err != nil {
		a.log.Error(logMsg+"a.analog.TransportFindOne failed", logger.Error(err))

		return result, err
	}

	rIDs := domain.ReferenceNames{
		Model:    transportData.Model,
		Name:     transportData.Name,
		Position: transportData.Position,
		Fuel:     transportData.Fuel,
	}

	rName, err := a.reference.ReferenceFindName(ctx, rIDs)
	if err != nil {
		a.log.Error(logMsg+"a.reference.ReferenceFindName failed", logger.Error(err))

		return result, err
	}

	result = domain.GovInfoWithCBID{
		AssessmentID: assessmentData.AssessmentID,
		CBID:         assessmentData.CBID,
		Transport: domain.Transport{
			Model:       rName.Model,
			Name:        rName.Name,
			Position:    rName.Position,
			Year:        transportData.Year,
			Speedometer: transportData.Speedometer,
			Validity:    transportData.Validity,
			Fuel:        rName.Fuel,
			Price:       transportData.Price,
			SellPrice:   transportData.SellPrice,
		},
		GovUzStore: domain.GovUzStore{
			Pinfl:                assessmentData.GovUzStore.Pinfl,
			OwnerFullname:        assessmentData.GovUzStore.OwnerFullname,
			OwnerType:            assessmentData.GovUzStore.OwnerType,
			OwnerDateBirth:       assessmentData.GovUzStore.OwnerDateBirth,
			VehicleID:            assessmentData.GovUzStore.VehicleID,
			PlateNumber:          assessmentData.GovUzStore.PlateNumber,
			Model:                assessmentData.GovUzStore.Model,
			VehicleColor:         assessmentData.GovUzStore.VehicleColor,
			RegistrationDate:     assessmentData.GovUzStore.RegistrationDate,
			IibPermitInfo:        assessmentData.GovUzStore.IibPermitInfo,
			Division:             assessmentData.GovUzStore.Division,
			GarovPermitInfo:      assessmentData.GovUzStore.GarovPermitInfo,
			Year:                 assessmentData.GovUzStore.Year,
			VehicleType:          assessmentData.GovUzStore.VehicleType,
			Kuzov:                assessmentData.GovUzStore.Kuzov,
			FullWeight:           assessmentData.GovUzStore.FullWeight,
			EmptyWeight:          assessmentData.GovUzStore.EmptyWeight,
			Motor:                assessmentData.GovUzStore.Motor,
			FuelType:             assessmentData.GovUzStore.FuelType,
			Seats:                assessmentData.GovUzStore.Seats,
			Stands:               assessmentData.GovUzStore.Stands,
			Comments:             assessmentData.GovUzStore.Comments,
			Inspection:           assessmentData.GovUzStore.Inspection,
			TexpassportSeria:     assessmentData.GovUzStore.TexpassportSeria,
			TexpassportNumber:    assessmentData.GovUzStore.TexpassportNumber,
			BodyTypeName:         assessmentData.GovUzStore.BodyTypeName,
			Shassi:               assessmentData.GovUzStore.Shassi,
			Power:                assessmentData.GovUzStore.Power,
			DateSchetSpravka:     assessmentData.GovUzStore.DateSchetSpravka,
			TuningPermit:         assessmentData.GovUzStore.TuningPermit,
			TuningGivenDate:      assessmentData.GovUzStore.TuningGivenDate,
			TuningIssueDate:      assessmentData.GovUzStore.TuningIssueDate,
			PrevPnfl:             assessmentData.GovUzStore.PrevPnfl,
			PrevOwner:            assessmentData.GovUzStore.PrevOwner,
			PrevOwnerType:        assessmentData.GovUzStore.PrevOwnerType,
			PrevPlateNumber:      assessmentData.GovUzStore.PrevPlateNumber,
			PrevTexpasportSery:   assessmentData.GovUzStore.PrevTexpasportSery,
			PrevTexpasportNumber: assessmentData.GovUzStore.PrevTexpasportNumber,
			State:                assessmentData.GovUzStore.State,
		},
	}

	return result, nil
}

func (a *Usecase) DeleteAssessment(ctx context.Context, assessmentID string) (domain.Result, error) {
	var (
		logMsg = "uc.Assessment.DeleteAssessment "
	)
	assessment, err := a.assessment.Find(ctx, assessmentID)
	if err != nil {
		a.log.Error(logMsg+"a.assessment.Find failed", logger.Error(err))
		return domain.Result{}, err
	}

	_, errT := a.transport.DeleteTransport(ctx, assessment.GID, assessment.CreatedAt.Format("2006"))
	if errT != nil {
		a.log.Error(logMsg+"a.transport.DeleteTransport failed", logger.Error(errT))
		return domain.Result{}, errT
	}

	if err := a.assessment.DeleteAssessment(ctx, assessmentID); err != nil {
		a.log.Error(logMsg+"a.assessment.DeleteAssessment failed", logger.Error(err))
		return domain.Result{}, err
	}

	return domain.Result{Result: true}, nil
}

// func FindTopAnalogs(resultFinal, alldata []domain.AssessmentStore, year, index int, fuel string) []domain.AssessmentStore {
// 	for _, data := range alldata {
// 		if data.Transport.Year-index == year || year == data.Transport.Year+index {
// 			if strings.EqualFold(data.Transport.Fuel, fuel) {
// 				resultFinal = append(resultFinal, data)
// 			}
// 		}
// 	}
// 	return resultFinal
// }

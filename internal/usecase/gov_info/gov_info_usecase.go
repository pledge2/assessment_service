package gov_info_usecase

import (
	"context"
	"pledge/assessment_service/internal/domain"
	"pledge/assessment_service/pkg/logger"
)

type govInfoClient interface {
	SearchByCarNum(ctx context.Context, val domain.GovUzParams) (domain.GovUzResponse, error)
	// SearchByTechNum(val domain.GovUzParams) (domain.GovUzResponse, error)
	// SearchByVinNum(val domain.GovUzParams) (domain.GovUzResponse, error)
}

type govInfoRepo interface {
	Update(ctx context.Context, id string, data domain.GovUzStore) error
	IncrementAttempt(ctx context.Context, id string) error
}

type assessmentRepo interface {
	GetInfo(ctx context.Context, id string) (domain.Assessment, error)
}

type Usecase struct {
	govRepo        govInfoRepo
	govClient      govInfoClient
	assessmentRepo assessmentRepo
	log            logger.Logger
}

func New(govClient govInfoClient, govRepo govInfoRepo, assessmentRepo assessmentRepo, log logger.Logger) *Usecase {
	return &Usecase{
		govRepo:        govRepo,
		govClient:      govClient,
		assessmentRepo: assessmentRepo,
		log:            log,
	}
}

func (g *Usecase) SearchByCarNum(ctx context.Context, val domain.GovUzParams) (domain.GovUz, error) {
	var (
		logMsg = "uc.GovUz.SearchByTechNum "
		result domain.GovUz
	)

	info, err := g.assessmentRepo.GetInfo(ctx, val.AssessmentID)
	if err != nil {
		g.log.Error(logMsg+"g.assessmentRepo.GetInfo failed", logger.Error(err))

		return result, err
	}

	if err := g.govRepo.IncrementAttempt(ctx, info.IIBInfoID); err != nil {
		g.log.Error(logMsg+"g.govRepo.IncrementAttempt failed", logger.Error(err))

		return result, err
	}

	var data domain.GovUzResponse
	var iibInfo domain.GovUzStore

	if info.CBID == 1 {
		data = govUzMock()
		iibInfo = domain.GovUzStore{
			OwnerType:            data.OwnerType,
			OwnerFullname:        data.OwnerFullname,
			Pinfl:                data.Pinfl,
			VehicleID:            data.Vehicle[0].VehicleID,
			PlateNumber:          data.Vehicle[0].PlateNumber,
			Model:                data.Vehicle[0].Model,
			VehicleColor:         data.Vehicle[0].VehicleColor,
			Division:             data.Vehicle[0].Division,
			Year:                 data.Vehicle[0].Year,
			VehicleType:          data.Vehicle[0].VehicleType,
			Kuzov:                data.Vehicle[0].Kuzov,
			FullWeight:           data.Vehicle[0].FullWeight,
			EmptyWeight:          data.Vehicle[0].EmptyWeight,
			Motor:                data.Vehicle[0].Motor,
			FuelType:             data.Vehicle[0].FuelType,
			Seats:                data.Vehicle[0].Seats,
			Stands:               data.Vehicle[0].Stands,
			Comments:             data.Vehicle[0].Comments,
			RegistrationDate:     data.Vehicle[0].RegistrationDate,
			TexpassportSeria:     data.Vehicle[0].TexpassportSeria,
			TexpassportNumber:    data.Vehicle[0].TexpassportNumber,
			BodyTypeName:         data.Vehicle[0].BodyTypeName,
			Shassi:               data.Vehicle[0].Shassi,
			Power:                data.Vehicle[0].Power,
			DateSchetSpravka:     data.Vehicle[0].DateSchetSpravka,
			TuningPermit:         data.Vehicle[0].TuningPermit,
			TuningGivenDate:      data.Vehicle[0].TuningGivenDate,
			TuningIssueDate:      data.Vehicle[0].TuningIssueDate,
			PrevPnfl:             data.Vehicle[0].PrevPnfl,
			PrevOwner:            data.Vehicle[0].PrevOwner,
			PrevOwnerType:        data.Vehicle[0].PrevOwnerType,
			PrevPlateNumber:      data.Vehicle[0].PrevPlateNumber,
			PrevTexpasportSery:   data.Vehicle[0].PrevTexpasportSery,
			PrevTexpasportNumber: data.Vehicle[0].PrevTexpasportNumber,
			State:                data.Vehicle[0].State,
			Inspection:           data.Vehicle[0].Inspection,
		}
	} else {
		data, err := g.govClient.SearchByCarNum(ctx, val)
		if err != nil {
			g.log.Error(logMsg+"g.govClient.SearchByCarNum failed", logger.Error(err))

			return result, err
		}
		iibInfo = domain.GovUzStore{
			OwnerType:            data.OwnerType,
			OwnerFullname:        data.OwnerFullname,
			Pinfl:                data.Pinfl,
			VehicleID:            data.Vehicle[0].VehicleID,
			PlateNumber:          data.Vehicle[0].PlateNumber,
			Model:                data.Vehicle[0].Model,
			VehicleColor:         data.Vehicle[0].VehicleColor,
			Division:             data.Vehicle[0].Division,
			Year:                 data.Vehicle[0].Year,
			VehicleType:          data.Vehicle[0].VehicleType,
			Kuzov:                data.Vehicle[0].Kuzov,
			FullWeight:           data.Vehicle[0].FullWeight,
			EmptyWeight:          data.Vehicle[0].EmptyWeight,
			Motor:                data.Vehicle[0].Motor,
			FuelType:             data.Vehicle[0].FuelType,
			Seats:                data.Vehicle[0].Seats,
			Stands:               data.Vehicle[0].Stands,
			Comments:             data.Vehicle[0].Comments,
			RegistrationDate:     data.Vehicle[0].RegistrationDate,
			TexpassportSeria:     data.Vehicle[0].TexpassportSeria,
			TexpassportNumber:    data.Vehicle[0].TexpassportNumber,
			BodyTypeName:         data.Vehicle[0].BodyTypeName,
			Shassi:               data.Vehicle[0].Shassi,
			Power:                data.Vehicle[0].Power,
			DateSchetSpravka:     data.Vehicle[0].DateSchetSpravka,
			TuningPermit:         data.Vehicle[0].TuningPermit,
			TuningGivenDate:      data.Vehicle[0].TuningGivenDate,
			TuningIssueDate:      data.Vehicle[0].TuningIssueDate,
			PrevPnfl:             data.Vehicle[0].PrevPnfl,
			PrevOwner:            data.Vehicle[0].PrevOwner,
			PrevOwnerType:        data.Vehicle[0].PrevOwnerType,
			PrevPlateNumber:      data.Vehicle[0].PrevPlateNumber,
			PrevTexpasportSery:   data.Vehicle[0].PrevTexpasportSery,
			PrevTexpasportNumber: data.Vehicle[0].PrevTexpasportNumber,
			State:                data.Vehicle[0].State,
			Inspection:           data.Vehicle[0].Inspection,
		}
	}

	if err := g.govRepo.Update(ctx, info.IIBInfoID, iibInfo); err != nil {
		g.log.Error(logMsg+"g.govRepo.Update failed", logger.Error(err))

		return result, err
	}

	result = domain.GovUz{
		OwnerFullname:      iibInfo.OwnerFullname,
		OwnerPinfl:         iibInfo.Pinfl,
		VehicleMadeYear:    iibInfo.Year,
		VehiclePlateNumber: iibInfo.PlateNumber,
		VehicleModel:       iibInfo.Model,
		VehicleColor:       iibInfo.VehicleColor,
		VehicleKuzov:       iibInfo.Kuzov,
		VehicleFullWeight:  iibInfo.FullWeight,
		VehicleEmptyWeight: iibInfo.EmptyWeight,
		VehicleMotor:       iibInfo.Motor,
		PassportNumber:     iibInfo.TexpassportNumber,
		PassportSeria:      iibInfo.TexpassportSeria,
		VinNumber:          iibInfo.Motor,
		VehiclePower:       iibInfo.Power,
		Seats:              iibInfo.Seats,
		Shassi:             iibInfo.Shassi,
	}

	return result, nil
}

func govUzMock() domain.GovUzResponse {
	response := domain.GovUzResponse{
		Result:          1,
		Comment:         "Mock Data",
		Pinfl:           "123456789012",
		OwnerFullname:   "John Doe",
		OwnerType:       2,
		OrganizationInn: "9876543210",
		Vehicle: []domain.Vehicle{
			{
				VehicleID:            1,
				PlateNumber:          "60Z777ZZ",
				Model:                "BMW M5CS",
				VehicleColor:         "Blue",
				RegistrationDate:     "2023-01-01",
				Division:             "Division A",
				Year:                 2022,
				VehicleType:          1,
				Kuzov:                "1FT7W2BT4DEB51920",
				FullWeight:           1970,
				EmptyWeight:          1770,
				Motor:                "1FT7W2BT4DEB51920",
				FuelType:             1,
				Seats:                5,
				Stands:               2,
				Comments:             "Test comments",
				Inspection:           "2023-12-31",
				TexpassportSeria:     "ABC",
				TexpassportNumber:    "123456",
				BodyTypeName:         "Sedan",
				Shassi:               "XYZ987654321",
				Power:                627,
				DateSchetSpravka:     "2023-01-15",
				TuningPermit:         "T123",
				TuningGivenDate:      "2023-01-20",
				TuningIssueDate:      "2023-01-25",
				PrevPnfl:             "987654321098",
				PrevOwner:            "Jane Doe",
				PrevOwnerType:        "Individual",
				PrevPlateNumber:      "ABC123",
				PrevTexpasportSery:   "DEF",
				PrevTexpasportNumber: "987654",
				State:                "Active",
			},
		},
	}
	return response
}

func (g *Usecase) UpdateGovInfoData(ctx context.Context, assessmentID string, info domain.GovUzStore) error {
	var (
		logMsg = "uc.GovUz.UpdateGovInfoData "
	)
	aData, err := g.assessmentRepo.GetInfo(ctx, assessmentID)
	if err != nil {
		g.log.Error(logMsg+"g.assessmentRepo.GetInfo failed", logger.Error(err))

		return err
	}

	if err := g.govRepo.Update(ctx, aData.IIBInfoID, info); err != nil {
		g.log.Error(logMsg+"g.govRepo.Update failed", logger.Error(err))

		return err
	}

	return nil
}

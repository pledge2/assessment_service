package status_usecase

import (
	"context"
	"pledge/assessment_service/internal/domain"
	"pledge/assessment_service/pkg/logger"
)

type statusRepo interface {
	Store(ctx context.Context, data domain.Status) (string, error)
	FindStatus(ctx context.Context, id string) (domain.Status, error)
	FindStatuses(ctx context.Context) ([]domain.Status, error)
	Update(ctx context.Context, id string, data domain.Status) error
	Delete(ctx context.Context, id string) error
}

type Usecase struct {
	status statusRepo
	log    logger.Logger
}

func New(status statusRepo, log logger.Logger) *Usecase {
	return &Usecase{
		status: status,
		log:    log,
	}
}

func (s *Usecase) Store(ctx context.Context, data domain.Status) (string, error) {
	var logMsg = "uc.Status.Store "

	id, err := s.status.Store(ctx, data)
	if err != nil {
		s.log.Error(logMsg+"s.status.Store failed", logger.Error(err))

		return "", err
	}

	return id, nil
}

func (s *Usecase) FindOne(ctx context.Context, id string) (domain.Status, error) {
	var logMsg = "uc.Status.FindOne "

	result, err := s.status.FindStatus(ctx, id)
	if err != nil {
		s.log.Error(logMsg+"s.status.FindStatus failed", logger.Error(err))

		return domain.Status{}, err
	}

	return result, nil
}

func (s *Usecase) FindAll(ctx context.Context) ([]domain.Status, error) {
	var logMsg = "uc.Status.FindAll "

	result, err := s.status.FindStatuses(ctx)
	if err != nil {
		s.log.Error(logMsg+"s.status.FindStatuses failed", logger.Error(err))

		return nil, err
	}

	return result, nil
}

func (s *Usecase) Update(ctx context.Context, id string, data domain.Status) error {
	var logMsg = "uc.Status.Update "

	if err := s.status.Update(ctx, id, data); err != nil {
		s.log.Error(logMsg+"s.status.Update failed", logger.Error(err))

		return err
	}

	return nil
}

func (s *Usecase) Delete(ctx context.Context, id string) error {
	var logMsg = "uc.Status.Delete "

	if err := s.status.Delete(ctx, id); err != nil {
		s.log.Error(logMsg+"s.status.Delete failed", logger.Error(err))

		return err
	}

	return nil
}

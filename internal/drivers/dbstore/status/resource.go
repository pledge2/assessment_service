package status_store

import (
	"time"
)

type status struct {
	ID         string    `db:"id"`
	StatusCode int       `db:"status_code"`
	StatusName string    `db:"status_name"`
	PhotoName  string    `db:"photo_name"`
	CreateAt   time.Time `db:"created_at"`
	UpdateAt   time.Time `db:"updated_at"`
}

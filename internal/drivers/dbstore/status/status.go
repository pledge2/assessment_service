package status_store

import (
	"context"
	"database/sql"
	"errors"
	"pledge/assessment_service/internal/domain"
	"pledge/assessment_service/internal/errs"
	"pledge/assessment_service/pkg/logger"
	"pledge/assessment_service/pkg/utils"
	"time"
)

type StatusRepo struct {
	db  *sql.DB
	log logger.Logger
}

func New(db *sql.DB, log logger.Logger) *StatusRepo {
	return &StatusRepo{
		db:  db,
		log: log,
	}
}

func (s *StatusRepo) Store(ctx context.Context, info domain.Status) (string, error) {
	var (
		logMsg = "repo.Status.Store "
		query  = "INSERT INTO status(status_code, status_name, photo_name,  created_at, updated_at) VALUES($1, $2, $3, $4,$5) RETURNING id"
		data   status
		id     string
	)
	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		s.log.Error(logMsg+"s.db.BeginTx failed", logger.Error(err))

		return "", errs.ErrInternal
	}

	data = status{
		ID:         info.ID,
		StatusCode: info.StatusCode,
		StatusName: info.StatusName,
	}
	data.CreateAt = time.Now()
	data.UpdateAt = time.Now()
	formalizedStatusName := utils.Formalize(data.StatusName)

	row := tx.QueryRow(query, data.StatusCode, formalizedStatusName, "default", data.CreateAt, data.UpdateAt)

	err = row.Scan(&id)
	if err != nil {
		s.log.Error(logMsg+"row.Scan failed", logger.Error(err))

		return "", errs.ErrInternal
	}

	if err := tx.Commit(); err != nil {
		s.log.Error(logMsg+"tx.Commit failed", logger.Error(err))

		return "", errs.ErrInternal
	}

	return id, nil
}

func (s *StatusRepo) FindByStatusCode(ctx context.Context, statusCode int) (domain.Status, error) {
	var (
		logMsg = "repo.Status.FindByStatusCode "
		query  = "SELECT id, status_code, status_name FROM status WHERE status_code=$1"
		data   status
		result domain.Status
	)

	row := s.db.QueryRowContext(ctx, query, statusCode)

	err := row.Scan(
		&data.ID,
		&data.StatusCode,
		&data.StatusName,
	)

	if errors.Is(err, sql.ErrNoRows) {
		s.log.Error(logMsg+"row.Scan failed", logger.Error(err))

		return result, errs.ErrDataNotFound
	} else if err != nil {
		s.log.Error(logMsg+"row.Scan failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Status{
		ID:         data.ID,
		StatusCode: data.StatusCode,
		StatusName: data.StatusName,
	}

	return result, nil
}

func (s *StatusRepo) FindStatus(ctx context.Context, id string) (domain.Status, error) {
	var (
		logMsg = "repo.Status.FindStatus "
		query  = "SELECT id, status_code, status_name, created_at, updated_at FROM status WHERE id = $1"
		data   status
		result domain.Status
	)

	row := s.db.QueryRowContext(ctx, query, id)

	err := row.Scan(
		&data.ID,
		&data.StatusCode,
		&data.StatusName,
		&data.CreateAt,
		&data.UpdateAt,
	)
	if errors.Is(err, sql.ErrNoRows) {
		s.log.Error(logMsg+"row.Scan failed", logger.Error(err))

		return result, errs.ErrDataNotFound
	} else if err != nil {
		s.log.Error(logMsg+"row.Scan failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Status{
		ID:         data.ID,
		StatusCode: data.StatusCode,
		StatusName: data.StatusName,
		CreateAt:   data.CreateAt,
	}

	return result, nil
}

func (s *StatusRepo) FindStatuses(ctx context.Context) ([]domain.Status, error) {
	var (
		logMsg = "repo.Status.FindStatuses "
		query  = "SELECT id, status_code, status_name, created_at FROM status ORDER BY status_code ASC"
		result []domain.Status
		data   status
	)
	rows, err := s.db.QueryContext(ctx, query)
	if err != nil {
		s.log.Error(logMsg+"s.db.QueryContext failed", logger.Error(err))

		return nil, errs.ErrInternal
	}

	defer func() { _ = rows.Close() }()

	for rows.Next() {
		err = rows.Scan(
			&data.ID,
			&data.StatusCode,
			&data.StatusName,
			&data.CreateAt,
		)
		if errors.Is(err, sql.ErrNoRows) {
			s.log.Error(logMsg+"row.Scan failed", logger.Error(err))

			return result, errs.ErrDataNotFound
		} else if err != nil {
			s.log.Error(logMsg+"row.Scan failed", logger.Error(err))

			return nil, errs.ErrInternal
		}

		info := domain.Status{
			ID:         data.ID,
			StatusCode: data.StatusCode,
			StatusName: data.StatusName,
			CreateAt:   data.CreateAt,
		}
		result = append(result, info)
	}
	return result, nil
}

func (s *StatusRepo) Update(ctx context.Context, id string, data domain.Status) error {
	var (
		logMsg = "repo.Status.Update "
		query  = "UPDATE status SET status_code = $1, status_name = $2, updated_at = $3 WHERE id = $4"
	)
	data.UpdateAt = time.Now()

	row, err := s.db.ExecContext(ctx, query, data.StatusCode, data.StatusName, data.UpdateAt, id)
	if err != nil {
		s.log.Error(logMsg+"s.db.ExecContext failed", logger.Error(err))

		return errs.ErrInternal
	}

	if n, err := row.RowsAffected(); n < 1 || err != nil {
		s.log.Error(logMsg+" RowsAffected -- %w ", logger.Error(err))
		return errs.ErrInternal
	}

	return nil
}

func (s *StatusRepo) Delete(ctx context.Context, id string) error {
	var (
		logMsg = "repo.Status.Delete "
		query  = "DELETE FROM status WHERE id = $1"
	)

	row, err := s.db.ExecContext(ctx, query, id)
	if err != nil {
		s.log.Error(logMsg+"s.db.ExecContext failed", logger.Error(err))

		return errs.ErrInternal
	}

	if n, err := row.RowsAffected(); n < 1 || err != nil {
		s.log.Error(logMsg+" RowsAffected -- %w ", logger.Error(err))
		return errs.ErrInternal
	}

	return nil
}

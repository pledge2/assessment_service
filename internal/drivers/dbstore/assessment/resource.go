package assessment_store

import (
	"database/sql"
	"pledge/assessment_service/internal/domain"
	"time"
)

type Assessment struct {
	ID                 string         `db:"id"`
	CBID               int            `db:"cbid"`
	Direct             sql.NullInt64  `db:"direct"`
	EXTCBID            sql.NullInt64  `db:"cbid"`
	BranchID           sql.NullInt64  `db:"cbid"`
	AIDs               []string       `db:"a_id"`
	AID                string         `db:"a_id"`
	PID                string         `db:"p_id"`
	GID                string         `db:"g_id"`
	IIBInfoID          sql.NullString `db:"iib_info_id"`
	Status             int            `db:"status"`
	CardID             sql.NullString `db:"card_id"`
	OwnerFullName      string         `db:"owner_fullname"`
	VehiclePlateNumber string         `db:"vehicle_plate_number"`
	VehicleKuzov       string         `db:"vehicle_kuzov"`
	CreateAt           time.Time      `db:"created_at"`
	UpdatedAt          time.Time      `db:"updated_at"`
}

type AssessmentStatusCount struct {
	StatusCode  int `db:"status"`
	StatusCount int `db:"count"`
}

type AssessmentWithIIBInfo struct {
	ID                 string         `db:"id"`
	CBID               int            `db:"cbid"`
	AIDs               []string       `db:"a_id"`
	AID                string         `db:"a_id"`
	PID                string         `db:"p_id"`
	GID                sql.NullString `db:"g_id"`
	IIBInfoID          sql.NullString `db:"iib_info_id"`
	Status             int            `db:"status"`
	CreatedAt          time.Time      `db:"created_at"`
	VehicleKuzov       string         `db:"vehicle_kuzov"`
	OwnerPinfl         string         `db:"owner_pinfl"`
	OwnerFullname      string         `db:"owner_fullname"`
	VehicleModel       string         `db:"vehicle_model"`
	VehicleColor       string         `db:"vehicle_color"`
	VehicleMadeYear    int            `db:"vehicle_made_year"`
	VehicleMotor       string         `db:"vehicle_motor"`
	VehiclePlateNumber string         `db:"vehicle_plate_number"`
	VehicleFullWeight  int            `db:"vehicle_full_weight"`
	VehicleEmptyWeight int            `db:"vehicle_empty_weight"`
	VehiclePower       int            `db:"power"`
	PassportNumber     string         `db:"texpassport_number"`
	PassportSeria      string         `db:"texpassport_seria"`
	IIBPermitInfo      string         `db:"iib_permit_info"`
	Shassi             string         `db:"shassi"`
}

type AssessmentInfo struct {
	ID              string         `db:"id"`
	CBID            int            `db:"cbid"`
	GID             sql.NullString `db:"g_id"`
	IIBInfoID       sql.NullString `db:"iib_info_id"`
	Status          int            `db:"status"`
	VehicleMadeYear sql.NullInt64  `db:"vehicle_made_year"`
	VehicleFuelType sql.NullInt64  `db:"vehicle_fuel_year_type"`
}

type ReportData struct {
	CBID                  int            `json:"cbid"`
	AnalogID              []string       `json:"a_id"`
	TransportID           string         `json:"g_id"`
	CardID                sql.NullString `json:"card_id"`
	OwnerFullName         string         `json:"owner_fullname"`
	OwnerPinfl            string         `json:"owner_pinfl"`
	VIN                   string         `json:"vehicle_kuzov"`
	PlateNum              string         `json:"vehicle_plate_number"`
	MadeYear              int            `json:"vehicle_made_year"`
	AssessmentUpdatedtime time.Time      `json:"assessment_updated_time"`
}

type UpdateAnalogID struct {
	ID       string   `json:"id"`
	AnalogID []string `json:"a_id"`
}

type GovInfoWithCBID struct {
	AssessmentID         string         `db:"id"`
	TransportID          string         `db:"g_id"`
	CBID                 string         `db:"cbid"`
	OwnerType            sql.NullInt64  `db:"owner_type"`
	OwnerPinfl           sql.NullString `db:"owner_pinfl"`
	OwnerFullName        sql.NullString `db:"owner_fullname"`
	OwnerDateBirth       sql.NullString `db:"owner_date_birth"`
	VehicleID            sql.NullInt64  `db:"vehicle_id"`
	VehiclePlateNumber   sql.NullString `db:"vehicle_plate_number"`
	VehicleModel         sql.NullString `db:"vehicle_model"`
	VehicleColor         sql.NullString `db:"vehicle_color"`
	VehicleKuzov         sql.NullString `db:"vehicle_kuzov"`
	VehicleMotor         sql.NullString `db:"vehicle_motor"`
	Division             sql.NullString `db:"division"`
	IibRegistrationDate  sql.NullString `db:"iib_registration_date"`
	IibPermitInfo        sql.NullString `db:"iib_permit_info"`
	GarovPermitInfo      sql.NullString `db:"garov_permit_info"`
	Inspection           sql.NullString `db:"inspection"`
	VehicleMadeYear      sql.NullInt64  `db:"vehicle_made_year"`
	VehicleType          sql.NullInt64  `db:"vehicle_type"`
	VehicleFullWeight    sql.NullInt64  `db:"vehicle_full_weight"`
	VehicleEmptyWeight   sql.NullInt64  `db:"vehicle_empty_weight"`
	VehicleFuelType      sql.NullInt64  `db:"vehicle_fuel_type"`
	VehicleSeats         sql.NullInt64  `db:"vehicle_seats"`
	VehicleStands        sql.NullInt64  `db:"vehicle_stands"`
	TexpassportSeria     sql.NullString `db:"texpassport_seria"`
	TexpassportNumber    sql.NullString `db:"texpassport_number"`
	BodyTypeName         sql.NullString `db:"body_type_name"`
	Shassi               sql.NullString `db:"shassi"`
	Power                sql.NullInt64  `db:"power"`
	DateSchetSpravka     sql.NullString `db:"date_schet_spravka"`
	TuningPermit         sql.NullString `db:"tuning_permit"`
	TuningGivenDate      sql.NullString `db:"tuning_given_date"`
	TuningIssueDate      sql.NullString `db:"tuning_issue_date"`
	PrevPnfl             sql.NullString `db:"prev_pnfl"`
	PrevOwner            sql.NullString `db:"prev_owner"`
	PrevOwnerType        sql.NullString `db:"prev_owner_type"`
	PrevPlateNumber      sql.NullString `db:"prev_plate_number"`
	PrevTexpasportSeria  sql.NullString `db:"prev_texpassport_seria"`
	PrevTexpasportNumber sql.NullString `db:"prev_texpassport_number"`
	State                sql.NullString `db:"statee"`
}

func (data GovInfoWithCBID) transformToDomain() domain.GovInfoWithCBID {
	result := domain.GovInfoWithCBID{
		AssessmentID: data.AssessmentID,
		TransportID:  data.TransportID,
		CBID:         data.CBID,
	}

	if data.OwnerType.Valid {
		result.GovUzStore.OwnerType = int(data.OwnerType.Int64)
	}
	if data.OwnerPinfl.Valid {
		result.GovUzStore.Pinfl = data.OwnerPinfl.String
	}
	if data.OwnerFullName.Valid {
		result.GovUzStore.OwnerFullname = data.OwnerFullName.String
	}
	if data.OwnerDateBirth.Valid {
		result.GovUzStore.OwnerDateBirth = data.OwnerDateBirth.String
	}
	if data.VehicleID.Valid {
		result.GovUzStore.VehicleID = int(data.VehicleID.Int64)
	}
	if data.VehiclePlateNumber.Valid {
		result.GovUzStore.PlateNumber = data.VehiclePlateNumber.String
	}
	if data.VehicleModel.Valid {
		result.GovUzStore.PlateNumber = data.VehiclePlateNumber.String
	}
	if data.VehicleColor.Valid {
		result.GovUzStore.VehicleColor = data.VehicleColor.String
	}
	if data.VehicleKuzov.Valid {
		result.GovUzStore.Kuzov = data.VehicleKuzov.String
	}
	if data.VehicleMotor.Valid {
		result.GovUzStore.Motor = data.VehicleMotor.String
	}
	if data.Division.Valid {
		result.GovUzStore.Division = data.Division.String
	}
	if data.IibRegistrationDate.Valid {
		result.GovUzStore.RegistrationDate = data.IibRegistrationDate.String
	}
	if data.IibPermitInfo.Valid {
		result.GovUzStore.IibPermitInfo = data.IibPermitInfo.String
	}
	if data.GarovPermitInfo.Valid {
		result.GovUzStore.GarovPermitInfo = data.GarovPermitInfo.String
	}
	if data.Inspection.Valid {
		result.GovUzStore.Inspection = data.Inspection.String
	}
	if data.VehicleMadeYear.Valid {
		result.GovUzStore.Year = int(data.VehicleMadeYear.Int64)
	}
	if data.VehicleType.Valid {
		result.GovUzStore.VehicleType = int(data.VehicleType.Int64)
	}
	if data.VehicleFullWeight.Valid {
		result.GovUzStore.FullWeight = int(data.VehicleFullWeight.Int64)
	}
	if data.VehicleEmptyWeight.Valid {
		result.GovUzStore.EmptyWeight = int(data.VehicleEmptyWeight.Int64)
	}
	if data.VehicleFuelType.Valid {
		result.GovUzStore.FuelType = int(data.VehicleFuelType.Int64)
	}
	if data.VehicleSeats.Valid {
		result.GovUzStore.FuelType = int(data.VehicleFuelType.Int64)
	}
	if data.VehicleStands.Valid {
		result.GovUzStore.Stands = int(data.VehicleStands.Int64)
	}
	if data.TexpassportSeria.Valid {
		result.GovUzStore.TexpassportSeria = data.TexpassportSeria.String
	}
	if data.TexpassportNumber.Valid {
		result.GovUzStore.TexpassportNumber = data.TexpassportNumber.String
	}
	if data.BodyTypeName.Valid {
		result.GovUzStore.BodyTypeName = data.BodyTypeName.String
	}
	if data.Shassi.Valid {
		result.GovUzStore.Shassi = data.Shassi.String
	}
	if data.Power.Valid {
		result.GovUzStore.Power = int(data.Power.Int64)
	}
	if data.DateSchetSpravka.Valid {
		result.GovUzStore.DateSchetSpravka = data.DateSchetSpravka.String
	}
	if data.TuningPermit.Valid {
		result.GovUzStore.TuningPermit = data.TuningPermit.String
	}
	if data.TuningGivenDate.Valid {
		result.GovUzStore.TuningGivenDate = data.TuningGivenDate.String
	}
	if data.TuningIssueDate.Valid {
		result.GovUzStore.TuningIssueDate = data.TuningIssueDate.String
	}
	if data.PrevPnfl.Valid {
		result.GovUzStore.PrevPnfl = data.PrevPnfl.String
	}
	if data.PrevOwner.Valid {
		result.GovUzStore.PrevOwner = data.PrevOwner.String
	}
	if data.PrevOwnerType.Valid {
		result.GovUzStore.PrevOwnerType = data.PrevOwnerType.String
	}
	if data.PrevPlateNumber.Valid {
		result.GovUzStore.PrevPlateNumber = data.PrevPlateNumber.String
	}
	if data.State.Valid {
		result.GovUzStore.State = data.State.String
	}
	if data.PrevTexpasportSeria.Valid {
		result.GovUzStore.PrevTexpasportSery = data.PrevTexpasportSeria.String
	}
	if data.PrevTexpasportNumber.Valid {
		result.GovUzStore.PrevTexpasportNumber = data.PrevTexpasportNumber.String
	}
	return result
}

package assessment_store

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"github.com/lib/pq"
	"pledge/assessment_service/internal/domain"
	"pledge/assessment_service/internal/errs"
	"pledge/assessment_service/pkg/logger"
)

const (
	zero = 0
)

type AssessmentRepo struct {
	db  *sql.DB
	log logger.Logger
}

func New(db *sql.DB, log logger.Logger) *AssessmentRepo {
	return &AssessmentRepo{
		db:  db,
		log: log,
	}
}

func (s *AssessmentRepo) Store(ctx context.Context, info domain.AssessmentStore) (string, error) {
	var (
		logMsg = "repo.Assessment.Store "
		query  = "INSERT INTO assessment(cbid, a_id, p_id, g_id, iib_info_id, status, card_id, ext_cbid, branch_id, direct) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) RETURNING id"
		id     string
	)

	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		s.log.Error(logMsg+"s.db.BeginTx failed", logger.Error(err))

		return "", errs.ErrInternal
	}

	defer func() { _ = tx.Rollback() }()

	data := Assessment{
		CBID: info.CBID,
		Direct: sql.NullInt64{
			Int64: int64(info.Direct),
		},
		EXTCBID: sql.NullInt64{
			Int64: int64(info.EXTCBID),
		},
		BranchID: sql.NullInt64{
			Int64: int64(info.BranchID),
		},
		PID:  info.PID,
		GID:  info.GID,
		AIDs: info.AIDs,
		IIBInfoID: sql.NullString{
			String: info.IIBInfoID,
		},
		Status: info.Status,
		CardID: sql.NullString{
			String: info.CardID,
		},
	}

	row := tx.QueryRow(query, data.CBID, pq.Array(data.AIDs), data.PID, data.GID, data.IIBInfoID.String, data.Status, data.CardID.String, data.EXTCBID.Int64, data.BranchID.Int64, data.Direct.Int64)

	if err := row.Scan(&id); err != nil {
		s.log.Error(logMsg+"row.Scan failed", logger.Error(err))

		return "", errs.ErrInternal
	}

	if err := tx.Commit(); err != nil {
		s.log.Error(logMsg+"tx.Commit failed", logger.Error(err))

		return "", errs.ErrInternal
	}

	return id, nil
}

func (s *AssessmentRepo) GetInfo(ctx context.Context, id string) (domain.Assessment, error) {
	var (
		logMsg = "repo.Assessment.GetInfo "
		query  = `SELECT s.id, s.cbid, s.g_id, s.iib_info_id, s.status,	i.vehicle_made_year, i.vehicle_fuel_type FROM assessment s JOIN iib_info i ON s.iib_info_id = i.id WHERE s.id= $1`
		result domain.Assessment
		data   AssessmentInfo
	)
	row := s.db.QueryRowContext(ctx, query, id)

	err := row.Scan(
		&data.ID,
		&data.CBID,
		&data.GID.String,
		&data.IIBInfoID.String,
		&data.Status,
		&data.VehicleMadeYear,
		&data.VehicleFuelType,
	)
	if errors.Is(err, sql.ErrNoRows) {
		s.log.Error(logMsg+"row.Scan failed", logger.Error(err))

		return result, errs.ErrDataNotFound
	} else if err != nil {
		s.log.Error(logMsg+"row.Scan failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Assessment{
		ID:          data.ID,
		CBID:        data.CBID,
		IIBInfoID:   data.IIBInfoID.String,
		IIBInfoYear: int(data.VehicleMadeYear.Int64),
		IIBInfoFuel: int(data.VehicleFuelType.Int64),
	}

	if data.GID.Valid {
		result.GID = data.GID.String
	}
	if data.IIBInfoID.Valid {
		result.IIBInfoID = data.IIBInfoID.String
	}

	return result, nil
}

func (s *AssessmentRepo) Find(ctx context.Context, id string) (domain.Assessment, error) {
	var (
		logMsg = "repo.Assessment.Find "
		query  = `SELECT s.id, s.cbid, s.a_id, s.p_id, s.g_id, s.iib_info_id, s.status, s.created_at,
	i.vehicle_kuzov, i.owner_pinfl, i.owner_fullname, i.vehicle_model, i.vehicle_color, 
	i.vehicle_made_year, i.vehicle_motor, i.vehicle_full_weight, i.vehicle_empty_weight, i.vehicle_plate_number, 
	i.iib_permit_info, i.power, i.texpassport_seria, i.texpassport_number, i.shassi
	FROM assessment s JOIN iib_info i ON s.iib_info_id = i.id WHERE s.id= $1`
		result domain.Assessment
		data   AssessmentWithIIBInfo
	)

	row := s.db.QueryRowContext(ctx, query, id)

	err := row.Scan(
		&data.ID,
		&data.CBID,
		pq.Array(&data.AIDs),
		&data.PID,
		&data.GID.String,
		&data.IIBInfoID.String,
		&data.Status,
		&data.CreatedAt,
		&data.VehicleKuzov,
		&data.OwnerPinfl,
		&data.OwnerFullname,
		&data.VehicleModel,
		&data.VehicleColor,
		&data.VehicleMadeYear,
		&data.VehicleMotor,
		&data.VehicleFullWeight,
		&data.VehicleEmptyWeight,
		&data.VehiclePlateNumber,
		&data.IIBPermitInfo,
		&data.VehiclePower,
		&data.PassportSeria,
		&data.PassportNumber,
		&data.Shassi,
	)

	if errors.Is(err, sql.ErrNoRows) {
		s.log.Error(logMsg+"row.Scan failed", logger.Error(err))

		return result, errs.ErrDataNotFound
	} else if err != nil {
		s.log.Error(logMsg+"row.Scan failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Assessment{
		ID:            data.ID,
		CBID:          data.CBID,
		AIDs:          data.AIDs,
		PID:           data.PID,
		GID:           data.GID.String,
		IIBPermitInfo: data.IIBPermitInfo,
		IIBInfoID:     data.IIBInfoID.String,
		GovUz: domain.GovUz{
			VehicleKuzov:       data.VehicleKuzov,
			OwnerPinfl:         data.OwnerPinfl,
			OwnerFullname:      data.OwnerFullname,
			VehicleModel:       data.VehicleModel,
			VehicleColor:       data.VehicleColor,
			VehicleMadeYear:    data.VehicleMadeYear,
			VehicleMotor:       data.VehicleMotor,
			VehicleFullWeight:  data.VehicleFullWeight,
			VehicleEmptyWeight: data.VehicleEmptyWeight,
			VehiclePlateNumber: data.VehiclePlateNumber,
			VehiclePower:       data.VehiclePower,
			PassportNumber:     data.PassportNumber,
			PassportSeria:      data.PassportSeria,
			Shassi:             data.Shassi,
		},
		Status:    data.Status,
		CreatedAt: data.CreatedAt,
	}
	return result, nil
}

func (s *AssessmentRepo) FindAll(ctx context.Context) ([]domain.Assessment, error) {
	var (
		logMsg = "repo.Assessment.FindAll "
		query  = "SELECT id, g_id, created_at FROM assessment"
		result []domain.Assessment
		data   Assessment
	)

	rows, err := s.db.QueryContext(ctx, query)
	if err != nil {
		s.log.Error(logMsg+"s.db.QueryContext failed", logger.Error(err))

		return nil, errs.ErrInternal
	}
	defer func() { _ = rows.Close() }()

	for rows.Next() {

		err = rows.Scan(
			&data.ID,
			&data.GID,
			&data.CreateAt,
		)
		if err != nil {
			s.log.Error(logMsg+"row.Scan failed", logger.Error(err))

			return nil, errs.ErrInternal
		}
		value := domain.Assessment{
			ID:        data.ID,
			GID:       data.GID,
			CreatedAt: data.CreateAt,
		}
		result = append(result, value)
	}

	if len(result) == zero {
		s.log.Error(logMsg+"row.Scan failed", logger.Error(err))

		return nil, errs.ErrDataNotFound
	}

	return result, nil
}

func (s *AssessmentRepo) FindByStatus(ctx context.Context, status, limit, offset, cbid, branchID, roleCode, direct int) ([]domain.Assessment, error) {
	var (
		logMsg = "repo.Assessment.FindByStatus "
		result []domain.Assessment
		data   Assessment

		args  []interface{}
		query = `SELECT s.id, s.cbid, s.a_id, s.p_id, s.g_id, s.iib_info_id, s.created_at, s.updated_at, s.card_id, i.owner_fullname, i.vehicle_plate_number, i.vehicle_kuzov
		FROM assessment s JOIN iib_info i ON s.iib_info_id = i.id WHERE s.status = $1`
		where       string
		columnOrder = 4
		OrderBy     = " Order BY s.created_at desc LIMIT $2 OFFSET $3;"
	)

	args = append(args, status, limit, offset)
	switch roleCode {
	case 100:
		{
			query = fmt.Sprint(query, OrderBy)
		}
	case 90:
		{
			query = fmt.Sprint(query, OrderBy)
		}
	case 80:
		{
			query = fmt.Sprint(query, OrderBy)
		}
	case 70:
		{
			args = append(args, cbid, branchID)
			where = fmt.Sprint(" and (s.cbid = $", columnOrder, " or s.branch_id = $", columnOrder+1, ")")
			query = fmt.Sprint(query, where, OrderBy)
		}
	case 60:
		{
			args = append(args, cbid, branchID, direct)
			where = fmt.Sprint(" and (s.cbid = $", columnOrder, " or (s.branch_id = $", columnOrder+1, " and s.direct = $", columnOrder+2, "))")
			query = fmt.Sprint(query, where, OrderBy)
		}
	case 40:
		{
			args = append(args, cbid, cbid)
			where = fmt.Sprint(" and (s.cbid = $", columnOrder, " or s.ext_cbid = $", columnOrder+1, ")")
			query = fmt.Sprint(query, where, OrderBy)
		}
	case 30:
		{
			query = fmt.Sprint(query, OrderBy)
		}
	case 20:
		{
			args = append(args, cbid)
			where = fmt.Sprint(" and (s.cbid = $", columnOrder, ")")
			query = fmt.Sprint(query, where, OrderBy)
		}
	case 1:
		{
			args = append(args, cbid)
			where = fmt.Sprint(" and (s.cbid = $", columnOrder, ")")
			query = fmt.Sprint(query, where, OrderBy)
		}
	}

	rows, err := s.db.QueryContext(ctx, query, args...)
	if err != nil {
		s.log.Error(logMsg+"s.db.QueryContext failed", logger.Error(err))

		return nil, errs.ErrInternal
	}

	defer func() { _ = rows.Close() }()

	for rows.Next() {
		err = rows.Scan(
			&data.ID,
			&data.CBID,
			pq.Array(&data.AIDs),
			&data.PID,
			&data.GID,
			&data.IIBInfoID,
			&data.CreateAt,
			&data.UpdatedAt,
			&data.CardID,
			&data.OwnerFullName,
			&data.VehiclePlateNumber,
			&data.VehicleKuzov,
		)

		if err != nil {
			s.log.Error(logMsg+"row.Scan failed", logger.Error(err))

			return nil, errs.ErrInternal
		}

		info := domain.Assessment{
			ID:   data.ID,
			CBID: data.CBID,
			AIDs: data.AIDs,
			PID:  data.PID,
			GID:  data.GID,
			GovUz: domain.GovUz{
				VehicleKuzov:       data.VehicleKuzov,
				VehiclePlateNumber: data.VehiclePlateNumber,
				OwnerFullname:      data.OwnerFullName,
			},
			CreatedAt: data.CreateAt,
		}
		if data.IIBInfoID.Valid {
			info.IIBInfoID = data.IIBInfoID.String
		}
		if data.CardID.Valid {
			info.CardID = data.CardID.String
		}
		result = append(result, info)
	}

	if len(result) == zero {
		s.log.Error(logMsg+"row.Scan failed", logger.Error(err))

		return nil, errs.ErrDataNotFound
	}

	return result, nil
}

func (s *AssessmentRepo) GetTotalAssessmentCount(ctx context.Context, status, cbid, branchID, roleCode, direct int) (int, error) {
	var (
		logMsg     = "repo.Assessment.GetTotalAssessmentCount "
		totalCount int

		args        []interface{}
		query       = `SELECT COUNT(*) FROM assessment WHERE status = $1`
		where       string
		columnOrder = 2
	)

	args = append(args, status)
	switch roleCode {
	case 100:
		{
			query = fmt.Sprint(query)
		}
	case 90:
		{
			query = fmt.Sprint(query)
		}
	case 80:
		{
			query = fmt.Sprint(query)
		}
	case 70:
		{
			args = append(args, cbid, branchID)
			where = fmt.Sprint(" and (cbid = $", columnOrder, " or branch_id = $", columnOrder+1, ")")
			query = fmt.Sprint(query, where)
		}
	case 60:
		{
			args = append(args, cbid, branchID, direct)
			where = fmt.Sprint(" and (cbid = $", columnOrder, " or (branch_id = $", columnOrder+1, " and direct = $", columnOrder+2, "))")
			query = fmt.Sprint(query, where)
		}
	case 40:
		{
			args = append(args, cbid, cbid)
			where = fmt.Sprint(" and (cbid = $", columnOrder, " or ext_cbid = $", columnOrder+1, ")")
			query = fmt.Sprint(query, where)
		}
	case 30:
		{
			query = fmt.Sprint(query)
		}
	case 20:
		{
			args = append(args, cbid)
			where = fmt.Sprint(" and (cbid = $", columnOrder, ")")
			query = fmt.Sprint(query, where)
		}
	case 1:
		{
			args = append(args, cbid)
			where = fmt.Sprint(" and (cbid = $", columnOrder, ")")
			query = fmt.Sprint(query, where)
		}
	}

	err := s.db.QueryRowContext(ctx, query, args...).Scan(&totalCount)
	if err != nil {
		s.log.Error(logMsg+"s.db.QueryRowContext failed", logger.Error(err))

		return zero, errs.ErrInternal
	}
	return totalCount, nil
}

func (s *AssessmentRepo) GetAssessmentByBranchID(ctx context.Context, status, branchID int) ([]int, error) {
	var (
		logMsg     = "repo.Assessment.GetAssessmentByBranchID "
		countQuery = "SELECT cbid FROM assessment WHERE status = $1 and branch_id = $2"
		cbid       int
		cbids      []int
	)

	rows, err := s.db.QueryContext(ctx, countQuery, status, branchID)
	if err != nil {
		s.log.Error(logMsg+"s.db.QueryRowContext failed", logger.Error(err))

		return cbids, errs.ErrInternal
	}

	for rows.Next() {

		err = rows.Scan(
			&cbid,
		)

		if err != nil {
			s.log.Error(logMsg+"row.Scan failed", logger.Error(err))

			return cbids, errs.ErrInternal
		}

		cbids = append(cbids, cbid)
	}

	if len(cbids) == zero {
		s.log.Error(logMsg+"len(cbids) == zero", logger.Error(errs.ErrDataNotFound))
		return cbids, errs.ErrDataNotFound
	}

	return cbids, nil
}

func (s *AssessmentRepo) CountAllStatus(ctx context.Context, cbid, branchID, roleCode, direct int) ([]domain.AssessmentStatus, error) {
	var (
		logMsg = "repo.Assessment.CountAllStatus "
		data   AssessmentStatusCount
		result []domain.AssessmentStatus

		args        []interface{}
		query       = `SELECT status, count(status) as count FROM assessment`
		where       string
		columnOrder = 1
		GroupBy     = " GROUP BY status ORDER BY status ASC;"
	)

	// args = append(args, status)
	switch roleCode {
	case 100:
		{
			query = fmt.Sprint(query, GroupBy)
		}
	case 90:
		{
			query = fmt.Sprint(query, GroupBy)
		}
	case 80:
		{
			query = fmt.Sprint(query, GroupBy)
		}
	case 70:
		{
			args = append(args, cbid, branchID)
			where = fmt.Sprint(" where (cbid = $", columnOrder, " or branch_id = $", columnOrder+1, ")")
			query = fmt.Sprint(query, where, GroupBy)
		}
	case 60:
		{
			args = append(args, cbid, branchID, direct)
			where = fmt.Sprint(" where (cbid = $", columnOrder, " or (branch_id = $", columnOrder+1, " and direct = $", columnOrder+2, "))")
			query = fmt.Sprint(query, where, GroupBy)
		}
	case 40:
		{
			args = append(args, cbid, cbid)
			where = fmt.Sprint(" where (cbid = $", columnOrder, " or ext_cbid = $", columnOrder+1, ")")
			query = fmt.Sprint(query, where, GroupBy)
		}
	case 30:
		{
			query = fmt.Sprint(query, GroupBy)
		}
	case 20:
		{
			args = append(args, cbid)
			where = fmt.Sprint(" where (cbid = $", columnOrder, ")")
			query = fmt.Sprint(query, where, GroupBy)
		}
	case 1:
		{
			args = append(args, cbid)
			where = fmt.Sprint(" where (cbid = $", columnOrder, ")")
			query = fmt.Sprint(query, where, GroupBy)
		}
	}

	rows, err := s.db.QueryContext(ctx, query, args...)
	if err != nil {
		s.log.Error(logMsg+"s.db.QueryContext failed", logger.Error(err))

		return result, errs.ErrInternal
	}
	defer func() { _ = rows.Close() }()

	for rows.Next() {

		err = rows.Scan(
			&data.StatusCode,
			&data.StatusCount,
		)

		if err != nil {
			s.log.Error(logMsg+"row.Scan failed", logger.Error(err))

			return result, errs.ErrInternal
		}

		value := domain.AssessmentStatus{
			StatusCode:  data.StatusCode,
			StatusCount: data.StatusCount,
		}
		result = append(result, value)
	}

	if len(result) == zero {
		s.log.Error(logMsg+"row.Scan failed", logger.Error(err))

		return nil, errs.ErrDataNotFound
	}

	return result, nil
}

func (s *AssessmentRepo) UpdateAnalogID(ctx context.Context, req domain.AssessmentStore) error {
	var (
		logMsg = "repo.Assessment.Update "
		query  = "UPDATE assessment SET a_id=$1 where id = $2"
	)

	data := UpdateAnalogID{
		ID:       req.ID,
		AnalogID: req.AIDs,
	}

	row, err := s.db.ExecContext(ctx, query, pq.Array(data.AnalogID), data.ID)
	if err != nil {
		s.log.Error(logMsg+"s.db.ExecContext failed", logger.Error(err))

		return errs.ErrInternal
	}

	if n, err := row.RowsAffected(); n < 1 || err != nil {
		s.log.Error(logMsg+" RowsAffected -- %w ", logger.Error(err))
		return errs.ErrInternal
	}

	return nil
}

func (s *AssessmentRepo) UpdateStatus(ctx context.Context, id string, status int) error {
	var (
		logMsg      = "repo.Assessment.UpdateStatus "
		setParams   = ""
		args        []interface{}
		columnOrder = 3
		where       = " where id = $2"
	)

	args = append(args, status, id)

	if status == 3 {
		setParams = fmt.Sprint(setParams, " , ext_cbid = $", columnOrder)
		args = append(args, 12294)
	} else if status == 6 {
		setParams = fmt.Sprint(setParams, " , ext_cbid = $", columnOrder)
		args = append(args, 6599)
	} else {
		setParams = fmt.Sprint(setParams)
	}

	query := `UPDATE assessment SET status = $1`

	query = fmt.Sprintln(query, setParams, where)

	row, err := s.db.ExecContext(ctx, query, args...)
	if err != nil {
		s.log.Error(logMsg+"s.db.ExecContext failed", logger.Error(err))

		return errs.ErrInternal
	}

	if n, err := row.RowsAffected(); n < 1 || err != nil {
		s.log.Error(logMsg+" RowsAffected -- %w ", logger.Error(err))
		return errs.ErrInternal
	}
	return nil
}

func (s *AssessmentRepo) CountAllAssessments(ctx context.Context) (int, error) {
	var (
		logMsg = "repo.Assessment.CountAllAssessments "
		query  = `SELECT COUNT(id) FROM assessment 
                 WHERE EXTRACT(YEAR FROM created_at) = EXTRACT(YEAR FROM CURRENT_TIMESTAMP) 
                   AND EXTRACT(MONTH FROM created_at) = EXTRACT(MONTH FROM CURRENT_TIMESTAMP)`
		totalCount int
	)
	err := s.db.QueryRowContext(ctx, query).Scan(&totalCount)
	if err != nil {
		s.log.Error(logMsg+"s.db.QueryRowContext failed", logger.Error(err))

		return zero, errs.ErrInternal
	}

	return totalCount, nil
}

func (s *AssessmentRepo) GetReportData(ctx context.Context, id string) (domain.ReportData, error) {
	var (
		logMsg = "repo.Assessment.GetReportData "
		query  = `SELECT a.cbid, a.a_id, a.g_id, a.card_id, a.updated_at,
      	i.owner_fullname, i.owner_pinfl, i.vehicle_kuzov, i.vehicle_plate_number, i.vehicle_made_year
		FROM assessment a JOIN iib_info i ON a.iib_info_id = i.id WHERE a.id = $1;`
		result domain.ReportData
		data   ReportData
	)
	row := s.db.QueryRowContext(ctx, query, id)

	err := row.Scan(
		&data.CBID,
		pq.Array(&data.AnalogID),
		&data.TransportID,
		&data.CardID,
		&data.AssessmentUpdatedtime,
		&data.OwnerFullName,
		&data.OwnerPinfl,
		&data.VIN,
		&data.PlateNum,
		&data.MadeYear,
	)
	if errors.Is(err, sql.ErrNoRows) {
		s.log.Error(logMsg+"row.Scan failed", logger.Error(err))

		return result, errs.ErrDataNotFound
	} else if err != nil {
		s.log.Error(logMsg+"row.Scan failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.ReportData{
		CBID:          data.CBID,
		AnalogID:      data.AnalogID,
		TransportID:   data.TransportID,
		OwnerFullName: data.OwnerFullName,
		OwnerPinfl:    data.OwnerPinfl,
		VIN:           data.VIN,
		PlateNum:      data.PlateNum,
		MadeYear:      data.MadeYear,
		UpdatedAt:     data.AssessmentUpdatedtime,
	}
	if data.CardID.Valid {
		result.CardID = data.CardID.String
	}
	return result, nil
}

func (s *AssessmentRepo) GetGovInfoByAssessmentIDAndStatus(ctx context.Context, cbid, texPassSeria, texPassNum string) (domain.GovInfoWithCBID, error) {
	var (
		logMsg = "repo.Assessment.GetGovInfoByAssessmentIDAndStatus "
		query  = `select a.id,
       		   a.g_id,
			   a.cbid,
			   i.owner_type,
			   i.owner_pinfl,
			   i.owner_fullname,
			   i.owner_date_birth,
			   i.vehicle_id,
			   i.vehicle_plate_number,
			   i.vehicle_model,
			   i.vehicle_color,
			   i.vehicle_kuzov,
			   i.vehicle_motor,
			   i.division,
			   i.iib_registration_date,
			   i.iib_permit_info,
			   i.garov_permit_info,
			   i.inspection,
			   i.vehicle_made_year,
			   i.vehicle_type,
			   i.vehicle_full_weight,
			   i.vehicle_empty_weight,
			   i.vehicle_fuel_type,
			   i.vehicle_seats,
			   i.vehicle_stands,
			   i.texpassport_seria,
			   i.texpassport_number,
			   i.body_type_name,
			   i.shassi,
			   i.power,
			   i.date_schet_spravka,
			   i.tuning_permit,
			   i.tuning_given_date,
			   i.tuning_issue_date,
			   i.prev_pnfl,
			   i.prev_owner,
			   i.prev_owner_type,
			   i.prev_plate_number,
			   i.statee,
			   i.prev_texpassport_seria,
			   i.prev_texpassport_number
			from assessment a join iib_info i on a.iib_info_id = i.id 
			where a.cbid=$1 and i.texpassport_seria=$2 and i.texpassport_number=$3 and a.status=$4;`
		result domain.GovInfoWithCBID
		data   GovInfoWithCBID
	)

	row := s.db.QueryRowContext(ctx, query, cbid, texPassSeria, texPassNum, 7)

	err := row.Scan(
		&data.AssessmentID,
		&data.TransportID,
		&data.CBID,
		&data.OwnerType,
		&data.OwnerPinfl,
		&data.OwnerFullName,
		&data.OwnerDateBirth,
		&data.VehicleID,
		&data.VehiclePlateNumber,
		&data.VehicleModel,
		&data.VehicleColor,
		&data.VehicleKuzov,
		&data.VehicleMotor,
		&data.Division,
		&data.IibRegistrationDate,
		&data.IibPermitInfo,
		&data.GarovPermitInfo,
		&data.Inspection,
		&data.VehicleMadeYear,
		&data.VehicleType,
		&data.VehicleFullWeight,
		&data.VehicleEmptyWeight,
		&data.VehicleFuelType,
		&data.VehicleSeats,
		&data.VehicleStands,
		&data.TexpassportSeria,
		&data.TexpassportNumber,
		&data.BodyTypeName,
		&data.Shassi,
		&data.Power,
		&data.DateSchetSpravka,
		&data.TuningPermit,
		&data.TuningGivenDate,
		&data.TuningIssueDate,
		&data.PrevPnfl,
		&data.PrevOwner,
		&data.PrevOwnerType,
		&data.PrevPlateNumber,
		&data.State,
		&data.PrevTexpasportSeria,
		&data.PrevTexpasportNumber,
	)
	if errors.Is(err, sql.ErrNoRows) {
		s.log.Error(logMsg+"sql.ErrNoRows", logger.Error(err))
		fmt.Println("No rows found.")
		return result, errs.ErrDataNotFound
	} else if err != nil {
		s.log.Error(logMsg+"row.Scan failed", logger.Error(err))
		fmt.Println("Error scanning row:", err)
		return result, errs.ErrInternal
	}
	//if errors.Is(err, sql.ErrNoRows) {
	//	s.log.Error(logMsg+"sql.ErrNoRows", logger.Error(err))
	//
	//	return result, errs.ErrDataNotFound
	//} else if err != nil {
	//	s.log.Error(logMsg+"row.Scan failed", logger.Error(err))
	//
	//	return result, errs.ErrInternal
	//}

	return data.transformToDomain(), nil
}

func (s *AssessmentRepo) DeleteAssessment(ctx context.Context, id string) error {
	var (
		logMsg = "repo.Assessment.DeleteAssessment "
		query  = `DELETE FROM assessment WHERE id=$1`
	)

	row, err := s.db.ExecContext(ctx, query, id)
	if err != nil {
		s.log.Error(logMsg+"m.db.ExecContext failed", logger.Error(err))

		return err
	}

	if n, err := row.RowsAffected(); n < 1 || err != nil {
		s.log.Error(logMsg+"no RowsAffected", logger.Error(err))

		return errs.ErrInternal
	}
	return nil
}

package gov_info_store

import (
	"context"
	"database/sql"
	"pledge/assessment_service/internal/domain"
	"pledge/assessment_service/internal/errs"
	"pledge/assessment_service/pkg/logger"
)

const (
	zero = 0
)

type GovInfoRepo struct {
	db  *sql.DB
	log logger.Logger
}

func New(db *sql.DB, log logger.Logger) *GovInfoRepo {
	return &GovInfoRepo{
		db:  db,
		log: log,
	}
}

func (s *GovInfoRepo) Store(ctx context.Context, info domain.GovUzStore) (string, error) {
	var (
		logMsg = "repo.GovUz.Store "
	)
	data := govUzInfo{
		OwnerType:            info.OwnerType,
		Pinfl:                info.Pinfl,
		OwnerFullname:        info.OwnerFullname,
		VehicleID:            info.VehicleID,
		PlateNumber:          info.PlateNumber,
		Model:                info.Model,
		VehicleColor:         info.VehicleColor,
		RegistrationDate:     info.RegistrationDate,
		Division:             info.Division,
		Year:                 info.Year,
		VehicleType:          info.VehicleType,
		Kuzov:                info.Kuzov,
		FullWeight:           info.FullWeight,
		EmptyWeight:          info.EmptyWeight,
		Motor:                info.Motor,
		FuelType:             info.FuelType,
		Seats:                info.Seats,
		Stands:               info.Stands,
		Comments:             info.Comments,
		TexpassportSeria:     info.TexpassportSeria,
		TexpassportNumber:    info.TexpassportNumber,
		BodyTypeName:         info.BodyTypeName,
		Shassi:               info.Shassi,
		Power:                info.Power,
		DateSchetSpravka:     info.DateSchetSpravka,
		TuningPermit:         info.TuningPermit,
		TuningGivenDate:      info.TuningGivenDate,
		TuningIssueDate:      info.TuningIssueDate,
		PrevPnfl:             info.PrevPnfl,
		PrevOwner:            info.PrevOwner,
		PrevOwnerType:        info.PrevOwnerType,
		PrevPlateNumber:      info.PlateNumber,
		PrevTexpasportSery:   info.PrevTexpasportSery,
		PrevTexpasportNumber: info.PrevTexpasportNumber,
		State:                info.State,
		Inspection:           info.Inspection,
	}

	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		s.log.Error(logMsg+"s.db.BeginTx failed", logger.Error(err))

		return "", errs.ErrInternal
	}

	defer func() { _ = tx.Rollback() }()

	query := "INSERT INTO iib_info(owner_type, owner_pinfl, owner_fullname, vehicle_id, vehicle_plate_number, vehicle_model, vehicle_color, iib_registration_date, division, vehicle_made_year, vehicle_type,vehicle_kuzov,vehicle_full_weight,vehicle_empty_weight,vehicle_motor,vehicle_fuel_type,vehicle_seats,vehicle_stands,iib_permit_info,garov_permit_info,inspection, texpassport_seria, texpassport_number, body_type_name, shassi, power, date_schet_spravka, tuning_permit, tuning_given_date, tuning_issue_date,prev_pnfl, prev_owner, prev_owner_type, prev_plate_number, prev_texpassport_seria, prev_texpassport_number, statee) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23, $24, $25, $26, $27, $28, $29, $30, $31, $32, $33, $34, $35, $36, $37) RETURNING id"

	row := tx.QueryRow(query, data.OwnerType, data.Pinfl, data.OwnerFullname, data.VehicleID, data.PlateNumber, data.Model, data.VehicleColor, data.RegistrationDate, data.Division, data.Year, data.VehicleType, data.Kuzov, data.FullWeight, data.EmptyWeight, data.Motor, data.FuelType, data.Seats, data.Stands, data.Comments, "NOT EXISTS", data.Inspection, data.TexpassportSeria, data.TexpassportNumber, data.BodyTypeName, data.Shassi, data.Power, data.DateSchetSpravka, data.TuningPermit, data.TuningGivenDate, data.TuningIssueDate, data.PrevPnfl, data.PrevOwner, data.PrevOwnerType, data.PrevPlateNumber, data.PrevTexpasportSery, data.PrevTexpasportNumber, data.State)

	if err := row.Scan(&data.ID); err != nil {
		s.log.Error(logMsg+"row.Scan failed", logger.Error(err))

		return "", errs.ErrInternal
	}

	if err := tx.Commit(); err != nil {
		s.log.Error(logMsg+"tx.Commit failed", logger.Error(err))

		return "", err
	}

	return data.ID, nil
}

func (s *GovInfoRepo) Update(ctx context.Context, id string, info domain.GovUzStore) error {
	var (
		logMsg = "repo.GovUz.Update "
		query  = `UPDATE iib_info 
	SET 
    owner_type=$1, 
    owner_pinfl=$2, 
    owner_fullname=$3, 
    vehicle_id=$4, 
    vehicle_plate_number=$5, 
    vehicle_model=$6, 
    vehicle_color=$7, 
    iib_registration_date=$8, 
    division=$9, 
    vehicle_made_year=$10, 
    vehicle_type=$11,
    vehicle_kuzov=$12,
    vehicle_full_weight=$13,
    vehicle_empty_weight=$14,
    vehicle_motor=$15,
    vehicle_fuel_type=$16,
    vehicle_seats=$17,
    vehicle_stands=$18,
    iib_permit_info=$19,
    garov_permit_info=$20,
    inspection=$21, 
    texpassport_seria=$22, 
    texpassport_number=$23, 
    body_type_name=$24, 
    shassi=$25, 
    power=$26, 
    date_schet_spravka=$27, 
    tuning_permit=$28, 
    tuning_given_date=$29, 
    tuning_issue_date=$30,
    prev_pnfl=$31, 
    prev_owner=$32, 
    prev_owner_type=$33, 
    prev_plate_number=$34, 
    prev_texpassport_seria=$35, 
    prev_texpassport_number=$36, 
    statee=$37 
	where id = $38`
	)
	data := govUzInfo{
		OwnerType:            info.OwnerType,
		Pinfl:                info.Pinfl,
		OwnerFullname:        info.OwnerFullname,
		VehicleID:            info.VehicleID,
		PlateNumber:          info.PlateNumber,
		Model:                info.Model,
		VehicleColor:         info.VehicleColor,
		RegistrationDate:     info.RegistrationDate,
		Division:             info.Division,
		Year:                 info.Year,
		VehicleType:          info.VehicleType,
		Kuzov:                info.Kuzov,
		FullWeight:           info.FullWeight,
		EmptyWeight:          info.EmptyWeight,
		Motor:                info.Motor,
		FuelType:             info.FuelType,
		Seats:                info.Seats,
		Stands:               info.Stands,
		Comments:             info.Comments,
		TexpassportSeria:     info.TexpassportSeria,
		TexpassportNumber:    info.TexpassportNumber,
		BodyTypeName:         info.BodyTypeName,
		Shassi:               info.Shassi,
		Power:                info.Power,
		DateSchetSpravka:     info.DateSchetSpravka,
		TuningPermit:         info.TuningPermit,
		TuningGivenDate:      info.TuningGivenDate,
		TuningIssueDate:      info.TuningIssueDate,
		PrevPnfl:             info.PrevPnfl,
		PrevOwner:            info.PrevOwner,
		PrevOwnerType:        info.PrevOwnerType,
		PrevPlateNumber:      info.PlateNumber,
		PrevTexpasportSery:   info.PrevTexpasportSery,
		PrevTexpasportNumber: info.PrevTexpasportNumber,
		State:                info.State,
		Inspection:           info.Inspection,
	}

	row, err := s.db.ExecContext(ctx, query, data.OwnerType, data.Pinfl, data.OwnerFullname, data.VehicleID, data.PlateNumber, data.Model, data.VehicleColor, data.RegistrationDate, data.Division, data.Year, data.VehicleType, data.Kuzov, data.FullWeight, data.EmptyWeight, data.Motor, data.FuelType, data.Seats, data.Stands, data.Comments, "NOT EXISTS", data.Inspection, data.TexpassportSeria, data.TexpassportNumber, data.BodyTypeName, data.Shassi, data.Power, data.DateSchetSpravka, data.TuningPermit, data.TuningGivenDate, data.TuningIssueDate, data.Pinfl, data.PrevOwner, data.PrevOwnerType, data.PrevPlateNumber, data.PrevTexpasportSery, data.PrevTexpasportNumber, data.State, id)
	if err != nil {
		s.log.Error(logMsg+"s.db.ExecContext failed", logger.Error(err))

		return errs.ErrInternal
	}

	if n, err := row.RowsAffected(); n < 1 || err != nil {
		s.log.Error(logMsg+" RowsAffected -- %w ", logger.Error(err))
		return errs.ErrInternal
	}

	return nil
}

func (s *GovInfoRepo) IncrementAttempt(ctx context.Context, id string) error {
	var (
		logMsg = "repo.GovUz.IncrementAttempt "
		query  = "UPDATE iib_info SET count = $1 where id = $2"
	)

	count, err := s.getCount(ctx, id)
	if err != nil {
		s.log.Error(logMsg+"s.getCount failed", logger.Error(err))

		return err
	}
	count++

	row, err := s.db.ExecContext(ctx, query, count, id)
	if err != nil {
		s.log.Error(logMsg+"s.db.ExecContext failed", logger.Error(err))

		return errs.ErrInternal
	}

	if n, err := row.RowsAffected(); n < 1 || err != nil {
		s.log.Error(logMsg+" RowsAffected -- %w ", logger.Error(err))
		return errs.ErrInternal
	}

	return nil
}

func (s *GovInfoRepo) getCount(ctx context.Context, id string) (int, error) {
	var (
		logMsg = "repo.GovUz.getCount "
		query  = `SELECT count FROM iib_info ii WHERE ii.id= $1`
		count  sql.NullInt64
	)
	row := s.db.QueryRowContext(ctx, query, id)

	if err := row.Scan(&count); err != nil {
		s.log.Error(logMsg+"row.Scan failed", logger.Error(err))

		return zero, errs.ErrInternal
	}

	return int(count.Int64), nil
}

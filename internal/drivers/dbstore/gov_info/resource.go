package gov_info_store

import (
	"time"
)

type govUzInfo struct {
	ID                   string    `db:"id"`
	Pinfl                string    `db:"pinfl"`
	OwnerFullname        string    `db:"owner"`
	OwnerType            int       `db:"owner_type"`
	VehicleID            int       `db:"vehicle_id"`
	PlateNumber          string    `db:"plate_number"`
	Model                string    `db:"model"`
	VehicleColor         string    `db:"vehicle_color"`
	RegistrationDate     string    `db:"registration_date"`
	Division             string    `db:"division"`
	Year                 int       `db:"year"`
	VehicleType          int       `db:"vehicle_type"`
	Kuzov                string    `db:"kuzov"`
	FullWeight           int       `db:"full_weight"`
	EmptyWeight          int       `db:"empty_weight"`
	Motor                string    `db:"motor"`
	FuelType             int       `db:"fuel_type"`
	Seats                int       `db:"seats"`
	Stands               int       `db:"stands"`
	Comments             string    `db:"comments"`
	Inspection           string    `db:"inspection"`
	TexpassportSeria     string    `db:"texpassport_seria"`
	TexpassportNumber    string    `db:"texpassport_number"`
	BodyTypeName         string    `db:"body_type_name"`
	Shassi               string    `db:"shassi"`
	Power                int       `db:"power"`
	DateSchetSpravka     string    `db:"date_schet_spravka"`
	TuningPermit         string    `db:"tuning_permit"`
	TuningGivenDate      string    `db:"tuning_given_date"`
	TuningIssueDate      string    `db:"tuning_issue_date"`
	PrevPnfl             string    `db:"prev_pnfl"`
	PrevOwner            string    `db:"prev_owner"`
	PrevOwnerType        string    `db:"prev_owner_type"`
	PrevPlateNumber      string    `db:"prev_plate_number"`
	PrevTexpasportSery   string    `db:"prev_texpasport_seria"`
	PrevTexpasportNumber string    `db:"prev_texpasport_number"`
	State                string    `db:"state"`
	CreateAt             time.Time `db:"created_at"`
}

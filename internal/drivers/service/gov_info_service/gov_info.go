package gov_info_service

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"pledge/assessment_service/internal/config"
	"pledge/assessment_service/internal/domain"
	"pledge/assessment_service/internal/errs"
	"pledge/assessment_service/pkg/logger"
	"time"

	"github.com/google/uuid"
)

const (
	grantType    = "client_credentials"
	tokenPath    = "/oauth2/token"
	govUzPathNew = "/newgovuz/v1/yhxbb/service/carinfo/v1"
)

type Client struct {
	Client *http.Client
	cfg    config.Config
	token  string
	log    logger.Logger
}

func New(cfg config.Config, log logger.Logger) *Client {
	return &Client{
		Client: &http.Client{
			Timeout: time.Second * 10,
		},
		cfg: cfg,
		log: log,
	}
}

type Data struct {
	GrantType string `json:"grant_type"`
}

//goland:noinspection ALL
func (c *Client) GetToken(ctx context.Context) (string, error) {
	var (
		logMsg   = "service.GovUz.GetToken "
		response tokenResponse
		url      = fmt.Sprint(c.cfg.OpenApiTokenUrl, tokenPath)
	)

	data := Data{
		GrantType: grantType,
	}

	reqData, err := json.Marshal(data)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal failed", logger.Error(err))

		return "", errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPost, url, bytes.NewBuffer(reqData))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext failed", logger.Error(err))

		return "", errs.ErrInternal
	}

	request.Header.Add("Content-Type", "application/json")
	request.SetBasicAuth(c.cfg.OpenApiUsername, c.cfg.OpenApiPassword)

	resp, err := c.Client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.Client.Do failed", logger.Error(err))

		return "", errs.ErrInternal
	}
	defer resp.Body.Close()

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll failed", logger.Error(err))

		return "", errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal failed", logger.Error(err))

		return "", errs.ErrInternal
	}

	c.token = response.AccessToken

	return response.AccessToken, nil
}

func (c *Client) SearchByCarNum(ctx context.Context, info domain.GovUzParams) (domain.GovUzResponse, error) {
	var (
		logMsg = "service.GovUz.SearchByTechNum "
		result domain.GovUzResponse
		data   govUzResponse
		url    = fmt.Sprint(c.cfg.OpenApiUrl + govUzPathNew)
	)
	val := CarNumber{
		PassportNumber: info.PassportNumber,
		PassportSeria:  info.PassportSeria,
		PlateNumber:    info.PlateNumber,
	}

	val.RequestID = uuid.NewString()
	if c.token == "" {
		token, err := c.GetToken(ctx)
		if err != nil {
			c.log.Error(logMsg+"c.token == empty, c.GetToken failed", logger.Error(err))

			return result, errs.ErrInternal
		}
		c.token = token
	}

	reqData, err := json.Marshal(val)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPost, url, bytes.NewBuffer(reqData))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext failed", logger.Error(err))

		return result, err
	}

	request.Header.Add("Content-Type", "application/json; charset=UTF-8")
	request.Header.Add("Authorization", "Bearer "+c.token)

	resp, err := c.Client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.Client.Do failed", logger.Error(err))

		return result, errs.ErrInternal
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusUnauthorized {
		token, err := c.GetToken(ctx)
		if err != nil {
			c.log.Error(logMsg+"c.GetToken failed", logger.Error(err))

			return result, err
		}
		c.token = token
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &data); err != nil {
		c.log.Error(logMsg+"json.Unmarshal failed", logger.Error(err))
		c.log.Info(fmt.Sprint(logMsg+"Response", string(res)))

		return result, errs.ErrInternal
	}

	if data.PResult != 1 {
		c.log.Error(logMsg + "data.PResult != 1")
		c.log.Info(fmt.Sprint(logMsg+"Response", string(res)))

		return result, errs.New(data.PComment)
	} else {
		result = domain.GovUzResponse{
			Result:          data.PResult,
			Comment:         data.PComment,
			OwnerType:       data.POwnerType,
			Pinfl:           data.PPinpp,
			OrganizationInn: fmt.Sprint(data.POrganizationInn),
			OwnerFullname:   data.POwner,
		}

		for _, vehicle := range data.Vehicle {

			value := domain.Vehicle{
				VehicleID:            vehicle.PVehicleID,
				TexpassportSeria:     vehicle.PTexpassportSery,
				TexpassportNumber:    vehicle.PTexpassportNumber,
				PlateNumber:          vehicle.PPlateNumber,
				Model:                vehicle.PModel,
				VehicleColor:         vehicle.PVehicleColor,
				RegistrationDate:     vehicle.PRegistrationDate,
				Division:             fmt.Sprint(vehicle.PDivision),
				Year:                 vehicle.PYear,
				VehicleType:          vehicle.PVehicleType,
				Kuzov:                vehicle.PKuzov,
				FullWeight:           vehicle.PFullWeight,
				EmptyWeight:          vehicle.PEmptyWeight,
				Motor:                vehicle.PMotor,
				FuelType:             vehicle.PFuelType,
				Seats:                vehicle.PSeats,
				Stands:               vehicle.PStands,
				Comments:             fmt.Sprint(vehicle.PComments),
				Inspection:           fmt.Sprint(vehicle.NextInspectionDate),
				BodyTypeName:         vehicle.PBodyTypeName,
				Shassi:               vehicle.PShassi,
				Power:                vehicle.PPower,
				DateSchetSpravka:     vehicle.PDateSchetSpravka,
				TuningPermit:         vehicle.PTuningPermit,
				TuningGivenDate:      fmt.Sprint(vehicle.PTuningGivenDate),
				TuningIssueDate:      fmt.Sprint(vehicle.PTuningIssueDate),
				PrevPnfl:             fmt.Sprint(vehicle.PrevPnfl),
				PrevOwner:            fmt.Sprint(vehicle.PrevOwner),
				PrevOwnerType:        fmt.Sprint(vehicle.PrevOwnerType),
				PrevPlateNumber:      fmt.Sprint(vehicle.PrevPlateNumber),
				PrevTexpasportSery:   fmt.Sprint(vehicle.PrevTexpasportSery),
				PrevTexpasportNumber: fmt.Sprint(vehicle.PrevTexpasportNumber),
			}
			result.Vehicle = append(result.Vehicle, value)
		}
		return result, nil
	}
}

package gov_info_service

type tokenResponse struct {
	AccessToken string `json:"access_token"`
	Scope       string `json:"scope"`
	TokenType   string `json:"token_type"`
	ExpiresIn   int    `json:"expires_in"`
}

type CarNumber struct {
	RequestID      string `json:"pRequestID"`
	PassportNumber string `json:"pTexpassportNumber"`
	PassportSeria  string `json:"pTexpassportSery"`
	PlateNumber    string `json:"pPlateNumber"`
	INN            string `json:"requestInn"`
	PINFL          string `json:"requestPinfl"`
}

// type technicalPass struct {
// 	RequestID      string `json:"pRequestID"`
// 	PassportNumber string `json:"pTexpassportNumber"`
// 	PassportSeria  string `json:"pTexpassportSery"`
// }
// type vinNumber struct {
// 	RequestID string `json:"pRequestID"`
// 	VinNumber string `json:"pVinNumber"`
// }

type govUzResponse struct {
	PResult          int         `json:"pResult"`
	PComment         string      `json:"pComment"`
	PPinpp           string      `json:"pPinpp"`
	POwner           string      `json:"pOwner"`
	POwnerType       int         `json:"pOwnerType"`
	POrganizationInn interface{} `json:"pOrganizationInn"`
	Vehicle          []Vehicle   `json:"Vehicle"`
}

type Vehicle struct {
	PVehicleID           int         `json:"pVehicleId"`
	PTexpassportSery     string      `json:"pTexpassportSery"`
	PTexpassportNumber   string      `json:"pTexpassportNumber"`
	PPlateNumber         string      `json:"pPlateNumber"`
	PModel               string      `json:"pModel"`
	PVehicleColor        string      `json:"pVehicleColor"`
	PRegistrationDate    string      `json:"pRegistrationDate"`
	PDivision            interface{} `json:"pDivision"`
	PYear                int         `json:"pYear"`
	PVehicleType         int         `json:"pVehicleType"`
	PBodyTypeName        string      `json:"pBodyTypeName"`
	PKuzov               string      `json:"pKuzov"`
	PShassi              string      `json:"pShassi"`
	PFullWeight          int         `json:"pFullWeight"`
	PEmptyWeight         int         `json:"pEmptyWeight"`
	PMotor               string      `json:"pMotor"`
	PFuelType            int         `json:"pFuelType"`
	PSeats               int         `json:"pSeats"`
	PStands              int         `json:"pStands"`
	PComments            interface{} `json:"pComments"`
	PPower               int         `json:"pPower"`
	PDateSchetSpravka    string      `json:"pDateSchetSpravka"`
	PTuningPermit        string      `json:"pTuningPermit"`
	PTuningGivenDate     interface{} `json:"pTuningGivenDate"`
	PTuningIssueDate     interface{} `json:"pTuningIssueDate"`
	PrevPnfl             interface{} `json:"prevPnfl"`
	PrevOwner            interface{} `json:"prevOwner"`
	PrevOwnerType        interface{} `json:"prevOwnerType"`
	PrevPlateNumber      interface{} `json:"prevPlateNumber"`
	PrevTexpasportSery   interface{} `json:"prevTexpasportSery"`
	PrevTexpasportNumber interface{} `json:"prevTexpasportNumber"`
	State                interface{} `json:"State"`
	NextInspectionDate   interface{} `json:"NextInspectionDate"`
}

// type govUzResponse struct {
// 	PResult           int                 `json:"pResult"`
// 	PComment          string              `json:"pComment"`
// 	POwnerType        int                 `json:"pOwnerType"`
// 	PPinpp            string              `json:"pPinpp"`
// 	POwnerInn         string              `json:"pOwnerInn"`
// 	POwner            string              `json:"pOwner"`
// 	PSurname          interface{}         `json:"pSurname"`
// 	PName             interface{}         `json:"pName"`
// 	PPatronym         interface{}         `json:"pPatronym"`
// 	PDateBirth        interface{}         `json:"pDateBirth"`
// 	VehicleInfoResult []vehicleInfoResult `json:"vehicleInfoResult"`
// }

// type vehicleInfoResult struct {
// 	PVehicleID        int         `json:"pVehicleId"`
// 	PPlateNumber      string      `json:"pPlateNumber"`
// 	PModel            string      `json:"pModel"`
// 	PVehicleColor     string      `json:"pVehicleColor"`
// 	PRegistrationDate string      `json:"pRegistrationDate"`
// 	PDivision         string      `json:"pDivision"`
// 	PYear             string      `json:"pYear"`
// 	PVehicleType      string      `json:"pVehicleType"`
// 	PKuzov            string      `json:"pKuzov"`
// 	PFullWeight       string      `json:"pFullWeight"`
// 	PEmptyWeight      string      `json:"pEmptyWeight"`
// 	PMotor            string      `json:"pMotor"`
// 	PFuelType         string      `json:"pFuelType"`
// 	PSeats            string      `json:"pSeats"`
// 	PStands           string      `json:"pStands"`
// 	PComments         string      `json:"pComments"`
// 	Inspection        interface{} `json:"inspection"`
// }

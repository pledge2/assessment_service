package identification_service

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"pledge/assessment_service/internal/config"
	"pledge/assessment_service/internal/domain"
	"pledge/assessment_service/internal/errs"
	service "pledge/assessment_service/pkg/http"
	"pledge/assessment_service/pkg/logger"
	"pledge/assessment_service/pkg/status"
	"strconv"
)

type Client struct {
	Client *http.Client
	cfg    config.Config
	log    logger.Logger
}

func New(cfg config.Config, log logger.Logger) *Client {
	return &Client{
		Client: &http.Client{},
		cfg:    cfg,
		log:    log,
	}
}

func (c *Client) GetEmpInfo(ctx context.Context, cbid int) (domain.EmpData, error) {
	var (
		logMsg   = "service.Identification.GetUserInfo "
		response service.DBOResponse
		data     UserInfo
		result   domain.EmpData
	)

	url := c.cfg.IdentificationService + userInfo

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext failed", logger.Error(err))

		return result, errs.ErrInternal
	}
	q := request.URL.Query()
	q.Add("cbid", fmt.Sprint(cbid))

	request.URL.RawQuery = q.Encode()

	resp, err := c.Client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.Client.Do failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal failed", logger.Error(err))
		c.log.Info(fmt.Sprint("Response", string(res)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(fmt.Sprint("Response", string(res)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.EmpData{
		EmpCBID:     data.CBID,
		EmpRole:     data.Role,
		EmpName:     data.Name,
		EmpPosition: data.Position,
		EmpBranchID: strconv.Itoa(data.BranchID),
	}

	return result, nil
}

// func (c *Client) GetRoles(ctx context.Context) (domain.EmpData, error) {
// 	var (
// 		logMsg   = "service.Identification.GetRoles "
// 		response service.DBOResponse
// 		data     UserInfo
// 		result   domain.EmpData
// 	)

// 	url := c.cfg.IdentificationService + userRole

// 	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
// 	if err != nil {
// 		c.log.Error(logMsg+"http.NewRequestWithContext failed", logger.Error(err))

// 		return result, errs.ErrInternal
// 	}
// 	q := request.URL.Query()
// 	q.Add("cbid", fmt.Sprint(cbid))

// 	request.URL.RawQuery = q.Encode()

// 	resp, err := c.Client.Do(request)
// 	if err != nil {
// 		c.log.Error(logMsg+"c.Client.Do failed", logger.Error(err))

// 		return result, errs.ErrInternal
// 	}

// 	res, err := io.ReadAll(resp.Body)
// 	if err != nil {
// 		c.log.Error(logMsg+"io.ReadAll failed", logger.Error(err))

// 		return result, errs.ErrInternal
// 	}

// 	if err = json.Unmarshal(res, &response); err != nil {
// 		c.log.Error(logMsg+"json.Unmarshal failed", logger.Error(err))
// 		c.log.Info(fmt.Sprint("Response", string(res)))

// 		return result, errs.ErrInternal
// 	}

// 	if response.ErrorCode != status.NoError {
// 		c.log.Info(fmt.Sprint("Response", string(res)))

// 		return result, errs.New(response.ErrorNote)
// 	}

// 	if err = response.Scan(&data); err != nil {
// 		c.log.Error(logMsg+"response.Scan failed", logger.Error(err))

// 		return result, errs.ErrInternal
// 	}

// 	result = domain.EmpData{
// 		EmpCBID:     data.CBID,
// 		EmpRole:     data.Role,
// 		EmpName:     data.Name,
// 		EmpPosition: data.Position,
// 		EmpBranchID: strconv.Itoa(data.BranchID),
// 	}

// 	return result, nil
// }

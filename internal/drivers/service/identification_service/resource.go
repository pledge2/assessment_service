package identification_service

const userInfo = "/user/info"

// const userRole = "/role/find/all"

type UserInfo struct {
	Name     string `json:"name"`
	CBID     int    `json:"cbid"`
	Role     int    `json:"role"`
	Position string `json:"position"`
	BranchID int    `json:"branch_id"`
}

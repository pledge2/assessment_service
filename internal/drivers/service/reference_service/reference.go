package reference_service

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"pledge/assessment_service/internal/config"
	"pledge/assessment_service/internal/domain"
	"pledge/assessment_service/internal/errs"
	service "pledge/assessment_service/pkg/http"
	"pledge/assessment_service/pkg/logger"
	"pledge/assessment_service/pkg/status"
)

const (
	findTypeByIDEndPoint     = "/type/find/one"
	findModelByIDEndPoint    = "/model/find/one"
	findNameByIDEndPoint     = "/name/find/one"
	findPositionByIDEndPoint = "/position/find/one"
	findDiscountAllEndPoint  = "/discount/all"
	referenceFindNameById    = "/reference/find/name"
	fuelControlByNameID      = "/fuel/control/all"
)

type Client struct {
	Client *http.Client
	cfg    config.Config
	log    logger.Logger
}

func New(cfg config.Config, log logger.Logger) *Client {
	return &Client{
		Client: &http.Client{},
		cfg:    cfg,
		log:    log,
	}
}

func (c *Client) GetTypeByID(ctx context.Context, id string) (domain.TransportType, error) {
	var (
		logMsg   = "service.Reference.GetTypeByID "
		url      = c.cfg.ReferenceService + findTypeByIDEndPoint + "?id=" + id
		response service.DBOResponse
		result   domain.TransportType
		data     typeResponse
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.Client.Get(request.URL.String())
	if err != nil {
		c.log.Error(logMsg+"c.Client.Get failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal failed", logger.Error(err))
		c.log.Info(fmt.Sprint(logMsg+"Response", string(res)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(fmt.Sprint(logMsg+"Response", string(res)))

		return result, errs.New(response.ErrorNote)
	}
	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+" result.Scan failed", logger.Error(err))

		return result, errs.ErrInternal
	}
	result = domain.TransportType{
		ID:      data.ID,
		TypeKey: data.TypeKey,
		Type:    data.Type,
	}
	return result, nil
}

func (c *Client) GetModelByID(ctx context.Context, id string) (domain.TransportModel, error) {
	var (
		logMsg   = "service.Reference.GetModelByID "
		url      = c.cfg.ReferenceService + findModelByIDEndPoint
		response service.DBOResponse
		result   domain.TransportModel
		data     transportModel
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext failed", logger.Error(err))
		return result, errs.ErrInternal
	}

	q := request.URL.Query()
	q.Add("id", id)

	request.URL.RawQuery = q.Encode()

	resp, err := c.Client.Get(request.URL.String())
	if err != nil {
		c.log.Error(logMsg+"c.Client.Get failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal failed", logger.Error(err))
		c.log.Info(fmt.Sprint(logMsg+"Response", string(res)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(fmt.Sprint(logMsg+"Response", string(res)))

		return result, errs.New(response.ErrorNote)
	}
	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+" result.Scan failed", logger.Error(err))

		return result, errs.ErrInternal
	}
	result = domain.TransportModel{
		ID:     data.ID,
		TypeID: data.TypeID,
		Model:  data.Model,
		Active: data.Active,
	}
	return result, nil
}

func (c *Client) GetNameByID(ctx context.Context, id string) (domain.TransportName, error) {
	var (
		logMsg   = "service.Reference.GetNameByID "
		url      = c.cfg.ReferenceService + findNameByIDEndPoint
		response service.DBOResponse
		result   domain.TransportName
		data     transportName
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+" http.NewRequestWithContext failed", logger.Error(err))

		return result, errs.ErrInternal
	}
	q := request.URL.Query()
	q.Add("id", id)
	request.URL.RawQuery = q.Encode()

	resp, err := c.Client.Get(request.URL.String())
	if err != nil {
		c.log.Error(logMsg+"c.Client.Get failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal failed", logger.Error(err))
		c.log.Info(fmt.Sprint(logMsg+"Response", string(res)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(fmt.Sprint(logMsg+"Response", string(res)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+" result.Scan failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.TransportName{
		ID:             data.ID,
		ModelID:        data.ModelID,
		Name:           data.Name,
		NameKey:        data.NameKey,
		TransportGroup: data.TransportGroup,
		Active:         data.Active,
	}
	return result, nil
}

func (c *Client) GetPositionByID(ctx context.Context, id string) (domain.TransportPosition, error) {
	var (
		logMsg   = "service.Reference.GetNameByID "
		url      = c.cfg.ReferenceService + findPositionByIDEndPoint
		result   domain.TransportPosition
		response service.DBOResponse
		data     transportPosition
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+" http.NewRequestWithContext failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	q := request.URL.Query()
	q.Add("id", id)
	request.URL.RawQuery = q.Encode()

	resp, err := c.Client.Get(request.URL.String())
	if err != nil {
		c.log.Error(logMsg+"c.Client.Get failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal failed", logger.Error(err))
		c.log.Info(fmt.Sprint(logMsg+"Response", string(res)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(fmt.Sprint("Response", string(res)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+" result.Scan failed", logger.Error(err))

		return result, errs.ErrInternal
	}
	result = domain.TransportPosition{
		ID:       data.ID,
		NameID:   data.NameID,
		Position: data.Position,
		Active:   data.Active,
	}
	return result, nil
}

func (c *Client) GetDiscount(ctx context.Context, nameID string) (domain.Discounts, error) {
	var (
		logMsg   = "service.Reference.GetDiscount "
		url      = c.cfg.ReferenceService + findDiscountAllEndPoint
		result   domain.Discounts
		response service.DBOResponse
		data     discounts
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+" http.NewRequestWithContext failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	q := request.URL.Query()
	q.Add("name_id", nameID)
	request.URL.RawQuery = q.Encode()

	resp, err := c.Client.Get(request.URL.String())
	if err != nil {
		c.log.Error(logMsg+"c.Client.Get failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal failed", logger.Error(err))
		c.log.Info(fmt.Sprint(logMsg+"Response", string(res)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(fmt.Sprint(logMsg+"Response", string(res)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+" result.Scan failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	for _, val := range data.Discounts {
		discounts := discountParam{
			Code:    val.Code,
			Percent: val.Percent,
		}
		result.DiscountParams = append(result.DiscountParams, domain.DiscountParams(discounts))
	}

	result.ID = data.ID
	result.NameID = data.NameID

	return result, nil
}

func (c *Client) ReferenceFindName(ctx context.Context, val domain.ReferenceNames) (domain.ReferenceNames, error) {
	var (
		logMsg   = "service.Reference.ReferenceFindName "
		url      = c.cfg.ReferenceService + referenceFindNameById
		response service.DBOResponse
		data     referenceNames
		result   domain.ReferenceNames
	)
	req := referenceNames{
		Type:     val.Type,
		Model:    val.Model,
		Name:     val.Name,
		Position: val.Position,
		Fuel:     val.Fuel,
	}

	reqBytes, err := json.Marshal(req)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPost, url, bytes.NewBuffer(reqBytes))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.Client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.Client.Do failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", req))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"info.Scan failed", logger.Error(err))

		return result, errs.ErrInternal
	}
	result = domain.ReferenceNames{
		Type:     data.Type,
		Model:    data.Model,
		Name:     data.Name,
		Position: data.Position,
		Fuel:     data.Fuel,
	}

	return result, nil
}

type fuelControlParam struct {
	FuelID    string `json:"fuel_id"`
	Fuel      string `json:"fuel"`
	Price     int    `json:"price"`
	CreatedAt string `json:"created_at"`
}

type fuelControls struct {
	ID           string             `json:"id"`
	NameID       string             `json:"name_id"`
	FuelControls []fuelControlParam `json:"fuel_controls"`
}

func (c *Client) FuelControls(ctx context.Context, nameID string) (domain.FuelControls, error) {
	var (
		logMsg   = "service.Reference.FuelControls "
		url      = c.cfg.ReferenceService + fuelControlByNameID
		result   domain.FuelControls
		response service.DBOResponse
		data     fuelControls
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+" http.NewRequestWithContext failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	q := request.URL.Query()
	q.Add("name_id", nameID)
	request.URL.RawQuery = q.Encode()

	resp, err := c.Client.Get(request.URL.String())
	if err != nil {
		c.log.Error(logMsg+"c.Client.Get failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal failed", logger.Error(err))
		c.log.Info(fmt.Sprint(logMsg+"Response", string(res)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(fmt.Sprint(logMsg+"Response", string(res)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+" result.Scan failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	for _, val := range data.FuelControls {
		fuels := domain.FuelControlParam{
			FuelID:    val.FuelID,
			Fuel:      val.Fuel,
			Price:     val.Price,
			CreatedAt: val.CreatedAt,
		}
		result.FuelControls = append(result.FuelControls, fuels)
	}

	result.ID = data.ID
	result.NameID = data.NameID

	return result, nil
}

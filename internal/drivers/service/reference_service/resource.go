package reference_service

type typeResponse struct {
	ID        string `json:"id"`
	TypeKey   string `json:"type_key"`
	Type      string `json:"type"`
	Active    bool   `json:"active"`
	PhotoUrl  string `json:"photo_url"`
	CreatedAt string `json:"created_at"`
}

type transportModel struct {
	ID     string `json:"id"`
	TypeID string `json:"type_id"`
	Model  string `json:"model"`
	Active bool   `json:"active"`
}

type transportName struct {
	ID             string `json:"id"`
	ModelID        string `json:"model_id"`
	Name           string `json:"name"`
	NameKey        string `json:"name_key"`
	TransportGroup string `json:"transport_group"`
	Active         bool   `json:"active"`
}

type transportPosition struct {
	ID       string `json:"id"`
	NameID   string `json:"name_id"`
	Position string `json:"position"`
	Active   bool   `json:"active"`
}

type discountParam struct {
	Code      int    `json:"code"`
	Percent   int    `json:"percent"`
	CreatedAt string `json:"created_at"`
}

type discounts struct {
	ID        string          `json:"id"`
	NameID    string          `json:"name_id"`
	Discounts []discountParam `json:"discounts"`
}

type referenceNames struct {
	Type     string `json:"type"`
	Model    string `json:"model"`
	Name     string `json:"name"`
	Position string `json:"position"`
	Fuel     string `json:"fuel"`
}

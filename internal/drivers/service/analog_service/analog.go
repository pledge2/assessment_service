package analog_service

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"pledge/assessment_service/internal/config"
	"pledge/assessment_service/internal/domain"
	"pledge/assessment_service/internal/errs"
	service "pledge/assessment_service/pkg/http"
	"pledge/assessment_service/pkg/logger"
	"pledge/assessment_service/pkg/status"
)

const (
	analogFindOneEndPoint    = "/analog/find/one"
	analogFindViewEndPoint   = "/analog/find/view"
	findAnalogEndPoint       = "/analog/find/top10"
	productFindOne           = "/product/find/one"
	transportFindOneEndPoint = "/transport/find/one"
)

type Client struct {
	Client *http.Client
	cfg    config.Config
	log    logger.Logger
}

func New(cfg config.Config, log logger.Logger) *Client {
	return &Client{
		Client: &http.Client{},
		cfg:    cfg,
		log:    log,
	}
}

func (c *Client) AnalogFindView(ctx context.Context, id string) (domain.Analog, error) {
	var (
		logMsg   = "service.Analog.AnalogFindView "
		url      = c.cfg.AnalogService + analogFindViewEndPoint
		response service.DBOResponse
		data     AnalogView
		result   domain.Analog
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}
	q := request.URL.Query()
	q.Add("id", id)

	request.URL.RawQuery = q.Encode()

	resp, err := c.Client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(fmt.Sprint(logMsg+"Request ", id))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(fmt.Sprint(logMsg+"Request ", id))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Analog{
		ID:            data.ID,
		Model:         data.Model,
		Name:          data.Name,
		Position:      data.Position,
		Year:          data.Year,
		Speedometer:   data.Speedometer,
		Price:         data.Price,
		SellPrice:     data.SellPrice,
		Fuel:          data.Fuel,
		Condition:     data.Validity,
		Comment:       data.Comment,
		SourceFile:    data.SourceFile,
		SourceFileUrl: data.SourceFileUrl,
	}

	return result, nil
}

func (c *Client) AnalogFindOne(ctx context.Context, id string) (domain.Analog, error) {
	var (
		logMsg   = "service.Analog.FindAnalog "
		url      = c.cfg.AnalogService + analogFindOneEndPoint
		response service.DBOResponse
		data     Analog
		result   domain.Analog
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext failed", logger.Error(err))

		return result, errs.ErrInternal
	}
	q := request.URL.Query()
	q.Add("id", id)

	request.URL.RawQuery = q.Encode()

	resp, err := c.Client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.Client.Do failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(fmt.Sprint(logMsg+"Request ", id))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(fmt.Sprint(logMsg+"Request ", id))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Analog{
		ID:            data.ID,
		Model:         data.Model,
		Name:          data.Name,
		Position:      data.Position,
		Year:          data.Year,
		Speedometer:   data.Speedometer,
		Price:         data.Price,
		SellPrice:     data.SellPrice,
		Fuel:          data.Fuel,
		Validity:      data.Validity,
		Comment:       data.Comment,
		SourceFile:    data.SourceFile,
		SourceFileUrl: data.SourceFileUrl,
	}

	return result, nil
}

func (c *Client) FindAnalog(ctx context.Context, val domain.Transport) ([]domain.AssessmentStore, error) {
	var (
		logMsg   = "service.Analog.FindAnalog "
		url      = c.cfg.AnalogService + findAnalogEndPoint
		reqVal   AnalogInfo
		response service.DBOResponse
		data     []AnalogInfo
		result   []domain.AssessmentStore
	)

	reqVal = AnalogInfo{
		Type:        val.Type,
		Model:       val.Model,
		Name:        val.Name,
		Position:    val.Position,
		Fuel:        val.Fuel,
		Speedometer: val.Speedometer,
		Validity:    val.Validity,
		Year:        val.Year,
	}

	reqBytes, err := json.Marshal(reqVal)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPost, url, bytes.NewBuffer(reqBytes))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.Client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.Client.Do failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(fmt.Sprint(logMsg+"Request ", val))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(fmt.Sprint(logMsg+"Request ", val))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	for _, val := range data {
		value := domain.AssessmentStore{
			GID: val.TID,
			AID: val.AID,
			Transport: domain.Transport{
				Type:        val.Type,
				Model:       val.Model,
				Name:        val.Name,
				Position:    val.Position,
				Year:        val.Year,
				Speedometer: val.Speedometer,
				Validity:    val.Validity,
				Fuel:        val.Fuel,
				SellPrice:   val.SellPrice,
			},
		}
		result = append(result, value)
	}
	return result, nil
}

func (c *Client) ProductFindOne(ctx context.Context, id string) (domain.Product, error) {
	var (
		logMsg   = "service.Product.ProductFindOne "
		url      = c.cfg.AnalogService + productFindOne
		result   domain.Product
		response service.DBOResponse
		data     ProductResponse
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+" http.NewRequestWithContext failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	q := request.URL.Query()
	q.Add("id", id)
	request.URL.RawQuery = q.Encode()

	resp, err := c.Client.Get(request.URL.String())
	if err != nil {
		c.log.Error(logMsg+"c.Client.Get failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(fmt.Sprint(logMsg+"Request ", id))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(fmt.Sprint(logMsg+"Request ", id))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+" result.Scan failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Product{
		Product: data.Product,
	}

	return result, nil
}

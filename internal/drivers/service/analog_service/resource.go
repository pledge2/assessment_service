package analog_service

type AnalogInfo struct {
	AID         string `json:"a_id"`
	TID         string `json:"t_id"`
	Type        string `json:"type"`
	Model       string `json:"model"`
	Name        string `json:"name"`
	Speedometer int    `json:"speedometer"`
	Validity    int    `json:"validity"`
	Year        int    `json:"year"`
	SellPrice   int    `json:"sell_price"`
	Position    string `json:"position"`
	Fuel        string `json:"fuel"`
}

type ProductResponse struct {
	Product string `json:"product"`
}

type Analog struct {
	ID            string       `json:"id"`
	Type          string       `json:"type"`
	Model         string       `json:"model"`
	Name          string       `json:"name"`
	Position      string       `json:"position"`
	Speedometer   int          `json:"speedometer"`
	Year          int          `json:"year"`
	Fuel          string       `json:"fuel"`
	SellPrice     int          `json:"sell_price"`
	Price         int          `json:"price"`
	Validity      int          `json:"validity"`
	Active        bool         `json:"active"`
	Comment       string       `json:"comment"`
	SourceFileUrl string       `json:"source_file_url"`
	SourceFile    string       `json:"source_file"`
	AllPhotosUrl  AllPhotosUrl `json:"photos"`
	Employee      string       `json:"employee"`
	CreatedAt     string       `json:"created_at"`
	UpdatedAt     string       `json:"updated_at"`
}

type PhotoUrl struct {
	Title    string `json:"title"`
	Order    int    `json:"order"`
	PhotoUrl string `json:"photo_url"`
	LAT      string `json:"lat"`
	LON      string `json:"lon"`
}
type AllPhotosUrl struct {
	PhotoUrl []PhotoUrl `json:"photo"`
}

type AnalogView struct {
	ID            string       `json:"id"`
	Type          string       `json:"type"`
	Model         string       `json:"model"`
	Name          string       `json:"name"`
	Position      string       `json:"position"`
	Speedometer   int          `json:"speedometer"`
	Year          int          `json:"year"`
	Fuel          string       `json:"fuel"`
	SellPrice     int          `json:"sell_price"`
	Price         int          `json:"price"`
	Validity      string       `json:"validity"`
	Active        bool         `json:"active"`
	Comment       string       `json:"comment"`
	SourceFileUrl string       `json:"source_file_url"`
	SourceFile    string       `json:"source_file"`
	AllPhotosUrl  AllPhotosUrl `json:"photos"`
	Employee      string       `json:"employee"`
	CreatedAt     string       `json:"created_at"`
	UpdatedAt     string       `json:"updated_at"`
}

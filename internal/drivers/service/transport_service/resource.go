package transport_service

type Result struct {
	Result bool `json:"result"`
}

type Resp struct {
	ID string `json:"id"`
}

type PhotoName struct {
	Title     string `json:"title"`
	Order     int    `json:"order"`
	PhotoName string `json:"photo_name"`
	LAT       string `json:"lat"`
	LON       string `json:"lon"`
}

type PhotoUrl struct {
	Title    string `json:"title"`
	Order    int    `json:"order"`
	PhotoUrl string `json:"photo_url"`
	LAT      string `json:"lat"`
	LON      string `json:"lon"`
}
type allPhotosUrl struct {
	PhotoUrl []PhotoUrl `json:"photo"`
}

//	type allPhotosName struct {
//		PhotoName []PhotoName `json:"photo"`
//	}
type Price struct {
	Price     int `json:"price"`
	SellPrice int `json:"sell_price"`
}

type transportStore struct {
	Type        string `json:"type"`
	Model       string `json:"model"`
	Name        string `json:"name"`
	Position    string `json:"position"`
	Year        int    `json:"year"`
	Speedometer int    `json:"speedometer"`
	Validity    int    `json:"validity"`
	Fuel        string `json:"fuel"`
	ExtraParam  string `json:"extra_param"`
}

type transport struct {
	ID           string       `json:"id"`
	Type         string       `json:"type"`
	Model        string       `json:"model"`
	Name         string       `json:"name"`
	Position     string       `json:"position"`
	Year         int          `json:"year"`
	Speedometer  int          `json:"speedometer"`
	Validity     int          `json:"validity"`
	Fuel         string       `json:"fuel"`
	Price        int          `json:"price"`
	SellPrice    int          `json:"sell_price"`
	ExtraParam   string       `json:"extra_param"`
	AllPhotosUrl allPhotosUrl `json:"photos"`
}

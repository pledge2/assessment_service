package transport_service

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"pledge/assessment_service/internal/config"
	"pledge/assessment_service/internal/domain"
	"pledge/assessment_service/internal/errs"
	service "pledge/assessment_service/pkg/http"
	"pledge/assessment_service/pkg/logger"
	"pledge/assessment_service/pkg/status"
)

const (
	storeTransportEndPoint  = "/transport/store"
	findTransportEndPoint   = "/transport/find/one"
	getPhotosEndPoint       = "/transport/get/photos"
	updatePhotosEndPoint    = "/transport/update/photo"
	updatePriceEndPoint     = "/transport/update/price"
	deleteTransportEndPoint = "/transport/delete"
)

type Client struct {
	Client *http.Client
	cfg    config.Config
	log    logger.Logger
}

func New(cfg config.Config, log logger.Logger) *Client {
	return &Client{
		Client: &http.Client{},
		cfg:    cfg,
		log:    log,
	}
}

func (c *Client) Store(ctx context.Context, val domain.Transport) (string, error) {
	var (
		logMsg   = "service.Transport.Store "
		url      = c.cfg.AnalogService + storeTransportEndPoint
		response service.DBOResponse
		data     transportStore
		id       Resp
	)
	data = transportStore{
		Type:        val.Type,
		Model:       val.Model,
		Name:        val.Name,
		Position:    val.Position,
		Year:        val.Year,
		Speedometer: val.Speedometer,
		Validity:    val.Validity,
		Fuel:        val.Fuel,
		ExtraParam:  val.ExtraParam,
	}

	reqBytes, err := json.Marshal(data)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal failed", logger.Error(err))

		return "", errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPost, url, bytes.NewBuffer(reqBytes))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext failed", logger.Error(err))

		return "", errs.ErrInternal
	}

	resp, err := c.Client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.Client.Do failed", logger.Error(err))

		return "", errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll failed", logger.Error(err))

		return "", errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal failed", logger.Error(err))
		c.log.Info(fmt.Sprint("Response", string(res)))

		return "", errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(fmt.Sprint("Response", string(res)))

		return "", errs.New(response.ErrorNote)
	}

	if err = response.Scan(&id); err != nil {
		c.log.Error(logMsg+"info.Scan failed", logger.Error(err))

		return "", errs.ErrInternal
	}

	return id.ID, nil
}

func (c *Client) StorePhoto(ctx context.Context, id, folder string, info domain.PhotoName) (domain.Result, error) {
	var (
		logMsg      = "service.Transport.StorePhoto "
		url         = c.cfg.AnalogService + updatePhotosEndPoint + "?transportID=" + id + "&folder=" + folder
		response    service.DBOResponse
		requestBody PhotoName
		data        Result
		result      domain.Result
	)

	requestBody = PhotoName{
		Title:     info.Title,
		Order:     info.Order,
		PhotoName: info.PhotoName,
		LAT:       info.LAT,
		LON:       info.LON,
	}

	reqData, err := json.Marshal(requestBody)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPut, url, bytes.NewBuffer(reqData))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.Client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.Client.Do failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal failed", logger.Error(err))
		c.log.Info(fmt.Sprint("Response", string(res)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(fmt.Sprint("Response", string(res)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"info.Scan failed", logger.Error(err))

		return result, errs.ErrInternal
	}
	result = domain.Result{
		Result: data.Result,
	}
	return result, nil
}

func (c *Client) UpdatePrice(ctx context.Context, id string, price, sell_price int) (domain.Result, error) {
	var (
		logMsg    = "service.Transport.UpdatePrice "
		url       = c.cfg.AnalogService + updatePriceEndPoint
		response  service.DBOResponse
		result    domain.Result
		dataPrice Price
		data      Result
	)

	dataPrice.Price = price
	dataPrice.SellPrice = sell_price

	reqData, err := json.Marshal(dataPrice)
	if err != nil {
		c.log.Error(logMsg+"json.Marshal failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	request, err := http.NewRequestWithContext(ctx, http.MethodPut, url, bytes.NewBuffer(reqData))
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	q := request.URL.Query()
	q.Add("transportID", id)
	request.URL.RawQuery = q.Encode()

	resp, err := c.Client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.Client.Do failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Info(fmt.Sprint("Response", string(res)))
		c.log.Error(logMsg+"io.Unmarshal failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(fmt.Sprint("Response", string(res)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"info.Scan failed", logger.Error(err))

		return result, errs.ErrInternal
	}
	result = domain.Result{
		Result: data.Result,
	}
	return result, nil
}

func (c *Client) GetPhotos(ctx context.Context, id, year string) (domain.AllPhotosUrl, error) {
	var (
		logMsg   = "service.Transport.GetPhotos "
		url      = c.cfg.AnalogService + getPhotosEndPoint + "?id=" + id + "&year=" + year
		response service.DBOResponse
		result   domain.AllPhotosUrl
		data     allPhotosUrl
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext failed", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.Client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))
		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"info.Scan failed", logger.Error(err))

		return result, errs.ErrInternal
	}
	for _, val := range data.PhotoUrl {

		value := domain.PhotoUrl{
			Title:    val.Title,
			Order:    val.Order,
			PhotoUrl: val.PhotoUrl,
			LAT:      val.LAT,
			LON:      val.LON,
		}

		result.PhotoUrl = append(result.PhotoUrl, value)
	}
	return result, nil
}

func (c *Client) Find(ctx context.Context, id string) (domain.Transport, error) {
	var (
		logMsg   = "service.Transport.Find "
		url      = c.cfg.AnalogService + findTransportEndPoint
		data     transport
		result   domain.Transport
		response service.DBOResponse
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext failed", logger.Error(err))

		return result, err
	}

	q := request.URL.Query()
	q.Add("id", id)
	request.URL.RawQuery = q.Encode()

	resp, err := c.Client.Get(request.URL.String())
	if err != nil {
		c.log.Error(logMsg+"c.Client.Get failed", logger.Error(err))

		return result, err
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll failed", logger.Error(err))

		return result, err
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(fmt.Sprint(logMsg+"Request ", id))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(fmt.Sprint(logMsg+"Request ", id))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err = response.Scan(&data); err != nil {
		c.log.Error(logMsg+"info.Scan failed", logger.Error(err))

		return result, err
	}
	result = domain.Transport{
		ID:          data.ID,
		Type:        data.Type,
		Model:       data.Model,
		Name:        data.Name,
		Position:    data.Position,
		Year:        data.Year,
		Speedometer: data.Speedometer,
		Validity:    data.Validity,
		Fuel:        data.Fuel,
		SellPrice:   data.SellPrice,
		Price:       data.Price,
		ExtraParam:  data.ExtraParam,
	}

	for _, photo := range data.AllPhotosUrl.PhotoUrl {

		value := domain.PhotoUrl{
			PhotoUrl: photo.PhotoUrl,
			LAT:      photo.LAT,
			LON:      photo.LON,
			Title:    photo.Title,
			Order:    photo.Order,
		}
		result.AllPhotosUrl.PhotoUrl = append(result.AllPhotosUrl.PhotoUrl, value)
	}
	return result, nil
}

func (c *Client) DeleteTransport(ctx context.Context, id, year string) (domain.Result, error) {
	var (
		logMsg   = "service.Transport.DeleteTransport "
		url      = c.cfg.AnalogService + deleteTransportEndPoint + "?id=" + id + "&year=" + year
		response service.DBOResponse
		data     Result
		result   domain.Result
	)

	request, err := http.NewRequestWithContext(ctx, http.MethodDelete, url, nil)
	if err != nil {
		c.log.Error(logMsg+"http.NewRequestWithContext", logger.Error(err))

		return result, errs.ErrInternal
	}

	resp, err := c.Client.Do(request)
	if err != nil {
		c.log.Error(logMsg+"c.client.Do", logger.Error(err))

		return result, errs.ErrInternal
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		c.log.Error(logMsg+"io.ReadAll", logger.Error(err))

		return result, errs.ErrInternal
	}

	if err = json.Unmarshal(res, &response); err != nil {
		c.log.Error(logMsg+"json.Unmarshal", logger.Error(err))
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.ErrInternal
	}

	if response.ErrorCode != status.NoError {
		c.log.Info(logMsg, logger.Any("Request", url))
		c.log.Info(logMsg, logger.String("Response", string(response.Data)))

		return result, errs.New(response.ErrorNote)
	}

	if err := response.Scan(&data); err != nil {
		c.log.Error(logMsg+"response.Scan", logger.Error(err))

		return result, errs.ErrInternal
	}

	result = domain.Result{
		Result: data.Result,
	}

	return result, nil
}

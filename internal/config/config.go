package config

import (
	"fmt"
)

type Config struct {
	Environment string `env:"ENVIRONMENT"`
	LogLevel    string `env:"LOG_LEVEL"`
	ServerIP    string `env:"SERVER_IP"`
	HTTPPort    string `env:"HTTP_PORT"`
	ServiceName string `env:"SERVICE_NAME"`

	PostgresHost     string `env:"POSTGRES_HOST"`
	PostgresPort     string `env:"POSTGRES_PORT"`
	PostgresDatabase string `env:"POSTGRES_DATABASE"`
	PostgresUser     string `env:"POSTGRES_USER"`
	PostgresPassword string `env:"POSTGRES_PASSWORD"`
	PostgresSSLMode  string `env:"POSTGRES_SSLMODE"`

	ReferenceService      string `env:"REFERENCE_SERVICE"`
	AnalogService         string `env:"ANALOG_SERVICE"`
	IdentificationService string `env:"IDENTIFICATION_SERVICE"`
	OpenApiTokenUrl       string `env:"OPEN_API_TOKEN_URL"`
	OpenApiUrl            string `env:"OPEN_API_URL"`
	OpenApiUsername       string `env:"OPEN_API_USERNAME"`
	OpenApiPassword       string `env:"OPEN_API_PASSWORD"`
}

func (c *Config) PostgresURI() string {
	return fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
		c.PostgresHost,
		c.PostgresPort,
		c.PostgresUser,
		c.PostgresPassword,
		c.PostgresDatabase,
		c.PostgresSSLMode)
}

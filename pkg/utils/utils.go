package utils

import (
	"strings"
	"unicode"
)

func IsDigit(val string) bool {
	if len(val) > 0 {
		for _, v := range val {
			if !unicode.IsDigit(v) {
				return false
			}

		}
		return true
	}
	return false
}

func Formalize(preStr string) string {
	preStr = strings.TrimSpace(preStr)
	var letter string
	var newStr string
	for i, str := range preStr {
		if i == 0 {
			letter = strings.ToUpper(string(str))
		} else {
			letter = strings.ToLower(string(str))
		}
		newStr = newStr + letter
	}
	return newStr
}

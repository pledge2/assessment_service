package status

var (
	NoError                      = 0
	ErrorCodeCbidFormat          = -1
	ErrorCodeCbidOrEmailNotFound = -2
	ErrorCodeCbidNotFound        = -3
	ErrorCodeCbidEmpty           = -4

	ErrorCodePasswordInvalid = -5
	ErrorCodePasswordFormat  = -6
	ErrorCodePasswordNotSame = -7

	ErrorCodeEmailFormat = -8
	ErrorCodeBankEmail   = -9

	ErrorCodeValidation = -10

	ErrorCodeAccessTokenExpired  = -11
	ErrorCodeRefreshTokenExpired = -12
	ErrorCodeTokenNotFound       = -13

	ErrorCodeDeviceFormat  = -14
	ErrorCodeDeviceBlocked = -15
	ErrorCodeSmsFormat     = -16
	ErrorCodeSmsExpired    = -17
	ErrorCodeDataNotFound  = -18
	ErrorCodeFakeUser      = -19

	ErrorCodeUnauthorization = -20

	ErrorCodeRoleIDEmpty    = -21
	ErrorCodeRoleIDFormat   = -22
	ErrorCodeRoleCodeEmpty  = -23
	ErrorCodeRoleCodeFormat = -24
	ErrorCodeLimitEmpty     = -25
	ErrorCodeLimitFormat    = -26
	ErrorCodePageEmpty      = -27
	ErrorCodePageFormat     = -28
	ErrorCodePostCodeEmpty  = -29

	ErrorCodeInternal = -30

	ErrorCodeCbidDuplicate     = -31
	ErrorCodePostCodeDuplicate = -32
	ErrorCodeRoleChanged       = -33

	ErrorCodeAssessmentIDEmpty  = -41
	ErrorCodeAssessmentIDFormat = -42
	ErrorCodeStatusIDEmpty      = -43
	ErrorCodeStatusIDFormat     = -44
	ErrorCodeStatusCodeEmpty    = -45
	ErrorCodeStatusCodeFormat   = -46

	ErrorCodeAnalogNotFound    = -50
	ErrorCodeAnalogIDEmpty     = -51
	ErrorCodeAnalogIDFormat    = -52
	ErrorCodeProductIDEmpty    = -53
	ErrorCodeProductIDFormat   = -54
	ErrorCodeTransportIDEmpty  = -55
	ErrorCodeTransportIDFormat = -56

	ErrorCodeBucketEmpty   = -60
	ErrorCodeFolderEmpty   = -61
	ErrorCodeLATEmpty      = -62
	ErrorCodeLONEmpty      = -63
	ErrorCodeSuffixEmpty   = -64
	ErrorCodeFileNameEmpty = -65
	ErrorCodeTooLargeSize  = -66

	ErrorCodeTypeIDEmpty       = -70
	ErrorCodeTypeIDFormat      = -71
	ErrorCodeModelIDEmpty      = -72
	ErrorCodeModelIDFormat     = -73
	ErrorCodeNameIDEmpty       = -74
	ErrorCodeNameIDFormat      = -75
	ErrorCodePositionIDEmpty   = -76
	ErrorCodePositionIDFormat  = -77
	ErrorCodeValidityIDEmpty   = -78
	ErrorCodeValidityIDFormat  = -79
	ErrorCodeFuelIDEmpty       = -80
	ErrorCodeFuelIDFormat      = -81
	ErrorCodeBranchIDEmpty     = -82
	ErrorCodeBranchIDFormat    = -83
	ErrorCodeDirectEmpty       = -84
	ErrorCodeDirectFormat      = -85
	ErrorCodeEmpIDEmpty        = -86
	ErrorCodeEmpIDFormat       = -87
	ErrorCodeDiscountIDEmpty   = -88
	ErrorCodeDiscountIDFormat  = -89
	ErrorCodeDiscountNotFound  = -90
	ErrorCodeTransportMadeYear = -97
	ErrorCodeFakedValidity     = -98

	ErrorCodeDefault = -99
)

var (
	// Success - ...
	Success = "Success"
	// Failure - ...
	Failure = "Failure"
)

package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"pledge/assessment_service/api/docs"
	"pledge/assessment_service/internal/bootstrap"
	"pledge/assessment_service/internal/config"
	"pledge/assessment_service/pkg/logger"

	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"github.com/sethvargo/go-envconfig"
)

func main() {
	quitSignal := make(chan os.Signal, 1)
	signal.Notify(quitSignal, os.Interrupt)
	_ = godotenv.Load()

	var cfg config.Config
	if err := envconfig.ProcessWith(context.TODO(), &cfg, envconfig.OsLookuper()); err != nil {
		panic(fmt.Sprintf("envconfig.Process: %s", err))
	}

	docs.SwaggerInfo.Host = cfg.ServerIP + cfg.HTTPPort
	docs.SwaggerInfo.Description = "Storing Analogs, Get top analogs to assess pledges"
	docs.SwaggerInfo.Schemes = []string{"http"}

	log := logger.New(cfg.LogLevel, "Analog")

	ctx, cancel := context.WithCancel(context.Background())

	app := bootstrap.New(cfg, log, ctx)

	go func() {
		OSCall := <-quitSignal
		log.Info(fmt.Sprintf("system call:%+v", OSCall))
		cancel()
	}()

	app.Run(ctx)

	log.Info("REST Server Gracefully Shut Down")
}

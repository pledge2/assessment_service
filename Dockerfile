FROM hub-nx.hamkor.local/devops/golang:1.20 as builder

RUN mkdir -p $GOPATH/src/assessment
WORKDIR $GOPATH/src/assessment

COPY . ./

RUN export GOPROXY=http://gitlab.hamkorbank.uz:3002 && \
    export CGO_ENABLED=0 && \
    export GOOS=linux && \
    make build && \
    mv ./bin/assessment /

FROM hub-nx.hamkor.local/devops/alpine/with-tzdata:3.15.0
COPY --from=builder assessment .

CMD ["./assessment"]
